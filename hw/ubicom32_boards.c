/*
 * QEMU/UBICOM32 pseudo-board
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Nat Gurumoorthy
 *  Modified 2012 by Michael Eager.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * The pseudo board has timers.
 * The console io is simulated via the mailbox interface sitting in the OCP block.
 * The 8k has a mmu block while the 7k does not.
 */
#include "sysemu.h"
#include "boards.h"
#include "qemu-log.h"
#include "cpu.h"
#include "loader.h"
#include "memory.h"
#include "exec-memory.h"

static struct _loaderparams {
    int ram_size;
    const char *kernel_filename;
    const char *kernel_cmdline;
    //const char *initrd_filename;
} loaderparams;

static void load_kernel (CPUUBICOM32State *env)
{
    int64_t entry, kernel_low, kernel_high;
    long kernel_size;
    //long initrd_size;
    //ram_addr_t initrd_offset;
    int big_endian;

#ifdef TARGET_WORDS_BIGENDIAN
    big_endian = 1;
#else
    big_endian = 0;
#endif

    kernel_size = load_elf(loaderparams.kernel_filename, NULL, NULL,
                           (uint64_t *)&entry, (uint64_t *)&kernel_low,
                           (uint64_t *)&kernel_high, big_endian, ELF_MACHINE, 0);
    if (kernel_size < 0) {
        fprintf(stderr, "qemu: could not load kernel '%s'\n",
                loaderparams.kernel_filename);
        exit(1);
    }
    
    /*
     * Ubicom32 will not have a initrd for the time being.
     */
}
static void main_cpu_reset(void *opaque)
{
    CPUState *env = (CPUState *)opaque;

    cpu_reset(env);
/* FIXME -- anything else needed to reset cpu? */
    // env->halted = 0;
}

static void secondary_cpu_reset(void *opaque)
{
    main_cpu_reset(opaque);
}


static
void ubicom32_board_common_init (ram_addr_t ram_size,
			   	 const char *boot_device,
				 const char *kernel_filename, const char *kernel_cmdline,
			   	 const char *initrd_filename)
{
    UBICOM32CPU * cpu = NULL;
    CPUUBICOM32State *env = NULL;
    const struct ubicom32_board_t *def = &ubicom32_board_config;
/* FIXME - multiple memory regions? */
    MemoryRegion *address_space_mem = get_system_memory();
    MemoryRegion *ddr_ram = g_new(MemoryRegion, 1);
    MemoryRegion *flash_ram = g_new(MemoryRegion, 1);
    MemoryRegion *ocm_ram = g_new(MemoryRegion, 1);
    uint32_t i;
    char *filename;
    int bios_size;

    for (i=0; i < def->num_threads; i++) {
	cpu = cpu_ubicom32_init(def->cpu_name);

	if (!cpu) {
	    fprintf(stderr, "qemu: Unable to find Ubicom32 CPU %s definition\n", def->cpu_name);
	    exit(1);
	}

	env = &cpu->env;
	
	threads[i] = env;
	
	if (i == 0) {
	    qemu_register_reset(main_cpu_reset, cpu);
	} else {
	    qemu_register_reset(secondary_cpu_reset, cpu);
	    env->halted = 1;
	}
    }

    memory_region_init_ram(ddr_ram, "ubicom32.ddr_ram", def->ddr_size);
    vmstate_register_ram_global(ddr_ram);
    memory_region_add_subregion(address_space_mem, def->ddr_base, ddr_ram);

    memory_region_init_ram(flash_ram, "ubicom32.flash_ram", def->flash_size);
    vmstate_register_ram_global(flash_ram);
    memory_region_add_subregion(address_space_mem, def->flash_base, flash_ram);

    memory_region_init_ram(ocm_ram, "ubicom32.ocm_ram", def->ocm_size);
    vmstate_register_ram_global(ocm_ram);
    memory_region_add_subregion(address_space_mem, def->ocm_base, ocm_ram);

    /*
     * Get the off chip peripherals allocated and initialized.
     */
    
    ubicom32_ocp_init(env, def);
    
    /* load the BIOS image. */
    if (bios_name) {
/* FIXME -- filename not used. */
        filename = qemu_find_file(QEMU_FILE_TYPE_BIOS, bios_name);
	bios_size = load_image_targphys(bios_name,def->flash_base,
					def->flash_size);
    } else {
	bios_size = -1;
    }
    
    /*
     * Load kernel if there is onle.
     */
    if (kernel_filename) {
	loaderparams.ram_size = ram_size;
	loaderparams.kernel_filename = kernel_filename;
	loaderparams.kernel_cmdline = kernel_cmdline;
	//loaderparams.initrd_filename = initrd_filename;
	load_kernel(env);
    }
}

static
void ubicom32_board_init (ram_addr_t ram_size,
			       const char *boot_device,
			       const char *kernel_filename, const char *kernel_cmdline,
			       const char *initrd_filename, const char *cpu_model)
{
    // CPUUBICOM32State *env = NULL;
/* FIXME - multiple memory regions? */
    // MemoryRegion *address_space_mem = get_system_memory();
    // MemoryRegion *ram = g_new(MemoryRegion, 1);
    // uint32_t i;
    // ram_addr_t hrt0_offset;
    //ram_addr_t hrt1_offset;
    //ram_addr_t nrt_offset;
    // ram_addr_t flash_offset;
    // ram_addr_t ocm_offset;
    // ram_addr_t ddr_offset;
    // char *filename;
    // int bios_size;

    ubicom32_board_common_init (ram_size, boot_device, kernel_filename, kernel_cmdline, 
			        initrd_filename);

    /*
     * Allocate various pieces of memory.
     */
#if 0
/* FIXME -- check. */
    hrt0_offset = qemu_ram_alloc(1024);
    //hrt1_offset = qemu_ram_alloc(64);
    //nrt_offset = qemu_ram_alloc(64);
    flash_offset = qemu_ram_alloc(def->flash_size);
    ocm_offset = qemu_ram_alloc(def->ocm_size);
    ddr_offset = qemu_ram_alloc(def->ddr_size);
#else
/* FIXME */
//    memory_region_init_ram(ram, "hrt0", 1024);
//    memory_region_init_ram(ram, "flash", def->flash_size);
//    memory_region_init_ram(ram, "ocm", def->ocm_size);
//    memory_region_init_ram(ram, "ddr", def->ddr_size);
#endif
    
#if 0
/* FIXME - Translate to current metaphor. */
    cpu_register_physical_memory(def->hrt0_base, 1024, hrt0_offset | IO_MEM_RAM);
    //cpu_register_physical_memory(def->hrt1_base, 64, hrt1_offset | IO_MEM_RAM);
    //cpu_register_physical_memory(def->nrt_base, 64, nrt_offset | IO_MEM_RAM);
    cpu_register_physical_memory(def->flash_base, def->flash_size, flash_offset | IO_MEM_RAM);
    cpu_register_physical_memory(def->ocm_base, def->ocm_size, ocm_offset | IO_MEM_RAM);
    cpu_register_physical_memory(def->ddr_base, def->ddr_size, ddr_offset | IO_MEM_RAM);
#else
    /* FIXME */
    // memory_region_add_subregion(address_space_mem, 0, ram);
#endif
}


static QEMUMachine ubicom32_board_machine = {
    .name = "ubicom32",
    .desc = "ubicom32 configurable board platform",
    .init = ubicom32_board_init,
    .max_cpus = 12,
    .is_default = 1,
};

static void ubicom32_machine_init(void)
{
    qemu_register_machine(&ubicom32_board_machine);
}

machine_init(ubicom32_machine_init);
