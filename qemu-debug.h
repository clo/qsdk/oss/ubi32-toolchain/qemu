/*  Internal debug support.  */

extern int qemu_debug;

#define DEBUG_TRANSLATE 0x01
#define DEBUG_CPU_EXEC	0x02
#define DEBUG_SIMCALL 	0x04
#define DEBUG_TRAP 	0x08
