/*
 *  QEMU Ubicom32 CPU
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Natarajan Gurumoorthy
 *  Modified by Michael Eager <eager@eagercon.com> based on OpenRISC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "cpu.h"
#include "qemu-common.h"
#include "qemu-config.h"

/* FIXME: Should be dynamic - see ubicom32_board_t:num_threads.  */
CPUUBICOM32State *threads[NUM_CONTEXTS];
uint32_t ubicom32_global_registers[192];

static void init_globals(uint32_t *ptr, uint32_t init_val, uint32_t num_regs)
{
    while(num_regs--) {
	*ptr++ = init_val;
    }
}

/* CPUClass::reset() */

static void ubicom32_cpu_reset (CPUState *s)
{
    UBICOM32CPU *cpu = UBICOM32_CPU(s);
    CPUUBICOM32State *env = &cpu->env;

    if (qemu_loglevel_mask(CPU_LOG_RESET)) {
	qemu_log("CPU Reset (CPU %d)\n", env->cpu_index);
	log_cpu_state(env, 0);
    }

    memset(env, 0, offsetof(CPUUBICOM32State, breakpoints));

    tlb_flush(env, 1);

    /*
     * Set rosr from cpu->env.cpu_index
     */
    env->thread_regs[0x2e] = env->cpu_index << 2;

#if 1
/* FIXME */
    /*
     * Set pc to flash_base
     */
    env->thread_regs[0x34] = env->def->flash_base;

    /*
     * Setup chip_id from cpu->env.dev
     */
    env->global_regs[0x40 - 0x40] = env->def->chip_id;

    /*
     * Setup cc_sr_index;
     */
    if (env->def->chip_id == 0x00030002) {
	/*
	 * 7k. CSR is the cc reg.
	 */
	env->cc_sr_index = 0x2d;
    } else {
	/*
	 * 7k. CSR is the cc reg.
	 */
	env->cc_sr_index = 0x39;
    }
#else
    printf ("Ubicom32 CPU not completely reset\n");
#endif

    /*
     * Set up mt_en, mt_active, mt_hpri, mt_dbg_active
     */
    env->global_regs[0x4e - 0x40] = 0x1;
    env->global_regs[0x53 - 0x40] = 0x1;
    env->global_regs[0x54 - 0x40] = 0x1;
    env->global_regs[0x51 - 0x40] = 0x1;

    /*
     * Reset i_rangeX_hi registers to 0xfffffffc
     */
    init_globals(&env->global_regs[0x80 - 0x40], 0xfffffffc, 4);

    /*
     * Reset i_rangeX_en to 0xfff
     */
    init_globals(&env->global_regs[0x90 - 0x40], 0xfff, 4);

    /*
     * Reset d_rangeX_hi registers to 0xfffffffc
     */
    init_globals(&env->global_regs[0x98 - 0x40], 0xfffffffc, 6);

    /*
     * Reset d_rangeX_en to 0xfff
     */
    init_globals(&env->global_regs[0xa8 - 0x40], 0xfff, 6);

    /*
     * Reset i_rangeX_user_en to 0xfff
     */
    init_globals(&env->global_regs[0xb0 - 0x40], 0xfff, 4);

    /*
     * Reset d_rangeX_user_en to 0xfff
     */
    init_globals(&env->global_regs[0xb8 - 0x40], 0xfff, 6);
}

#if 0
static inline int ubicom32_feature(CPUUBICOM32State *env, int feature)
{
    return (cpu->env.features & (1u << feature)) != 0;
}
#endif

static inline void set_feature(UBICOM32CPU *cpu, int feature)
{
    cpu->feature |= feature;
}

static void ubicom32_cpu_realize(Object *obj, Error **errp)
{
    UBICOM32CPU *cpu = UBICOM32_CPU(obj);

    qemu_init_vcpu(&cpu->env);
    cpu_reset(CPU(cpu));
}

static void ubicom32_cpu_initfn(Object *obj)
{
    UBICOM32CPU *cpu = UBICOM32_CPU(obj);
    static int inited;

    cpu_exec_init(&cpu->env);

#ifndef CONFIG_USER_ONLY
    cpu_ubicom32_mmu_init(cpu);
#endif

    if (tcg_enabled() && !inited) {
        inited = 1;
        ubicom32_translate_init();
    }
}

/* CPU models */
static void ubicom32v5_initfn(Object *obj)
{
    //UBICOM32CPU *cpu = UBICOM32_CPU(obj);

    /* set_feature(cpu, UBICOM32_FEATURE_XXXX); */
}

typedef struct UBICOM32CPUInfo {
    const char *name;
    void (*initfn)(Object *obj);
} UBICOM32CPUInfo;

static const UBICOM32CPUInfo ubicom32_cpus[] = {
    { .name = "ubicom32v5",  .initfn = ubicom32v5_initfn },
};

static void ubicom32_cpu_class_init(ObjectClass *oc, void *data)
{
    UBICOM32CPU *occ = UBICOM32_CPU_CLASS(oc);
    CPUClass *cc = CPU_CLASS(occ);

    occ->parent_reset = cc->reset;
    cc->reset = ubicom32_cpu_reset;
}

static void cpu_register(const UBICOM32CPUInfo *info)
{
    TypeInfo type_info = {
        .name = info->name,
        .parent = TYPE_UBICOM32_CPU,
        .instance_size = sizeof(UBICOM32CPU),
        .instance_init = info->initfn,
        .class_size = sizeof(UBICOM32CPU),
    };

    type_register_static(&type_info);
}

static const TypeInfo ubicom32_cpu_type_info = {
    .name = TYPE_UBICOM32_CPU,
    .parent = TYPE_CPU,
    .instance_size = sizeof(UBICOM32CPU),
    .instance_init = ubicom32_cpu_initfn,
    .abstract = false,
    .class_size = sizeof(UBICOM32CPU),
    .class_init = ubicom32_cpu_class_init,
};

static void ubicom32_cpu_register_types(void)
{
    int i;

    type_register_static(&ubicom32_cpu_type_info);
    for (i = 0; i < ARRAY_SIZE(ubicom32_cpus); i++) {
        cpu_register(&ubicom32_cpus[i]);
    }
}

static uint32_t *find_valid_insn (const char *cpu_name)
{
    struct ubicom32_valid_insn_t *valid;

    for (valid = ubicom32_valid_insn; valid->name; valid++) {
        if (strcmp (cpu_name, valid->name) == 0)
	    return valid->valid_insn;
        }
    return NULL;
}

static int ubicom32_set_mapfile (QemuOpts *opts, void *opaque)
{
    const char *str;

    if ((str = qemu_opt_get (opts, "name"))) ubicom32_board_config.name = str;
    if ((str = qemu_opt_get (opts, "cpu_name"))) ubicom32_board_config.cpu_name = str;
    ubicom32_board_config.chip_id = qemu_opt_get_number (opts, "chip_id",
							 ubicom32_board_config.chip_id);
    ubicom32_board_config.hrt0_base = qemu_opt_get_number (opts, "hrt0_base",
							   ubicom32_board_config.hrt0_base);
    ubicom32_board_config.hrt1_base = qemu_opt_get_number (opts, "hrt1_base",
							   ubicom32_board_config.hrt1_base);
    ubicom32_board_config.nrt_base = qemu_opt_get_number (opts, "nrt_base",
							  ubicom32_board_config.nrt_base);
    ubicom32_board_config.flash_base = qemu_opt_get_number (opts, "flash_base",
							    ubicom32_board_config.flash_base);
    ubicom32_board_config.flash_size = qemu_opt_get_number (opts, "flash_size",
							    ubicom32_board_config.flash_size);
    ubicom32_board_config.ocm_base = qemu_opt_get_number (opts, "ocm_base",
							  ubicom32_board_config.ocm_base);
    ubicom32_board_config.ocm_size = qemu_opt_get_number (opts, "ocm_size",
							  ubicom32_board_config.ocm_size);
    ubicom32_board_config.ddr_base = qemu_opt_get_number (opts, "ddr_base",
							  ubicom32_board_config.ddr_base);
    ubicom32_board_config.ddr_size = qemu_opt_get_number (opts, "ddr_size",
							  ubicom32_board_config.ddr_size);
    ubicom32_board_config.ocp_base = qemu_opt_get_number (opts, "ocp_base",
							  ubicom32_board_config.ocp_base);
    ubicom32_board_config.ocp_size = qemu_opt_get_number (opts, "ocp_size",
							  ubicom32_board_config.ocp_size);
    ubicom32_board_config.io_base = qemu_opt_get_number (opts, "io_base",
							 ubicom32_board_config.io_base);
    ubicom32_board_config.io_size = qemu_opt_get_number (opts, "io_size",
							 ubicom32_board_config.io_size);
    ubicom32_board_config.ether_base = qemu_opt_get_number (opts, "ether_base",
							    ubicom32_board_config.ether_base);
    ubicom32_board_config.ether_size = qemu_opt_get_number (opts, "ether_size",
							    ubicom32_board_config.ether_size);
    ubicom32_board_config.timer_offset = qemu_opt_get_number (opts, "timer_offset",
							      ubicom32_board_config.timer_offset);
    ubicom32_board_config.mailbox_offset = qemu_opt_get_number (opts, "mailbox_offset",
							        ubicom32_board_config.mailbox_offset);
    ubicom32_board_config.mmu_offset = qemu_opt_get_number (opts, "mmu_offset",
							    ubicom32_board_config.mmu_offset);
    ubicom32_board_config.pll_offset = qemu_opt_get_number (opts, "pll_offset",
							    ubicom32_board_config.pll_offset);
    ubicom32_board_config.num_threads = qemu_opt_get_number (opts, "num_threads",
							     ubicom32_board_config.num_threads);
    ubicom32_board_config.num_timers = qemu_opt_get_number (opts, "num_timers",
							    ubicom32_board_config.num_timers);
    ubicom32_board_config.num_interrupts = qemu_opt_get_number (opts, "num_interrupts",
							        ubicom32_board_config.num_interrupts);
    ubicom32_board_config.has_mmu = qemu_opt_get_number (opts, "has_mmu",
							 ubicom32_board_config.has_mmu);
    ubicom32_board_config.mapped_base = qemu_opt_get_number (opts, "mapped_base",
							     ubicom32_board_config.mapped_base);
    ubicom32_board_config.mapped_size = qemu_opt_get_number (opts, "mapped_size",
							    ubicom32_board_config.mapped_size);
    ubicom32_board_config.reset_vector = qemu_opt_get_number (opts, "reset_vector",
							      ubicom32_board_config.reset_vector);
    return 0;
}

UBICOM32CPU *cpu_ubicom32_init(const char *cpu_name)
{
    UBICOM32CPU *cpu;
    static int tcg_inited = 0;

    if (!object_class_by_name(cpu_name)) {
        return NULL;
    }
    cpu = UBICOM32_CPU(object_new(cpu_name));
    cpu->env.cpu_model_str = cpu_name;

    if (qemu_opts_foreach(qemu_find_opts("mapfile"),
			  ubicom32_set_mapfile, NULL, 0))
        exit(1);

    ubicom32_board_config.valid_insn = find_valid_insn (ubicom32_board_config.cpu_name);
    if (!ubicom32_board_config.valid_insn) {
        printf ("Invalid Ubicom32 processor version %s\n", cpu_name);
        exit(1);
    }
    cpu->env.def = &ubicom32_board_config;

    /* Point to global registers. */
    cpu->env.global_regs = ubicom32_global_registers;

    /* Set number of regs for GDB. */
    switch (cpu->env.def->chip_id) {
/* FIXME: put in correct number of regs. */
      case UBICOM32_7K_CHIP_ID:
      case UBICOM32_8K_CHIP_ID:
      case UBICOM32_AKRONITE_CHIP_ID:
	cpu->env.num_gdb_regs = 103;
	break;

      default:
	assert(0);
    }

    ubicom32_cpu_realize(OBJECT(cpu), NULL);

    if (!tcg_inited) {
	ubicom32_tcg_init();
	tcg_inited = 1;
    }

    return cpu;
}

#if 0
/* Originally from translate.c */
CPUUBICOM32State *cpu_ubicom32_init (const char *cpu_model)
{
    CPUUBICOM32State *env;
    const struct ubicom32_board_t *def;
    static int tcg_inited = 0;

    def = ubicom32_find_board_by_name(cpu_model);
    if (!def)
        return NULL;
    env = qemu_mallocz(sizeof(CPUUBICOM32State));
    cpu->env.def = def;
    cpu->env.global_regs = ubicom32_global_registers;
    cpu_exec_init(env);
    env->cpu_model_str = cpu_model;

    if (!tcg_inited) {
	ubicom32_tcg_init();
	tcg_inited = 1;
    }
    cpu_reset(env);
    qemu_init_vcpu(env);
    return env;
}
#endif

typedef struct UBICOM32CPUList {
    fprintf_function cpu_fprintf;
    FILE *file;
} UBICOM32CPUList;

/* Sort alphabetically by type name, except for "any". */
gint ubicom32_cpu_list_compare(gconstpointer a, gconstpointer b)
{
    ObjectClass *class_a = (ObjectClass *)a;
    ObjectClass *class_b = (ObjectClass *)b;
    const char *name_a, *name_b;

    name_a = object_class_get_name(class_a);
    name_b = object_class_get_name(class_b);
    if (strcmp(name_a, "any") == 0) {
        return 1;
    } else if (strcmp(name_b, "any") == 0) {
        return -1;
    } else {
        return strcmp(name_a, name_b);
    }
}

static void ubicom32_cpu_list_entry(gpointer data, gpointer user_data)
{
    ObjectClass *oc = data;
    UBICOM32CPUList *s = user_data;

    (*s->cpu_fprintf)(s->file, "  %s\n",
                      object_class_get_name(oc));
}

void cpu_ubicom32_list(FILE *f, fprintf_function cpu_fprintf)
{
    UBICOM32CPUList s = {
        .file = f,
        .cpu_fprintf = cpu_fprintf,
    };
    GSList *list;

    list = object_class_get_list(TYPE_UBICOM32_CPU, false);
    list = g_slist_sort(list, ubicom32_cpu_list_compare);
    (*cpu_fprintf)(f, "Available CPUs:\n");
    g_slist_foreach(list, ubicom32_cpu_list_entry, &s);
    g_slist_free(list);
}

type_init(ubicom32_cpu_register_types)
