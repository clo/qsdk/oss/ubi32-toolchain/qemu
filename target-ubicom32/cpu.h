/*
 * ubicom32 virtual CPU header
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Nat Gurumoorthy
 *  Modified by Michael Eager <eager@eagercon.com> based on OpenRISC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CPU_UBICOM32_H
#define CPU_UBICOM32_H

#define TARGET_LONG_BITS 32
#define TARGET_PHYS_ADDR_SPACE_BITS 32
#define TARGET_VIRT_ADDR_SPACE_BITS 32
#define ELF_MACHINE  EM_UBICOM32

#define CPUArchState struct CPUUBICOM32State

#include "config.h"
#include "qemu-common.h"
#include "cpu-defs.h"
#include "softfloat.h"
#include "qemu/cpu.h"
#include "error.h"
#include "qemu-option.h"

#define TARGET_HAS_ICE 1
#define TYPE_UBICOM32_CPU "ubicom32-cpu"

#define UBICOM32_7K_CHIP_ID 0x0003ffff
#define UBICOM32_8K_CHIP_ID 0x0004ffff
#define UBICOM32_AKRONITE_CHIP_ID 0x0005ffff

/* FIXME -- magic value. */
#define GLOBAL_REG(x) ((x) - 0x40)

#define UBICOM32_CPU_CLASS(klass) \
    OBJECT_CLASS_CHECK(UBICOM32CPU, (klass), TYPE_UBICOM32_CPU)
#define UBICOM32_CPU(obj) \
    OBJECT_CHECK(UBICOM32CPU, (obj), TYPE_UBICOM32_CPU)
#define UBICOM32_CPU_GET_CLASS(obj) \
    OBJECT_GET_CLASS(UBICOM32CPU, (obj), TYPE_UBICOM32_CPU)


/* FIXME */
extern uint32_t ubicom32_global_registers[192];

/*
 * TRAP_CAUSE register
 */
#define TRAP_CAUSE_TOTAL		14
#define TRAP_CAUSE_PRIVILEGE            13
#define TRAP_CAUSE_DST_RANGE_ERR	12
#define TRAP_CAUSE_SRC1_RANGE_ERR	11
#define TRAP_CAUSE_I_RANGE_ERR		10
#define TRAP_CAUSE_DCAPT		9
#define TRAP_CAUSE_DST_SERROR		8
#define TRAP_CAUSE_SRC1_SERROR		7
#define TRAP_CAUSE_DST_MISALIGNED	6
#define TRAP_CAUSE_SRC1_MISALIGNED	5
#define TRAP_CAUSE_DST_DECODE_ERR	4
#define TRAP_CAUSE_SRC1_DECODE_ERR	3
#define TRAP_CAUSE_ILLEGAL_INST		2
#define TRAP_CAUSE_I_SERROR		1
#define TRAP_CAUSE_I_DECODE_ERR		0

/*
 * Board description structure will contain information describing the 
 * board or processor.  
 * Start addresses of the various blocks of memory OCM/DDR etc.
 * It will contain chip_id, number of thread, number of timers etc.
 */
struct ubicom32_board_t {
    const char *name;
    const char *cpu_name;
    uint32_t chip_id;
    uint32_t hrt0_base;
    uint32_t hrt1_base;
    uint32_t nrt_base;
    uint32_t flash_base;
    uint32_t flash_size;
    uint32_t ocm_base;
    uint32_t ocm_size;
    uint32_t ddr_base;
    uint32_t ddr_size;
    uint32_t ocp_base;
    uint32_t ocp_size;
    uint32_t io_base;
    uint32_t io_size;
    uint32_t ether_base;
    uint32_t ether_size;
    uint32_t timer_offset;
    uint32_t mailbox_offset;
    uint32_t mmu_offset;
    uint32_t pll_offset;
    uint32_t num_threads;
    uint32_t num_timers;
    uint32_t num_interrupts;
    uint32_t has_mmu;
    uint32_t mapped_base;
    uint32_t mapped_size;
    uint32_t reset_vector;
    uint32_t num_dranges;
    uint32_t num_iranges;
    uint32_t *valid_insn;
};

extern struct ubicom32_board_t ubicom32_board_config;

#define EM_UBICOM32 	        0xde3d	/* Ubicom32; no ABI */
#define ELF_MACHINE	EM_UBICOM32
/* FIXME: Should be dynamic. See num_threads. */
#define NUM_CONTEXTS 16
#define NB_MMU_MODES 2

typedef struct CPUUBICOM32State {
    uint32_t thread_regs[64];
    uint32_t thread_number;	/* Thread ID . */
    uint32_t pc;
    uint32_t dest_data;
    uint32_t src1_data;
    uint32_t src2_data;
    uint32_t dest_address;	/* Destination address. Load it and this will be used by to helper to do alignment and destination checks. */
    uint32_t src1_address;	/* Src1 address. Needs to be loaded and checked by the helper for alignment and range checks. */
    uint32_t bset_clr_tmp;	/* bset, bclr temporary. Holds the mask. */
    uint32_t postinc_areg;
    uint32_t increment;
    uint32_t current_pc;
    uint64_t src1_64;
    uint64_t src2_64;
    uint64_t temp_64;
    uint64_t dest_64;		/* 64 bit entries needed for DSP .4 computations. */
    uint32_t cc_sr_index;		/* For jupiter this is 0xe4/4 for Ares this is 0xb4/4 */
    CPU_COMMON
    const struct ubicom32_board_t *def;	/* Pointer to the structure that contain cpu address layout. */
    uint32_t *global_regs;	/* Global registers */
    uint32_t features;
    uint32_t num_gdb_regs;	/* Number of regs to send to GDB.  */
} CPUUBICOM32State;

struct ubicom32_valid_insn_t {
    char *name;
    uint32_t *valid_insn;
};

/**
 * UBICOM32CPU:
 * @env: #CPUUbicom32State
 *
 * A UBICOM32 CPU model.
 */
typedef struct UBICOM32CPU {
    /*< private >*/
    CPUState parent_class;
    /*< public >*/

    CPUUBICOM32State env;
    uint32_t feature;			/* CPU capabilities */

    void (*parent_reset)(CPUState *cpu);
} UBICOM32CPU;

static inline UBICOM32CPU *ubicom32_env_get_cpu(CPUUBICOM32State *env)
{
    return UBICOM32_CPU(container_of(env, UBICOM32CPU, env));
}

#define ENV_GET_CPU(e) CPU(ubicom32_env_get_cpu(e))

#if 0
extern uint32_t ubicom32_global_registers[192];
#endif

enum {
    // condition codes for jmpcc instruction
    CC_F,
    CC_NC,
    CC_C,
    CC_Z,
    CC_GE,
    CC_GT,
    CC_HI,
    CC_LE,
    CC_LS,
    CC_LT,
    CC_MI,
    CC_NZ,
    CC_PL,
    CC_T,
    CC_NV,
    CC_V,
};

void ubicom32_tcg_init(void);
UBICOM32CPU *cpu_ubicom32_init(const char *cpu_name);
int cpu_ubicom32_exec(CPUUBICOM32State *s);
void cpu_ubicom32_close(CPUUBICOM32State *s);
void do_interrupt(CPUUBICOM32State *env);
const struct ubicom32_board_t *cpu_ubicom32_find_by_name(const char *cpu_model);
void ubicom32_ocp_init (CPUUBICOM32State *env, const struct ubicom32_board_t *def);
void ubicom32_raise_interrupt(uint32_t int_num);
void generate_trap(CPUUBICOM32State *env, uint32_t thread_no, uint32_t trap_no);
unsigned int ubicom32_gdb_index_to_address(CPUUBICOM32State *env, int n);
void cpu_ubicom32_mmu_init(UBICOM32CPU *cpu);
void ubicom32_translate_init(void);
void cpu_ubicom32_list(FILE *f, fprintf_function cpu_fprintf);
gint ubicom32_cpu_list_compare(gconstpointer a, gconstpointer b);
void inst_mapping_check(CPUUBICOM32State *env, uint32_t i_vaddr);
void mapping_check(CPUUBICOM32State *env, uint32_t s1_vaddr, uint32_t d_vaddr, uint32_t check);
uint32_t tlb_check(CPUUBICOM32State *env, uint32_t *vpn, uint32_t *asid, uint32_t vaddr, 
		   uint32_t *return_ptec_entry, uint32_t is_read, uint32_t is_ifetch);
void create_fault(uint32_t tnum, uint32_t vpn, uint32_t asid, uint32_t type, 
		  uint32_t slot, uint32_t dtlb);
const struct ubicom32_board_t *ubicom32_find_board_by_name(const char *name);

static inline uint32_t ubicom32_range_check(uint32_t low, uint32_t size, uint32_t address) 
{
  /* Subtract low from address to avoid overflow. */
  return ((address >= low) && ((address - low) <= size));
};

uint32_t ip7k_hole_check(CPUUBICOM32State *env, uint32_t address);
uint32_t ip7k_data_range_check(CPUUBICOM32State *env, uint32_t address);
uint32_t ip7k_instruction_range_check(CPUUBICOM32State *env, uint32_t address);

uint32_t ip8k_hole_check(CPUUBICOM32State *env, uint32_t address);
uint32_t ip8k_data_range_check(CPUUBICOM32State *env, uint32_t address);
uint32_t ip8k_instruction_range_check(CPUUBICOM32State *env, uint32_t address);
int ubicom32_parse_config(QemuOptsList *opts_list, const char *optarg);
int ubicom32_parse_mapfile_option (const char *name, const char *value, void *opaque);
int ubicom32_validate_instruction (CPUUBICOM32State *env, unsigned int opcode);

extern CPUUBICOM32State *threads[NUM_CONTEXTS];
extern struct ubicom32_valid_insn_t ubicom32_valid_insn[];

#ifdef CONFIG_USER_ONLY
/* Linux uses 8k pages.  */
#define TARGET_PAGE_BITS 14
#else
/* FIXME:  4K is 12 bits.  */
/* Smallest TLB entry size is 4k.  */
#define TARGET_PAGE_BITS 14
#endif

#define cpu_init cpu_ubicom32_init
#define cpu_exec cpu_ubicom32_exec
#define cpu_gen_code cpu_ubicom32_gen_code
#define cpu_signal_handler cpu_ubicom32_signal_handler
#define cpu_list ubicom32_cpu_list

int cpu_ubicom32_handle_mmu_fault(CPUUBICOM32State *env, target_ulong address, int rw,
                              int mmu_idx, int is_softmmu);

void ubicom32_cpu_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...));

#include "cpu-all.h"

static inline int cpu_mmu_index(CPUUBICOM32State *env)
{
    return 0;
}

static inline bool cpu_has_work(CPUUBICOM32State *env)
{
    return true;
}

#include "exec-all.h"

extern void gen_pc_load(CPUUBICOM32State *env, TranslationBlock *tb,
		 unsigned long searched_pc, int pc_pos, void *puc);

static inline void cpu_pc_from_tb(CPUUBICOM32State *env, TranslationBlock *tb)
{
    env->thread_regs[0x34] = tb->pc;
}

static inline void cpu_get_tb_cpu_state(CPUUBICOM32State *env, target_ulong *pc,
                                        target_ulong *cs_base, int *flags)
{
    *pc = env->thread_regs[0x34];
    *cs_base = 0;
    *flags = 0;
    env->current_pc = env->thread_regs[0x34];
}

/*
** General defines
*/

#define UINT48_MAX      0xffffffffffffLL

#define BIT_0		1
#define BIT_1		(1 << 1)
#define BIT_2		(1 << 2)
#define BIT_3		(1 << 3)
#define BIT_4		(1 << 4)
#define BIT_5		(1 << 5)
#define BIT_6		(1 << 6)
#define BIT_7		(1 << 7)
#define BIT_8		(1 << 8)
#define BIT_9		(1 << 9)
#define BIT_10		(1 << 10)
#define BIT_11		(1 << 11)
#define BIT_12		(1 << 12)
#define BIT_13		(1 << 13)
#define BIT_14		(1 << 14)
#define BIT_15		(1 << 15)
#define BIT_16		(1 << 16)
#define BIT_17		(1 << 17)
#define BIT_18		(1 << 18)
#define BIT_19		(1 << 19)
#define BIT_20		(1 << 20)
#define BIT_21		(1 << 21)
#define BIT_22		(1 << 22)
#define BIT_23		(1 << 23)
#define BIT_24		(1 << 24)
#define BIT_25		(1 << 25)
#define BIT_26		(1 << 26)
#define BIT_27		(1 << 27)
#define BIT_28		(1 << 28)
#define BIT_29		(1 << 29)
#define BIT_30		(1 << 30)
#define BIT_31		(1 << 31)


#define CCF_16_C 0x01
#define CCF_16_V 0x02
#define CCF_16_Z 0x04
#define CCF_16_N 0x08
#define CCF_32_C 0x10
#define CCF_32_V 0x20
#define CCF_32_Z 0x40
#define CCF_32_N 0x80
#define CCF_DSP_O 0x100000

/*
** Instruction encoding defines
*/

#define DSP_C_BIT	BIT_20
#define DSP_T_BIT	BIT_19
#define DSP_S_BIT	BIT_18
#define DSP_O_BIT	BIT_17
#define DSP_A_BIT	BIT_16

/*
** CSR bits
*/

#define CSR_O_BIT	BIT_20
#define CSR_PRIV_BIT 21         /* Privilege bit in CSR */
#define CSR_PREV_PRIV_BIT 22    /* Previous Privilege bit in CSR */
#define CSR_PRIV_MASK   (0x3 << CSR_PRIV_BIT)

/* Ubicom32 FPU-related UCSR bits */

#define UCSR_FPU_ALL_BITS               (((1 << 5) - 1) << 15)
#define UCSR_FPU_INEXACT_BIT		BIT_15
#define UCSR_FPU_UNDERFLOW_BIT		BIT_16
#define UCSR_FPU_OVERFLOW_BIT		BIT_17
#define UCSR_FPU_DIVIDE_BY_ZERO_BIT	BIT_18
#define UCSR_FPU_INVALID_BIT            (1 << 19)

/* Macros to decode 11 bit source1/dest encoding */

#define SOURCE11_IS_IMMED(x)			((x >> 8) == 0)
#define SOURCE11_IS_DIRECT_REGISTER(x)		((x >> 8) == 1)
#define SOURCE11_IS_MEMORY(x)			(!SOURCE11_IS_IMMED(x) && !IS_REGISTER(x))
#define SOURCE11_REGISTER_NUM(x)		(x & 0xff)

#define S1_CHECK 0x01
#define DEST_CHECK 0x02
#define INST_CHECK 0x04

#define MMU_MISSQW0_TYPE_MISS_READ      0x0
#define MMU_MISSQW0_TYPE_MISS_WRITE     0x1
#define MMU_MISSQW0_TYPE_PRIVACC_READ   0x2
#define MMU_MISSQW0_TYPE_PRIVACC_WRITE  0x3
#define MMU_MISSQW0_TYPE_PRIVACC_MASK   0x2

#define MMU_MISSQW0_TYPE_SERVICED       0x4
#define MMU_MISSQW0_TYPE_SERROR         0x8

/* CPU registers */

/* Obsolete names for some registers */
#define REGISTER_MAC_HI REGISTER_ACC0_HI
#define REGISTER_MAC_LO REGISTER_ACC0_LO

typedef enum {

	REGISTER_D0		= 0,
	REGISTER_D1,
	REGISTER_D2,
	REGISTER_D3,
	REGISTER_D4,
	REGISTER_D5,
	REGISTER_D6,
	REGISTER_D7,
	REGISTER_D8,
	REGISTER_D9,
	REGISTER_D10,
	REGISTER_D11,
	REGISTER_D12,
	REGISTER_D13,
	REGISTER_D14,
	REGISTER_D15,

	REGISTER_A0		= 0x20,
	REGISTER_A1,
	REGISTER_A2,
	REGISTER_A3,
	REGISTER_A4,
	REGISTER_A5,
	REGISTER_A6,
	REGISTER_A7,

	REGISTER_ACC0_HI,
	REGISTER_ACC0_LO,
	REGISTER_MAC_RC16,
	REGISTER_SOURCE3,
	REGISTER_INST_CNT,
	REGISTER_CSR,
	REGISTER_ROSR,
	REGISTER_IREAD_DATA,
	REGISTER_INT_MASK0,
	REGISTER_INT_MASK1,
	REGISTER_INT_MASK2,

	REGISTER_PC		= 0x34,
	REGISTER_TRAP_CAUSE,
	REGISTER_ACC1_HI,
	REGISTER_ACC1_LO,
	REGISTER_PREVIOUS_PC	= 0x38,
	REGISTER_UCSR,

	/* Global registers */

	REGISTER_CHIP_ID	= 0x40,
	REGISTER_INT_STAT0,
	REGISTER_INT_STAT1,
	REGISTER_INT_STAT2,

	REGISTER_INT_SET0	= 0x45,
	REGISTER_INT_SET1,
	REGISTER_INT_SET2,

	REGISTER_INT_CLR0	= 0x49,
	REGISTER_INT_CLR1,
	REGISTER_INT_CLR2,

	REGISTER_GLOBAL_CTRL	= 0x4d,
	REGISTER_MT_ACTIVE,
	REGISTER_MT_ACTIVE_SET,
	REGISTER_MT_ACTIVE_CLR,
	REGISTER_MT_DBG_ACTIVE,
	REGISTER_MT_DBG_ACTIVE_SET,
	REGISTER_MT_EN,
	REGISTER_MT_HPRI,
	REGISTER_MT_HRT,
	REGISTER_MT_BREAK,
	REGISTER_MT_BREAK_CLR,
	REGISTER_MT_SINGLE_STEP,
	REGISTER_MT_MIN_DELAY_EN,
	REGISTER_MT_BREAK_SET,
	REGISTER_PERR_ADDR,
	REGISTER_DCAPT,

	REGISTER_MT_DBG_ACTIVE_CLR	= 0x5f,
	REGISTER_SCRATCHPAD0,
	REGISTER_SCRATCHPAD1,
	REGISTER_SCRATCHPAD2,
	REGISTER_SCRATCHPAD3,
	REGISTER_SCRATCHPAD4,
	REGISTER_SCRATCHPAD5,

	REGISTER_CHIP_CFG		= 0x68,
	REGISTER_MT_I_BLOCKED,
	REGISTER_MT_D_BLOCKED,
	REGISTER_MT_I_BLOCKED_SET,
	REGISTER_MT_D_BLOCKED_SET,
	REGISTER_MT_BLOCKED_CLR,
	REGISTER_MT_TRAP_EN,
	REGISTER_MT_TRAP,

	REGISTER_MT_TRAP_SET		= 0x70,
	REGISTER_MT_TRAP_CLR,
	REGISTER_SEP,
	REGISTER_MT_BTB_EN,
	REGISTER_BTB_ENTRIES,
	REGISTER_TNUM,

	REGISTER_I_RANGE0_HI		= 0x80,
	REGISTER_I_RANGE1_HI,
	REGISTER_I_RANGE2_HI,
	REGISTER_I_RANGE3_HI,

	REGISTER_I_RANGE0_LO		= 0x88,
	REGISTER_I_RANGE1_LO,
	REGISTER_I_RANGE2_LO,
	REGISTER_I_RANGE3_LO,

	REGISTER_I_RANGE0_EN		= 0x90,
	REGISTER_I_RANGE1_EN,
	REGISTER_I_RANGE2_EN,
	REGISTER_I_RANGE3_EN,

	REGISTER_D_RANGE0_HI		= 0x98,
	REGISTER_D_RANGE1_HI,
	REGISTER_D_RANGE2_HI,
	REGISTER_D_RANGE3_HI,
	REGISTER_D_RANGE4_HI,
	REGISTER_D_RANGE5_HI,

	REGISTER_D_RANGE0_LO		= 0xa0,
	REGISTER_D_RANGE1_LO,
	REGISTER_D_RANGE2_LO,
	REGISTER_D_RANGE3_LO,
	REGISTER_D_RANGE4_LO,

	REGISTER_D_RANGE0_EN		= 0xa8,
	REGISTER_D_RANGE1_EN,
	REGISTER_D_RANGE2_EN,
	REGISTER_D_RANGE3_EN,
	REGISTER_D_RANGE4_EN,

	REGISTER_I_RANGE0_USER_EN	= 0xb0,
	REGISTER_I_RANGE1_USER_EN,
	REGISTER_I_RANGE2_USER_EN,
	REGISTER_I_RANGE3_USER_EN,

	REGISTER_D_RANGE0_USER_EN	= 0xb8,
	REGISTER_D_RANGE1_USER_EN,
	REGISTER_D_RANGE2_USER_EN,
	REGISTER_D_RANGE3_USER_EN,
	REGISTER_D_RANGE4_USER_EN,
	REGISTER_D_RANGE5_USER_EN

} UBICOM32_REGISTER;

/* FIXME: Delete obsolete code. */
#if 0
/* you can call this signal handler from your SIGBUS and SIGSEGV
   signal handlers to inform the virtual CPU of exceptions. non zero
   is returned if the signal was handled by the virtual CPU.  */
int cpu_ubicom32_signal_handler(int host_signum, void *pinfo,
                           void *puc);
void cpu_ubicom32_flush_flags(CPUUBICOM32State *, int);

enum {
    CC_OP_DYNAMIC, /* Use env->cc_op  */
    CC_OP_FLAGS, /* CC_DEST = CVZN, CC_SRC = unused */
    CC_OP_LOGIC, /* CC_DEST = result, CC_SRC = unused */
    CC_OP_ADD,   /* CC_DEST = result, CC_SRC = source */
    CC_OP_SUB,   /* CC_DEST = result, CC_SRC = source */
    CC_OP_CMPB,  /* CC_DEST = result, CC_SRC = source */
    CC_OP_CMPW,  /* CC_DEST = result, CC_SRC = source */
    CC_OP_ADDX,  /* CC_DEST = result, CC_SRC = source */
    CC_OP_SUBX,  /* CC_DEST = result, CC_SRC = source */
    CC_OP_SHIFT, /* CC_DEST = result, CC_SRC = carry */
};

#define SR_I_SHIFT 8
#define SR_I  0x0700
#define SR_M  0x1000
#define SR_S  0x2000
#define SR_T  0x8000

#define UBICOM32_SSP    0
#define UBICOM32_USP    1

/* CACR fields are implementation defined, but some bits are common.  */
#define UBICOM32_CACR_EUSP  0x10

#define MACSR_PAV0  0x100
#define MACSR_OMC   0x080
#define MACSR_SU    0x040
#define MACSR_FI    0x020
#define MACSR_RT    0x010
#define MACSR_N     0x008
#define MACSR_Z     0x004
#define MACSR_V     0x002
#define MACSR_EV    0x001

void ubicom32_set_irq_level(CPUUBICOM32State *env, int level, uint8_t vector);
void ubicom32_set_macsr(CPUUBICOM32State *env, uint32_t val);
void ubicom32_switch_sp(CPUUBICOM32State *env);

#define UBICOM32_FPCR_PREC (1 << 6)

void do_ubicom32_semihosting(CPUUBICOM32State *env, int nr);


void cpu_ubicom32_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...));

void register_ubicom32_insns (CPUUBICOM32State *env);

#ifdef CONFIG_USER_ONLY
/* Linux uses 8k pages.  */
#define TARGET_PAGE_BITS 13
#else
/* Smallest TLB entry size is 1k.  */
#define TARGET_PAGE_BITS 10
#endif

#define cpu_init cpu_ubicom32_init
#define cpu_exec cpu_ubicom32_exec
#define cpu_gen_code cpu_ubicom32_gen_code
#define cpu_signal_handler cpu_ubicom32_signal_handler
#define cpu_list cpu_ubicom32_list

/* MMU modes definitions */
#define MMU_MODE0_SUFFIX _kernel
#define MMU_MODE1_SUFFIX _user
#define MMU_USER_IDX 1
static inline int cpu_mmu_index (CPUState *env)
{
    return (env->sr & SR_S) == 0 ? 1 : 0;
}

int cpu_ubicom32_handle_mmu_fault(CPUState *env, target_ulong address, int rw,
                              int mmu_idx, int is_softmmu);

#if defined(CONFIG_USER_ONLY)
static inline void cpu_clone_regs(CPUState *env, target_ulong newsp)
{
    if (newsp)
        env->aregs[7] = newsp;
    env->dregs[0] = 0;
}
#endif

#endif /* if 0 */
#endif /* CPU_UBICOM32_H */
