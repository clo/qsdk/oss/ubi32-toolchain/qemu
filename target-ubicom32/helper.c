/*
 *  ubicom32 helper defines
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Nat Gurumoorthy
 *  Modified by Michael Eager <eager@eagercon.com> based on OpenRISC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <string.h>

#include "config.h"
#include "cpu.h"
#include "exec-all.h"
#include "qemu-common.h"
#include "gdbstub.h"
#include "qemu-config.h"

#include "helper.h"

#define SIGNBIT (1u << 31)

struct ubicom32_board_t ubicom32_board_config = {
  /* IP 7K */
    .name		= "ip7k",
    .cpu_name		= "ubicom32v5",
    .chip_id		= 0x0003ffff,
    .hrt0_base		= 0x00000800,
    .hrt1_base		= 0x00000900,
    .nrt_base		= 0x00000a00,
    .flash_base		= 0x60000000,
    .flash_size 	= 0x00800000,
    .ocm_base		= 0x3ffc0000,
    .ocm_size		= 0x00040000,
    .ddr_base		= 0x40000000,
    .ddr_size		= 0x10000000,
    .ocp_base		= 0x01000000,
    .ocp_size		= 0x00001000,
    .timer_offset	= 0x00000100,
    .mailbox_offset	= 0x00000300,
    .mmu_offset		= 0x00000000,
    .pll_offset		= 0x00000000,
    .num_threads	= 1,
    .num_timers		= 12,
    .num_interrupts	= 64,
    .has_mmu 		= 0,
    .mapped_base	= 0,
    .mapped_size	= 0xb0000000,
    .reset_vector	= 0x60000000,
    .num_dranges	= 6,
    .num_iranges	= 4,
    .valid_insn		= NULL
};


/* Validate opcode for architecture version.  */
int ubicom32_validate_instruction (CPUUBICOM32State *env, unsigned int opcode)
{
    uint32_t *vtable = env->def->valid_insn;
    uint32_t major_opcode = (opcode >> 27) & 0x1f;
    uint32_t opcode_extension;

    switch (major_opcode) {
      case 0x00:	/* Format 1 */
      case 0x01:	/*    "     */
      case 0x1e:	/* Format 9 */
      case 0x1f:	/*    "     */
		opcode_extension = (opcode >> 11) & 0x1f;
		break;

      case 0x02:	/* Format 4 */
      case 0x06:	/* Format 10 */
      case 0x03:	/* Format 11  & 12 */
		opcode_extension = (opcode >> 21) & 0x1f;
		break;

      case 0x04: 	/* Format 2 */
      case 0x05:	/*    "     */
      case 0x07:	/*    "     */
      case 0x08:	/* Format 3 */
      case 0x09:	/*    "     */
      case 0x0a:	/*    "     */
      case 0x0b:	/*    "     */
      case 0x0c:	/*    "     */
      case 0x0d:	/*    "     */
      case 0x0e:	/*    "     */
      case 0x0f:	/*    "     */
      case 0x10:	/*    "     */
      case 0x11:	/*    "     */
      case 0x12:	/*    "     */
      case 0x13:	/*    "     */
      case 0x14:	/*    "     */
      case 0x15:	/*    "     */
      case 0x16:	/*    "     */
      case 0x17:	/*    "     */
      case 0x18:	/* Format 5 */
      case 0x19:	/* Format 6 */
      case 0x1a:	/* Format 7 */
      case 0x1b:	/* Format 8 */
      case 0x1c:	/*    "     */
      case 0x1d:	/*    "     */
		opcode_extension = 0;
		break;

      default:
		return 0;
    }

    return vtable[major_opcode] & (1 << opcode_extension);
}

int ubicom32_parse_mapfile_option (const char *name, const char *value, void *opaque)
{
    unsigned int larg = strtoul (value, NULL, 0);

    printf("  %s = \"%s\"\n", name, value);

    if (strcmp (name, "name") == 0)
      ubicom32_board_config.name = g_strdup(value);
    else if (strcmp (name, "cpu_name") == 0)
      ubicom32_board_config.cpu_name = g_strdup(value);
    else if (strcmp (name, "chip_id") == 0)
      ubicom32_board_config.chip_id = larg;
    else if (strcmp (name, "hrt0_base") == 0)
      ubicom32_board_config.hrt0_base = larg;
    else if (strcmp (name, "hrt1_base") == 0)
      ubicom32_board_config.hrt1_base = larg;
    else if (strcmp (name, "nrt_base") == 0)
      ubicom32_board_config.nrt_base = larg;
    else if (strcmp (name, "flash_base") == 0)
      ubicom32_board_config.flash_base = larg;
    else if (strcmp (name, "flash_size") == 0)
      ubicom32_board_config.flash_size = larg;
    else if (strcmp (name, "ocm_base") == 0)
      ubicom32_board_config.ocm_base = larg;
    else if (strcmp (name, "ocm_size") == 0)
      ubicom32_board_config.ocm_size = larg;
    else if (strcmp (name, "ddr_base") == 0)
      ubicom32_board_config.ddr_base = larg;
    else if (strcmp (name, "ddr_size") == 0)
      ubicom32_board_config.ddr_size = larg;
    else if (strcmp (name, "ocp_base") == 0)
      ubicom32_board_config.ocp_base = larg;
    else if (strcmp (name, "ocp_size") == 0)
      ubicom32_board_config.ocp_size = larg;
    else if (strcmp (name, "io_base") == 0)
      ubicom32_board_config.io_base = larg;
    else if (strcmp (name, "io_size") == 0)
      ubicom32_board_config.io_size = larg;
    else if (strcmp (name, "ether_base") == 0)
      ubicom32_board_config.ether_base = larg;
    else if (strcmp (name, "ether_size") == 0)
      ubicom32_board_config.ether_size = larg;
    else if (strcmp (name, "timer_offset") == 0)
      ubicom32_board_config.timer_offset = larg;
    else if (strcmp (name, "mailbox_offset") == 0)
      ubicom32_board_config.mailbox_offset = larg;
    else if (strcmp (name, "mmu_offset") == 0)
      ubicom32_board_config.mmu_offset = larg;
    else if (strcmp (name, "pll_offset") == 0)
      ubicom32_board_config.pll_offset = larg;
    else if (strcmp (name, "num_threads") == 0)
      ubicom32_board_config.num_threads = larg;
    else if (strcmp (name, "num_timers") == 0)
      ubicom32_board_config.num_timers = larg;
    else if (strcmp (name, "num_interrupts") == 0)
      ubicom32_board_config.num_interrupts = larg;
    else if (strcmp (name, "has_mmu") == 0)
      ubicom32_board_config.has_mmu = larg;
    else if (strcmp (name, "mapped_base") == 0)
      ubicom32_board_config.mapped_base = larg;
    else if (strcmp (name, "mapped_size") == 0)
      ubicom32_board_config.mapped_size = larg;
    else if (strcmp (name, "has_mmu") == 0)
      ubicom32_board_config.has_mmu = larg;
    else if (strcmp (name, "reset_vector") == 0)
      ubicom32_board_config.reset_vector = larg;
    else if (strcmp (name, "num_dranges") == 0)
      ubicom32_board_config.num_dranges = larg;
    else if (strcmp (name, "num_iranges") == 0)
      ubicom32_board_config.num_iranges = larg;
    else
    {
      printf ("Error:  unrecognized Ubicom32 configuration option: %s\n", name);
      return 1;
    }

    return 0;
}

void ubicom32_cpu_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...))
{
   (*cpu_fprintf)(f, "%s\n", ubicom32_board_config.name);
}


#if 0
#include "ubicom32_gdb.c"
#endif

#if 0
uint64_t HELPER(macmuls)(CPUState *env, uint32_t op1, uint32_t op2)
{
    int64_t product;
    int64_t res;

    product = (uint64_t)op1 * op2;
    res = (product << 24) >> 24;
    if (res != product) {
        env->macsr |= MACSR_V;
        if (env->macsr & MACSR_OMC) {
            /* Make sure the accumulate operation overflows.  */
            if (product < 0)
                res = ~(1ll << 50);
            else
                res = 1ll << 50;
        }
    }
    return res;
}

uint64_t HELPER(macmulu)(CPUState *env, uint32_t op1, uint32_t op2)
{
    uint64_t product;

    product = (uint64_t)op1 * op2;
    if (product & (0xffffffull << 40)) {
        env->macsr |= MACSR_V;
        if (env->macsr & MACSR_OMC) {
            /* Make sure the accumulate operation overflows.  */
            product = 1ll << 50;
        } else {
            product &= ((1ull << 40) - 1);
        }
    }
    return product;
}

uint64_t HELPER(macmulf)(CPUState *env, uint32_t op1, uint32_t op2)
{
    uint64_t product;
    uint32_t remainder;

    product = (uint64_t)op1 * op2;
    if (env->macsr & MACSR_RT) {
        remainder = product & 0xffffff;
        product >>= 24;
        if (remainder > 0x800000)
            product++;
        else if (remainder == 0x800000)
            product += (product & 1);
    } else {
        product >>= 24;
    }
    return product;
}

void HELPER(macsats)(CPUState *env, uint32_t acc)
{
    int64_t tmp;
    int64_t result;
    tmp = env->macc[acc];
    result = ((tmp << 16) >> 16);
    if (result != tmp) {
        env->macsr |= MACSR_V;
    }
    if (env->macsr & MACSR_V) {
        env->macsr |= MACSR_PAV0 << acc;
        if (env->macsr & MACSR_OMC) {
            /* The result is saturated to 32 bits, despite overflow occuring
               at 48 bits.  Seems weird, but that's what the hardware docs
               say.  */
            result = (result >> 63) ^ 0x7fffffff;
        }
    }
    env->macc[acc] = result;
}

void HELPER(macsatu)(CPUState *env, uint32_t acc)
{
    uint64_t val;

    val = env->macc[acc];
    if (val & (0xffffull << 48)) {
        env->macsr |= MACSR_V;
    }
    if (env->macsr & MACSR_V) {
        env->macsr |= MACSR_PAV0 << acc;
        if (env->macsr & MACSR_OMC) {
            if (val > (1ull << 53))
                val = 0;
            else
                val = (1ull << 48) - 1;
        } else {
            val &= ((1ull << 48) - 1);
        }
    }
    env->macc[acc] = val;
}

void HELPER(macsatf)(CPUState *env, uint32_t acc)
{
    int64_t sum;
    int64_t result;

    sum = env->macc[acc];
    result = (sum << 16) >> 16;
    if (result != sum) {
        env->macsr |= MACSR_V;
    }
    if (env->macsr & MACSR_V) {
        env->macsr |= MACSR_PAV0 << acc;
        if (env->macsr & MACSR_OMC) {
            result = (result >> 63) ^ 0x7fffffffffffll;
        }
    }
    env->macc[acc] = result;
}

void HELPER(mac_set_flags)(CPUState *env, uint32_t acc)
{
    uint64_t val;
    val = env->macc[acc];
    if (val == 0)
        env->macsr |= MACSR_Z;
    else if (val & (1ull << 47));
        env->macsr |= MACSR_N;
    if (env->macsr & (MACSR_PAV0 << acc)) {
        env->macsr |= MACSR_V;
    }
    if (env->macsr & MACSR_FI) {
        val = ((int64_t)val) >> 40;
        if (val != 0 && val != -1)
            env->macsr |= MACSR_EV;
    } else if (env->macsr & MACSR_SU) {
        val = ((int64_t)val) >> 32;
        if (val != 0 && val != -1)
            env->macsr |= MACSR_EV;
    } else {
        if ((val >> 32) != 0)
            env->macsr |= MACSR_EV;
    }
}

void HELPER(flush_flags)(CPUState *env, uint32_t cc_op)
{
    cpu_ubicom32_flush_flags(env, cc_op);
}

uint32_t HELPER(get_macf)(CPUState *env, uint64_t val)
{
    int rem;
    uint32_t result;

    if (env->macsr & MACSR_SU) {
        /* 16-bit rounding.  */
        rem = val & 0xffffff;
        val = (val >> 24) & 0xffffu;
        if (rem > 0x800000)
            val++;
        else if (rem == 0x800000)
            val += (val & 1);
    } else if (env->macsr & MACSR_RT) {
        /* 32-bit rounding.  */
        rem = val & 0xff;
        val >>= 8;
        if (rem > 0x80)
            val++;
        else if (rem == 0x80)
            val += (val & 1);
    } else {
        /* No rounding.  */
        val >>= 8;
    }
    if (env->macsr & MACSR_OMC) {
        /* Saturate.  */
        if (env->macsr & MACSR_SU) {
            if (val != (uint16_t) val) {
                result = ((val >> 63) ^ 0x7fff) & 0xffff;
            } else {
                result = val & 0xffff;
            }
        } else {
            if (val != (uint32_t)val) {
                result = ((uint32_t)(val >> 63) & 0x7fffffff);
            } else {
                result = (uint32_t)val;
            }
        }
    } else {
        /* No saturation.  */
        if (env->macsr & MACSR_SU) {
            result = val & 0xffff;
        } else {
            result = (uint32_t)val;
        }
    }
    return result;
}

uint32_t HELPER(get_macs)(uint64_t val)
{
    if (val == (int32_t)val) {
        return (int32_t)val;
    } else {
        return (val >> 61) ^ ~SIGNBIT;
    }
}

uint32_t HELPER(get_macu)(uint64_t val)
{
    if ((val >> 32) == 0) {
        return (uint32_t)val;
    } else {
        return 0xffffffffu;
    }
}

uint32_t HELPER(get_mac_extf)(CPUState *env, uint32_t acc)
{
    uint32_t val;
    val = env->macc[acc] & 0x00ff;
    val = (env->macc[acc] >> 32) & 0xff00;
    val |= (env->macc[acc + 1] << 16) & 0x00ff0000;
    val |= (env->macc[acc + 1] >> 16) & 0xff000000;
    return val;
}

uint32_t HELPER(get_mac_exti)(CPUState *env, uint32_t acc)
{
    uint32_t val;
    val = (env->macc[acc] >> 32) & 0xffff;
    val |= (env->macc[acc + 1] >> 16) & 0xffff0000;
    return val;
}

void HELPER(set_mac_extf)(CPUState *env, uint32_t val, uint32_t acc)
{
    int64_t res;
    int32_t tmp;
    res = env->macc[acc] & 0xffffffff00ull;
    tmp = (int16_t)(val & 0xff00);
    res |= ((int64_t)tmp) << 32;
    res |= val & 0xff;
    env->macc[acc] = res;
    res = env->macc[acc + 1] & 0xffffffff00ull;
    tmp = (val & 0xff000000);
    res |= ((int64_t)tmp) << 16;
    res |= (val >> 16) & 0xff;
    env->macc[acc + 1] = res;
}

void HELPER(set_mac_exts)(CPUState *env, uint32_t val, uint32_t acc)
{
    int64_t res;
    int32_t tmp;
    res = (uint32_t)env->macc[acc];
    tmp = (int16_t)val;
    res |= ((int64_t)tmp) << 32;
    env->macc[acc] = res;
    res = (uint32_t)env->macc[acc + 1];
    tmp = val & 0xffff0000;
    res |= (int64_t)tmp << 16;
    env->macc[acc + 1] = res;
}

void HELPER(set_mac_extu)(CPUState *env, uint32_t val, uint32_t acc)
{
    uint64_t res;
    res = (uint32_t)env->macc[acc];
    res |= ((uint64_t)(val & 0xffff)) << 32;
    env->macc[acc] = res;
    res = (uint32_t)env->macc[acc + 1];
    res |= (uint64_t)(val & 0xffff0000) << 16;
    env->macc[acc + 1] = res;
}
#endif
