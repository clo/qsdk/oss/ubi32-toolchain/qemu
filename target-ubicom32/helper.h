/*
 *  ubicom32 helper function defines
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Nat Gurumoorthy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
#include "def-helper.h"

DEF_HELPER_2(src1_direct, void, env, i32)
DEF_HELPER_2(src1_64_direct, void, env, i32)
DEF_HELPER_2(src2_64_direct, void, env, i32)
DEF_HELPER_2(dest_direct, void, env, i32)
DEF_HELPER_2(src1_check, void, env, i32)
DEF_HELPER_2(dest_check, void, env, i32)
DEF_HELPER_1(raise_exception, void, i32)
DEF_HELPER_3(sub, void, env, i32, i32)
DEF_HELPER_3(add, void, env, i32, i32)
DEF_HELPER_3(bfrvrs, void, env, i32, i32)
DEF_HELPER_3(jmpcc, void, env, i32, i32)
DEF_HELPER_2(nzflag, void, env, i32)
DEF_HELPER_3(btst_clr_set, void, env, i32, i32)
DEF_HELPER_1(suspend, void, env)
DEF_HELPER_1(syscall, void, env)
DEF_HELPER_1(simcall, void, env)
DEF_HELPER_1(bkpt, void, env)
DEF_HELPER_1(finish_multiply, void, env)
DEF_HELPER_1(crcgen, void, env)
DEF_HELPER_1(iprivilege_check, void, env)
DEF_HELPER_2(debug2, void, env, i32)
DEF_HELPER_2(debug3, void, env, i32)
DEF_HELPER_2(direct_register_privilege_check, void, env, i32)
DEF_HELPER_3(address_check, void, env, i32, i32)
DEF_HELPER_2(dest_fpd32, void, env, i32)
DEF_HELPER_2(dest_fpd64, void, env, i32)
DEF_HELPER_2(pxvi, void, env, i32)
DEF_HELPER_2(pxblend, void, env, i32)
DEF_HELPER_2(pxadds, void, env, i32)
DEF_HELPER_2(pxcnv, void, env, i32)
DEF_HELPER_2(pxhi, void, env, i32)
DEF_HELPER_1(fadds, void, env)
DEF_HELPER_1(fsubs, void, env)
DEF_HELPER_1(fmuls, void, env)
DEF_HELPER_1(fdivs, void, env)
DEF_HELPER_1(fi2d, void, env)
DEF_HELPER_1(fs2d, void, env)
DEF_HELPER_1(fs2l, void, env)
DEF_HELPER_1(fsqrts, void, env)
DEF_HELPER_1(fnegs, void, env)
DEF_HELPER_1(fabss, void, env)
DEF_HELPER_1(fi2s, void, env)
DEF_HELPER_1(fs2i, void, env)
DEF_HELPER_1(fcmps, void, env)
DEF_HELPER_1(faddd, void, env)
DEF_HELPER_1(fsubd, void, env)
DEF_HELPER_1(fmuld, void, env)
DEF_HELPER_1(fdivd, void, env)
DEF_HELPER_1(fl2s, void, env)
DEF_HELPER_1(fd2s, void, env)
DEF_HELPER_1(fd2i, void, env)
DEF_HELPER_1(fsqrtd, void, env)
DEF_HELPER_1(fnegd, void, env)
DEF_HELPER_1(fabsd, void, env)
DEF_HELPER_1(fl2d, void, env)
DEF_HELPER_1(fd2l, void, env)
DEF_HELPER_1(fcmpd, void, env)
DEF_HELPER_1(debug, void, env)
DEF_HELPER_2(mulf, void, env, i32)
DEF_HELPER_2(muls, void, env, i32)
DEF_HELPER_2(mulu, void, env, i32)
DEF_HELPER_2(macs, void, env, i32)
DEF_HELPER_2(macf, void, env, i32)
DEF_HELPER_2(msuf, void, env, i32)
DEF_HELPER_2(macu, void, env, i32)
DEF_HELPER_2(macus, void, env, i32)
DEF_HELPER_2(madd2, void, env, i32)
DEF_HELPER_2(msub2, void, env, i32)
DEF_HELPER_2(muls4, void, env, i32)
DEF_HELPER_2(mulu4, void, env, i32)
DEF_HELPER_2(macu4, void, env, i32)
DEF_HELPER_2(macs4, void, env, i32)
DEF_HELPER_2(madd4, void, env, i32)
DEF_HELPER_2(msub4, void, env, i32)

#include "def-helper.h"
