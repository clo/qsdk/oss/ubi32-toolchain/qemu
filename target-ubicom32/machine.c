/*
 *  UBICOM32 Machine
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Natarajan Gurumoorthy
 *  Modified by Michael Eager <eager@eagercon.com> based on OpenRISC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "hw/hw.h"
#include "hw/boards.h"

static const VMStateDescription vmstate_cpu = {
    .name = "cpu",
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(thread_regs, CPUUBICOM32State, 64),
        VMSTATE_UINT32(pc, CPUUBICOM32State),
        VMSTATE_UINT32(thread_number, CPUUBICOM32State),
        VMSTATE_UINT32(cc_sr_index, CPUUBICOM32State),
        VMSTATE_END_OF_LIST()
    }
};

void cpu_save(QEMUFile *f, void *opaque)
{
    vmstate_save_state(f, &vmstate_cpu, opaque);
}

int cpu_load(QEMUFile *f, void *opaque, int version_id)
{
    return vmstate_load_state(f, &vmstate_cpu, opaque, version_id);
}
