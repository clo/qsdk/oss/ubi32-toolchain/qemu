/*
 *  UBICOM32 MMU
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Natarajan Gurumoorthy
 *  Modified by Michael Eager <eager@eagercon.com> based on OpenRISC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "cpu.h"
#include "qemu-common.h"
#include "gdbstub.h"
#include "host-utils.h"
#ifndef CONFIG_USER_ONLY
#include "hw/loader.h"
#endif

#ifndef CONFIG_USER_ONLY
target_phys_addr_t cpu_get_phys_page_debug(CPUUBICOM32State *env,
                                           target_ulong addr)
{
    return addr;
}

void cpu_ubicom32_mmu_init(UBICOM32CPU *cpu)
{
}

/*
 * We have to fix this routine when 8k mmu is implemented.
 */
int cpu_ubicom32_handle_mmu_fault (CPUUBICOM32State *env, target_ulong address, 
			           int rw, int mmu_idx, int is_softmmu)
{
    int prot;

    address &= TARGET_PAGE_MASK;
    prot = PAGE_READ | PAGE_WRITE;
    tlb_set_page(env, address, address, prot | PAGE_EXEC, mmu_idx, 
	         TARGET_PAGE_SIZE);
    return 0;
}
#endif

