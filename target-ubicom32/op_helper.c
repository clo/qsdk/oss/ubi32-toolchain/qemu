/*
 *  UBICOM32 helper routines
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Nat Gurumoorthy
 *  Modified by Michael Eager <eager@eagercon.com> based on OpenRISC
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdlib.h>

#include "cpu.h"
#include "dyngen-exec.h"
#include "helper.h"
#include "sysemu.h"
#include "softmmu-semi.h"
#include "qemu-debug.h"

// Causes problems with some tests
//#define FIX_NAN

#if defined(CONFIG_USER_ONLY)

void do_interrupt(CPUUBICOM32State *env)
{
    printf("In interrupt\n");
    env->exception_index = -1;
}

#else

void do_interrupt(CPUUBICOM32State *env)
{
    env->exception_index = -1;
}
#endif


static void raise_exception_with_env(CPUUBICOM32State *env, int tt)
{
    env->exception_index = tt;
    cpu_loop_exit(env);
}

static void raise_exception(int tt)
{
    env->exception_index = tt;
    cpu_loop_exit(env);
}

void HELPER(raise_exception)(uint32_t tt)
{
    raise_exception(tt);
}

#define SRC_THREAD_SELECT (1 << 8)
#define SRC_THREAD_MASK (0x1f << 9)
#define DEST_THREAD_SELECT (1 <<14)
#define DEST_THREAD_MASK (0x1f << 15)
#define CSR_INDEX 0x2d

void HELPER(src1_direct)(CPUUBICOM32State *env, uint32_t register_index)
{
    CPUUBICOM32State *src_env = env;
    if (register_index >= 0x40) {
	register_index -= 0x40;
	
	/*
	 * Move the Global register into src1_data.
	 */
	env->src1_data = env->global_regs[register_index];
	return;
    }
    
    /*
     * Check CSR and see if SRC_THREAD_SELECT bit is set.
     */
    if (env->thread_regs[CSR_INDEX] & SRC_THREAD_SELECT) {
	/*
	 * We have to pull out the src thread number and go read that threads register and move it into src1_data.
	 */
	int src_thread = (env->thread_regs[CSR_INDEX] & SRC_THREAD_MASK) >> 9;
	src_env = threads[src_thread];
    }

    /*
     * Just move the per thread register into src1_data and get out.
     */
    env->src1_data = src_env->thread_regs[register_index];
}

/*
** void HELPER(src1_64_direct)(CPUUBICOM32State *env, uint32_t register_index)
**
** Handles a 4 bit source1 encoding where:
**
** 0-7 = data register pairs D0/D1, D2/D3, D4/D5, etc
** 8: ACC0_LO/ACC0_HI (aka MAC_LO/MAC_HI)
** 9: ACC1_LO/ACC1_HI
*/

void HELPER(src1_64_direct)(CPUUBICOM32State *env, uint32_t register_index)
{
	CPUUBICOM32State *src_env = env;

	register_index <<= 1;

	if (register_index >= 16) {

		switch (register_index - 16) {

			case 0:
				register_index = 0x28;		/* ACC0_LO (MAC_LO) */
				break;

			case 2:
				register_index = 0x36;		/* ACC1_HI */
				break;
		}
	}

//	printf("gen_helper_src1_64_direct(): source register 0x%x\n", register_index);
    
	/* Check CSR and see if SRC_THREAD_SELECT bit is set. */

	if (env->thread_regs[CSR_INDEX] & SRC_THREAD_SELECT) {

		/* We have to pull out the src thread number and go read that threads register and move it into src1_data. */

		int src_thread = (env->thread_regs[CSR_INDEX] & SRC_THREAD_MASK) >> 9;
		src_env = threads[src_thread];
	}

	/* Just move the per thread register into src1_data and get out. */

	env->src1_64 = ((long long)src_env->thread_regs[register_index] << 32) | src_env->thread_regs[register_index + 1];
}

void HELPER(src2_64_direct)(CPUUBICOM32State *env, uint32_t register_index)
{
	register_index <<= 1;

	if (register_index >= 16) {

		switch (register_index - 16) {

			case 0:
				register_index = 0x28;		/* ACC0_HI (MAC_HI) */
				break;

			case 2:
				register_index = 0x36;		/* ACC1_HI */
				break;
		}
	}
    
	/* Move the data into env->src2_64. */

	env->src2_64 = ((long long)env->thread_regs[register_index] << 32) | env->thread_regs[register_index + 1];
}

static void bale_out(CPUUBICOM32State *env)
{
    if(env->postinc_areg) {
	/*
	 * Do the postinc.
	 */
	env->thread_regs[env->postinc_areg] += env->increment;
	env->postinc_areg = 0;
    }

    /*
     * PREVIOUS_PC = PC, PC = PC + 4
     */
    env->thread_regs[0x38] = env->current_pc;
    env->thread_regs[0x34] = env->current_pc + 4;
    raise_exception(EXCP_INTERRUPT);
}

static void bale_out_dbg(CPUUBICOM32State *env)
{
    if(env->postinc_areg) {
	/*
	 * Do the postinc.
	 */
	env->thread_regs[env->postinc_areg] += env->increment;
	env->postinc_areg = 0;
    }

    /*
     * PREVIOUS_PC = PC, PC = PC + 4
     */
    env->thread_regs[0x38] = env->current_pc;
    env->thread_regs[0x34] = env->current_pc + 4;
    raise_exception(EXCP_DEBUG);
}

void HELPER(dest_direct)(CPUUBICOM32State *env, uint32_t register_index)
{
    CPUUBICOM32State *dest_env = env;
    if (register_index >= 0x40) {
	uint32_t r_index = register_index - 0x40;
	uint32_t threads_mask = (1 <<env->def->num_threads) -1;
	int i;
	
	switch(register_index) {
	case 0x45: /* int_set0 */
	case 0x46: /* int_set1 */
	case 0x47: /* int_set2 */
	case 0x48: /* int_set3 */
	    env->global_regs[(register_index - 0x45) + GLOBAL_REG(0x41)] |= env->dest_data;
	    
	    /*
	     * See if interrupts are enabled. If they are not we are done.
	     */
	    if ((env->global_regs[GLOBAL_REG(0x4d)] & 1) == 0) {
		break;
	    }

	    /*
	     * Scan through all the contexts and wake up all suspended contexts.
	     */
	    for (i=0; i< env->def->num_threads; i++) {
		uint32_t mask_value = threads[i]->thread_regs[register_index - 0x45 + 0x30];
		
		/*
		 * And the per thread int_mask register with the correspoding int_stat register. If any
		 * bit are alive then set the bit for this thread in mt_active.
		 */
		if (mask_value & env->global_regs[(register_index - 0x45) + GLOBAL_REG(0x41)]) {
		    /*
		     * Turn on the bit for this thread in mt_active.
		     */
		    env->global_regs[GLOBAL_REG(0x4e)] |= (1 << i);
		}
	    }
	    break;
	case 0x49: /* int_clr 0 */
	case 0x4a: /* int_clr 1 */
	case 0x4b: /* int_clr 2 */
	case 0x4c: /* int_clr 3 */
	    env->global_regs[(register_index - 0x49) + GLOBAL_REG(0x41)] &= ~env->dest_data;
	    break;
	case 0x4f: /* mt_active_set */
	    env->global_regs[GLOBAL_REG(0x4e)] |= env->dest_data;
	    break;
	case 0x50: /* mt_active_clr */
	    env->global_regs[GLOBAL_REG(0x4e)] &= ~env->dest_data;
	    bale_out(env);
	    break;
	case 0x52: /* mt_dbg_active_set */
	    env->global_regs[GLOBAL_REG(0x51)] |= env->dest_data;
	    break;
	case 0x5f: /* mt_dbg_active_clr */
	    env->global_regs[GLOBAL_REG(0x51)] &= ~env->dest_data;
	    
	    /*
	     * If all the threads have been stopped and scratchpad3 is 1 (debuger attached)
	     * then set the DEBUG exception and the debugger will gain control.
	     */
	    if (((env->global_regs[GLOBAL_REG(0x51)] & threads_mask) == 0) && (env->global_regs[GLOBAL_REG(0x63)] & 0x1)) {
		bale_out_dbg (env);
	    } else {
		bale_out(env);
	    }
	    break;
	case 0x5a: /* mt_break_set */
	    env->global_regs[GLOBAL_REG(0x56)] |= env->dest_data;
	    break;
	case 0x57: /* mt_break_clr */
	    env->global_regs[GLOBAL_REG(0x56)] &= ~env->dest_data;
	    break;
	case 0x70: /* mt_trap_set */
	    env->global_regs[GLOBAL_REG(0x6f)] |= env->dest_data;
	    bale_out(env);
	    break;
	case 0x71: /* mt_trap_clr */
	    env->global_regs[GLOBAL_REG(0x6f)] &= ~env->dest_data;
	    break;
	case 0x53: /* mt_en */
	    //printf("MT_EX modified by %d old val 0x%03x new val 0x%03x\n", env->cpu_index, env->global_regs[r_index], env->dest_data);
	    env->global_regs[r_index] = env->dest_data;
	    bale_out(env);
	case 0x61: /* sp1 */
	    {
		env->global_regs[r_index] = env->dest_data;
		break;
	    }

	default:
	    /*
	     * Move dest_data the into Global register 
	     */
	    env->global_regs[r_index] = env->dest_data;
	}
	return;
    }

    /*
     * Check CSR and see if DEST_THREAD_SELECT bit is set.
     */
    if (env->thread_regs[CSR_INDEX] & DEST_THREAD_SELECT) {
	/*
	 * We have to pull out the src thread number and go read that threads register and move it into src1_data.
	 */
	int dest_thread = (env->thread_regs[CSR_INDEX] & DEST_THREAD_MASK) >> 15;
	dest_env = threads[dest_thread];
    }
  
    /*
     * Just move the per thread register into src1_data and get out.
     */
    dest_env->thread_regs[register_index] = env->dest_data;
}

/*
** This helper is used by floating-point functions which return a result
** in one of the four 32-bit accumulator registers ACC0_LO, ACC0_HI, ACC1_LO, ACC1_HI.
*/

void HELPER(dest_fpd32)(CPUUBICOM32State *env, uint32_t register_index)
{
	CPUUBICOM32State *dest_env = env;

	/* Check CSR and see if DEST_THREAD_SELECT bit is set. */

	if (env->thread_regs[CSR_INDEX] & DEST_THREAD_SELECT) {
		int dest_thread = (env->thread_regs[CSR_INDEX] & DEST_THREAD_MASK) >> 15;
		dest_env = threads[dest_thread];
	}

	switch (register_index) {

		case 0:		/* ACC0_LO */

			dest_env->thread_regs[0x29] = env->dest_data;
			break;

		case 1:		/* ACC0_HI */

			dest_env->thread_regs[0x28] = env->dest_data;
			break;

		case 2:		/* ACC1_LO */

			dest_env->thread_regs[0x37] = env->dest_data;
			break;

		case 3:		/* ACC1_HI */

			dest_env->thread_regs[0x36] = env->dest_data;
			break;
	}
}

/*
** This helper is used by floating-point functions which return a result
** in one of the two accumulator registers ACC0, ACC1.
*/

void HELPER(dest_fpd64)(CPUUBICOM32State *env, uint32_t register_index)
{
	CPUUBICOM32State *dest_env = env;

	/* Check CSR and see if DEST_THREAD_SELECT bit is set. */

	if (env->thread_regs[CSR_INDEX] & DEST_THREAD_SELECT) {
		printf("dest thread is not current thread\n");
		int dest_thread = (env->thread_regs[CSR_INDEX] & DEST_THREAD_MASK) >> 15;
		dest_env = threads[dest_thread];
	}

//	printf("dest_fpd64(): storing %llx at index: %d\n", env->dest_64, register_index);

	switch (register_index) {

		case 0:		/* ACC0 */

			dest_env->thread_regs[0x28] = env->dest_64 >> 32;
			dest_env->thread_regs[0x29] = env->dest_64;
			break;

		case 1:		/* ACC1 */

			dest_env->thread_regs[0x36] = env->dest_64 >> 32;
			dest_env->thread_regs[0x37] = env->dest_64;
			break;
	}
}

void generate_trap(CPUUBICOM32State *env, uint32_t thread_no, uint32_t trap_no) 
{
    uint32_t thread_mask = 1 << thread_no;
    uint32_t trap_cause = 1 << trap_no;
    
    if (qemu_debug & DEBUG_TRAP)
	printf ("generate_trap (thread_no=%d, trap_no=%d)\n", thread_no, trap_no);
    assert (thread_no < 12);
    assert (trap_no < 15);

    /*
     * Clear the MT_DBG_ACTIVE bit for this thread. Set the thread bit
     * in the MT_TRAP register. Set the trap interrupt bit. Set the trap cause register.
     */
    env->thread_regs[0x34] = env->current_pc;
    env->global_regs[GLOBAL_REG(0x51)] &= ~thread_mask;
    env->global_regs[GLOBAL_REG(0x6f)] |= thread_mask;
    
    env->thread_regs[0x35] |= trap_cause;

    if (semihosting_enabled) {
#if 0
	/* FIXME -- signal gdb. */
	if (use_gdb_syscalls()) {
	    gdb_signalled(env, trap_no);
	} else 
#endif
	{
            /* Force QEMU shutdown on trap rather than emulate it. */
	    printf ("exiting with trap %d\n", trap_no);
	    exit(-1);
	}
    }
    
    if (env->global_regs[GLOBAL_REG(0x6e)] & thread_mask) {
	/*
	 * Trap is enabled. Post an interrupt. Bit 31 in int_stat1
	 */
	ubicom32_raise_interrupt(32+31);
    }	
}

uint32_t ip7k_data_range_check(CPUUBICOM32State *env, uint32_t address)
{
    int i;
    uint32_t *ptr_hi = &env->global_regs[GLOBAL_REG(0x98)];
    uint32_t *ptr_lo = &env->global_regs[GLOBAL_REG(0xa0)];
    uint32_t *ptr_en = &env->global_regs[GLOBAL_REG(0xa8)];
    uint32_t thread_mask = 1 << env->cpu_index;
    uint32_t num_dranges = env->def->num_dranges;
    
    /*
     * Ethernet check.
     */
    if(ubicom32_range_check(env->def->ether_base, env->def->ether_size, address)) {
	return 1;
    }

    for (i = 0; i < num_dranges; i++, ptr_en++, ptr_hi++, ptr_lo++) {
	if (*ptr_en & thread_mask) {
	    /*
	     * This range is valid for this thread.
	     */
	    if (ubicom32_range_check(*ptr_lo, *ptr_hi - *ptr_lo, address)) {
		/*
		 * Valid range. Return.
		 */
		return 1;
	    }
	}
    }
    
    return 0;
}

uint32_t ip7k_instruction_range_check(CPUUBICOM32State *env, uint32_t address)
{
    int i;
    uint32_t *ptr_hi = &env->global_regs[GLOBAL_REG(0x80)];
    uint32_t *ptr_lo = &env->global_regs[GLOBAL_REG(0x88)];
    uint32_t *ptr_en = &env->global_regs[GLOBAL_REG(0x90)];
    uint32_t thread_mask = 1 << env->cpu_index;
    uint32_t num_iranges = env->def->num_iranges;
    
    for (i = 0; i < num_iranges; i++, ptr_en++, ptr_hi++, ptr_lo++) {
	if (*ptr_en & thread_mask) {
	    /*
	     * This range is valid for this thread.
	     */
	    if (ubicom32_range_check(*ptr_lo, *ptr_hi - *ptr_lo, address)) {
		/*
		 * Valid range. Return.
		 */
		return 1;
	    }
	}
    }
    
    return 0;
}

uint32_t ip7k_hole_check(CPUUBICOM32State *env, uint32_t address)
{

    /*
     * Ethernet check.
     */
    if(ubicom32_range_check(env->def->ether_base, env->def->ether_size, address)) {
	return 1;
    }

    /*
     * Check HRT0 range
     */
    if(ubicom32_range_check(env->def->hrt0_base, 0x3f, address)) {
	return 1;
    }
    
    /*
     * Check HRT1 range
     */
    if(ubicom32_range_check(env->def->hrt1_base, 0x3f, address)) {
	return 1;
    }

    /*
     * Check NRT range
     */
    if(ubicom32_range_check(env->def->nrt_base, 0x3f, address)) {
	return 1;
    }

    /*
     * Check range OCP block
     */
    if(ubicom32_range_check(env->def->ocp_base, env->def->ocp_size, address)) {
	return 1;
    }

    /*
     * Check range I/O block
     */
    if(ubicom32_range_check(0x0200000, 0x10000, address)) {
	return 1;
    }
    
    /*
     * Check range OCM
     */
    if(ubicom32_range_check(env->def->ocm_base, env->def->ocm_size, address)) {
	return 1;
    }
    
    /*
     * Check range DDR
     */
    if(ubicom32_range_check(env->def->ddr_base, env->def->ddr_size, address)) {
	return 1;
    }
    
    /*
     * Check range Flash
     */
    if(ubicom32_range_check(env->def->flash_base, env->def->flash_size, address)) {
	return 1;
    }

    /*
     * Check the mapped range.
     */
    if(ubicom32_range_check(env->def->mapped_base, env->def->mapped_size, address)) {
	return 1;
    }

    return 0;
}

uint32_t ip8k_data_range_check(CPUUBICOM32State *env, uint32_t address)
{
    int i;
    uint32_t *ptr_hi = &env->global_regs[GLOBAL_REG(0x98)];
    uint32_t *ptr_lo = &env->global_regs[GLOBAL_REG(0xa0)];
    uint32_t *ptr_en = &env->global_regs[GLOBAL_REG(0xa8)];
    uint32_t thread_mask = 1 << env->cpu_index;
    uint32_t num_dranges = env->def->num_dranges;


    /* If traps are disabled for this thread, then the range registers are not active,
       and all memory is accessible. */

    if (!(env->global_regs[REGISTER_MT_TRAP_EN] & (1 << env->thread_number)))
	return 1;

    
    /*
     * If the thread is in user mode the enables become the _user_en registers.
     */
    if (env->thread_regs[0x2d] & (1 << CSR_PRIV_BIT)) {
	ptr_en = &env->global_regs[GLOBAL_REG(0xb8)];
    }

    for (i = 0; i < num_dranges; i++, ptr_en++, ptr_hi++, ptr_lo++) {
	if (*ptr_en & thread_mask) {
	    /*
	     * This range is valid for this thread.
	     */

	    if (ubicom32_range_check(*ptr_lo, *ptr_hi - *ptr_lo, address)) {
		/*
		 * Valid range. Return.
		 */
		return 1;
	    }
	}
    }

    return 0;
}

uint32_t ip8k_instruction_range_check(CPUUBICOM32State *env, uint32_t address)
{
    int i;
    uint32_t *ptr_hi = &env->global_regs[GLOBAL_REG(0x80)];
    uint32_t *ptr_lo = &env->global_regs[GLOBAL_REG(0x88)];
    uint32_t *ptr_en = &env->global_regs[GLOBAL_REG(0x90)];
    uint32_t thread_mask = 1 << env->cpu_index;
    uint32_t num_iranges = env->def->num_iranges;
    
    /*
     * If the thread is in user mode the enables become the _user_en registers.
     */
    if (env->thread_regs[0x2d] & (1 << CSR_PRIV_BIT)) {
	ptr_en = &env->global_regs[GLOBAL_REG(0xb0)];
    }

    for (i = 0; i < num_iranges; i++, ptr_en++, ptr_hi++, ptr_lo++) {
	if (*ptr_en & thread_mask) {
	    /*
	     * This range is valid for this thread.
	     */
	    if (ubicom32_range_check(*ptr_lo, *ptr_hi - *ptr_lo, address)) {
		/*
		 * Valid range. Return.
		 */
		return 1;
	    }
	}
    }
    
    return 0;
}


/*
 * XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx
 * For the time being we are going to use this range check which is really the 7k.
 * Maybe we should change this and do it via env->def.
 */
uint32_t ip8k_hole_check(CPUUBICOM32State *env, uint32_t address)
{
    /*
     * Check HRT0 range
     */
    if(ubicom32_range_check(env->def->hrt0_base, 0x3f, address)) {
	return 1;
    }
    
    /*
     * Check HRT1 range
     */
    if(ubicom32_range_check(env->def->hrt1_base, 0x3f, address)) {
	return 1;
    }

    /*
     * Check NRT range
     */
    if(ubicom32_range_check(env->def->nrt_base, 0x3cf, address)) {
	return 1;
    }

    /*
     * Check range OCP block
     */
    if(ubicom32_range_check(env->def->ocp_base, env->def->ocp_size, address)) {
	return 1;
    }

    /*
     * Check range I/O block
     */
    if(ubicom32_range_check(env->def->io_base, env->def->io_size, address)) {
	return 1;
    }
    
    /*
     * Check range OCM
     */
    if(ubicom32_range_check(env->def->ocm_base, env->def->ocm_size, address)) {
	return 1;
    }
    
    /*
     * Check range DDR
     */
    if(ubicom32_range_check(env->def->ddr_base, env->def->ddr_size, address)) {
	return 1;
    }
    
    /*
     * Check range Flash
     */
    if(ubicom32_range_check(env->def->flash_base, env->def->flash_size, address)) {
	return 1;
    }

    /*
     * Check the mapped range.
     */
    if(ubicom32_range_check(env->def->mapped_base, env->def->mapped_size, address)) {
	return 1;
    }

    return 0;
}

enum {
    UBICOM32_NO_ERROR,
    UBICOM32_RANGE_ERROR,
    UBICOM32_INVALID_ADDRESS,
    UBICOM32_MAPPED_ADDRESS,
    UBICOM32_MISALIGNED_ERROR,
};

/*
 * src1_addr is already loaded with an address. We have to test for alignment and 
 * also do range checking and generate traps as needed.
 */
static uint32_t src1_check(CPUUBICOM32State *env, uint32_t opsize)
{
    uint32_t mask = opsize -1;
    uint32_t thread_mask = 1 << env->cpu_index;
   
	if (env->global_regs[REGISTER_MT_TRAP_EN] & thread_mask) {

		/* Do alignment check first. */

		if (env->src1_address & mask) {
			generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_MISALIGNED);
			return UBICOM32_MISALIGNED_ERROR;
		}
	}

    /*
     * Do range checks.
     */
    if (env->global_regs[0] == 0x0003ffff) {
	if (ip7k_data_range_check(env, env->src1_address) == 0) {
	    /*
	     * Not in any valid range. Triger SRC1_RANGE_ERR trap
	     */
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_RANGE_ERR);
	    return UBICOM32_RANGE_ERROR;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip7k_hole_check(env, env->src1_address)) {
	    if (env->def->has_mmu) {
		/*
		 * Check the mapped range.
		 */
		if(ubicom32_range_check(env->def->mapped_base, env->def->mapped_size, env->src1_address)) {
		    return UBICOM32_MAPPED_ADDRESS;
		}
	    }
	    return UBICOM32_NO_ERROR;
	}

	/*
	 * Access is in a hole. Generate SRC1_DECODE_ERR
	 */
	generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_DECODE_ERR);
	return UBICOM32_INVALID_ADDRESS;
    } else {
	if (ip8k_data_range_check(env, env->src1_address) == 0) {
	    /*
	     * Not in any valid range. Triger SRC1_RANGE_ERR trap
	     */

	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_RANGE_ERR);
	    return UBICOM32_RANGE_ERROR;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip8k_hole_check(env, env->src1_address)) {
	    if (env->def->has_mmu) {
		/*
		 * Check the mapped range.
		 */
		if(ubicom32_range_check(env->def->mapped_base, env->def->mapped_size, env->src1_address)) {
		    return UBICOM32_MAPPED_ADDRESS;
		}
	    }
	    return UBICOM32_NO_ERROR;
	}

	/*
	 * Access is in a hole. Generate SRC1_DECODE_ERR
	 */
	env->thread_regs[0x34] = env->current_pc;
	generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_DECODE_ERR);
	return UBICOM32_INVALID_ADDRESS;
    }
    
    /* NOTREACHED */
    return 0;
}

/*
 * dest_addr is already loaded with an address. We have to test for alignment and 
 * also do range checking and generate traps as needed.
 */
static uint32_t dest_check(CPUUBICOM32State *env, uint32_t opsize)
{
    uint32_t mask = opsize -1;
    uint32_t thread_mask = 1 << env->cpu_index;

	if (env->global_regs[REGISTER_MT_TRAP_EN] & thread_mask) {
 
		/* Do alignment check first. */

		if (env->dest_address & mask) {
			env->thread_regs[0x34] = env->current_pc;
			generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_MISALIGNED);
			return UBICOM32_MISALIGNED_ERROR;
		}
	}

    /*
     * Do range checks.
     */
    if (env->global_regs[0] == 0x0003ffff) {
	if (ip7k_data_range_check(env, env->dest_address) == 0) {
	    /*
	     * Not in any valid range. Triger DEST_RANGE_ERR trap
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_RANGE_ERR);
	    return UBICOM32_RANGE_ERROR;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip7k_hole_check(env, env->dest_address)) {
	    if(env->def->has_mmu) {
		/*
		 * Check the mapped range.
		 */
		if(ubicom32_range_check(env->def->mapped_base, env->def->mapped_size, env->dest_address)) {
		    return UBICOM32_MAPPED_ADDRESS;
		}
	    }
	    return UBICOM32_NO_ERROR;
	}

	/*
	 * Access is in a hole. Generate DEST_DECODE_ERR
	 */
	env->thread_regs[0x34] = env->current_pc;
	generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_DECODE_ERR);
	return UBICOM32_INVALID_ADDRESS;
    } else {
	if (ip8k_data_range_check(env, env->dest_address) == 0) {
	    /*
	     * Not in any valid range. Triger DEST_RANGE_ERR trap
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_RANGE_ERR);
	    return UBICOM32_RANGE_ERROR;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip8k_hole_check(env, env->dest_address)) {
	    if(env->def->has_mmu) {
		/*
		 * Check the mapped range.
		 */
		if(ubicom32_range_check(env->def->mapped_base, env->def->mapped_size, env->dest_address)) {
		    return UBICOM32_MAPPED_ADDRESS;
		}
	    }
	    return UBICOM32_NO_ERROR;
	}

	/*
	 * Access is in a hole. Generate DEST_DECODE_ERR
	 */
	env->thread_regs[0x34] = env->current_pc;
	generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_DECODE_ERR);
	return UBICOM32_INVALID_ADDRESS;
    }
    return 0;
}

/*
 * src1_addr is already loaded with an address. We have to test for alignment and 
 * also do range checking and generate traps as needed.
 */
void HELPER(src1_check)(CPUUBICOM32State *env, uint32_t opsize)
{
    uint32_t mask = opsize -1;
    uint32_t thread_mask = 1 << env->cpu_index;
   
	if (env->global_regs[REGISTER_MT_TRAP_EN] & thread_mask) {

		/* Do alignment check first. */

		if (env->src1_address & mask) {
			env->thread_regs[0x34] = env->current_pc;
			generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_MISALIGNED);
			cpu_loop_exit(env);
			return;
		}
	}

    /*
     * Do range checks.
     */
    if (env->global_regs[0] == 0x0003ffff) {
	if (ip7k_data_range_check(env, env->src1_address) == 0) {
	    /*
	     * Not in any valid range. Triger SRC1_RANGE_ERR trap
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_RANGE_ERR);
	    cpu_loop_exit(env);
	    return;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip7k_hole_check(env, env->src1_address)) {
	    return;
	}

	/*
	 * Access is in a hole. Generate SRC1_DECODE_ERR
	 */
	env->thread_regs[0x34] = env->current_pc;
	generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_DECODE_ERR);
	cpu_loop_exit(env);
	return;
    } else {
	if (ip8k_data_range_check(env, env->src1_address) == 0) {
	    /*
	     * Not in any valid range. Triger SRC1_RANGE_ERR trap
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_RANGE_ERR);
	    cpu_loop_exit(env);
	    return;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip8k_hole_check(env, env->src1_address)) {
	    return;
	}

	/*
	 * Access is in a hole. Generate SRC1_DECODE_ERR
	 */
	env->thread_regs[0x34] = env->current_pc;
	generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_DECODE_ERR);
	cpu_loop_exit(env);
	return;
    }
}

/*
 * dest_addr is already loaded with an address. We have to test for alignment and 
 * also do range checking and generate traps as needed.
 */
void HELPER(dest_check)(CPUUBICOM32State *env, uint32_t opsize)
{
    uint32_t mask = opsize -1;
    uint32_t thread_mask = 1 << env->cpu_index;
  
	if (env->global_regs[REGISTER_MT_TRAP_EN] & thread_mask) {

		/* Do alignment check first. */

		if (env->dest_address & mask) {
			env->thread_regs[0x34] = env->current_pc;
			generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_MISALIGNED);
			cpu_loop_exit(env);
			return;
		}
	}

    /*
     * Do range checks.
     */
    if (env->global_regs[0] == 0x0003ffff) {
	if (ip7k_data_range_check(env, env->dest_address) == 0) {
	    /*
	     * Not in any valid range. Triger DEST_RANGE_ERR trap
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_RANGE_ERR);
	    cpu_loop_exit(env);
	    return;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip7k_hole_check(env, env->dest_address)) {
	    return;
	}

	/*
	 * Access is in a hole. Generate DEST_DECODE_ERR
	 */
	env->thread_regs[0x34] = env->current_pc;
	generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_DECODE_ERR);
	cpu_loop_exit(env);
	return;
    } else {
	if (ip8k_data_range_check(env, env->dest_address) == 0) {
	    /*
	     * Not in any valid range. Triger DEST_RANGE_ERR trap
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_RANGE_ERR);
	    cpu_loop_exit(env);
	    return;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip8k_hole_check(env, env->dest_address)) {
	    return;
	}

	/*
	 * Access is in a hole. Generate DEST_DECODE_ERR
	 */
	env->thread_regs[0x34] = env->current_pc;
	generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_DECODE_ERR);
	cpu_loop_exit(env);
	return;
    }
}

void HELPER(address_check)(CPUUBICOM32State *env, uint32_t opsize, uint32_t check)
{
    uint32_t s1_check = 0;
    uint32_t d_check = 0;

    if (check & 0x1) {
	s1_check = src1_check(env, opsize);
    }

    if (check & 0x2) {
	d_check = dest_check(env, opsize);
    }

    if ((s1_check == 0) && (d_check == 0)) {
	/*
	 * Both the checks are normal.
	 */
	return;
    }

    /*
     * If either of the 2 returns indicate something other than UBICOM32_MAPPED_ADDRESS then
     * bale out of the current loop. The trap has been set and the thread has been blocked.
     */
    if ((s1_check && (s1_check != UBICOM32_MAPPED_ADDRESS)) || 
	(d_check && (d_check != UBICOM32_MAPPED_ADDRESS))) {
	cpu_loop_exit(env);
	return;
    }

    /*
     * You can be here only if either or both src1 and dest are accessing mapped space.
     * We will do the necessary PTEC walk by calling routines from the ocp backend.
     */
    check = 0;

    if (s1_check == UBICOM32_MAPPED_ADDRESS) {
	check = S1_CHECK;
    }

    if (d_check == UBICOM32_MAPPED_ADDRESS) {
	check |= DEST_CHECK;
    }

    /*
     * Call mapping_check to get everything resolved.
     * This routine may not return if we encounter a fault.
     */
    mapping_check(env, env->src1_address, env->dest_address, check);
}

void HELPER(sub)(CPUUBICOM32State *env, uint32_t opsize, uint32_t operation)
{
    /*
     * The sources are sitting in src1_data and src2_data in the env structure.
     * operation  0 sub
     *            1 subc
     *            2 cmpi
     */
    uint32_t res_32;
    uint32_t flag = 0;
    uint32_t b_32 = 0;
    uint16_t b_16 = 0;
    uint8_t b_8 = 0;
    if (operation == 1) {
	/*
	 * SUBC case. Borrow = !C32 flag.
	 * Set flag with current state of the Z flags.
	 */
	flag |= env->thread_regs[env->cc_sr_index] & (CCF_32_Z | CCF_16_Z);
	b_32 =  (env->thread_regs[env->cc_sr_index] & CCF_32_C) ? 0: 1;
	if (b_32) {
	    b_8 = 1;
	    b_16 = 1;
	}
    }

    res_32 = env->src1_data - env->src2_data - b_32;

    if (opsize == 2 || opsize == 4) {
	uint16_t s1 = env->src1_data;
	uint16_t s2 = env->src2_data;
	uint16_t res_16 = s1 - s2 - b_16;

	if (!((s1 < s2) || (b_16 && (s1 == s2)))) {
	    /*
	     * Set the 16 bit C Flag in csr.
	     */
	    flag |= CCF_16_C;
	}

	if (0x8000 & (s1 ^ s2) & (res_16 ^ s1)) {
	    /*
	     * Set the 16 bit overflow V flag in csr.
	     */
	    flag |= CCF_16_V;
	}
#if 1
	if (res_16 & 0x8000)
		flag |= CCF_16_N;

	if (operation == 1) {

		/*
		** From the IP7K PRM:
		**
		** "The Z flag is treated differently for subc than for other instructions.
		**  If the result is nonzero, the Z flag is cleared.
		**  If the result is zero, the Z flag is not changed."
		*/
		              
		if (res_16)
			flag &= ~CCF_16_Z;

	} else {

		if (!res_16)
			flag |= CCF_16_Z;
	}
#else
	if ((res_16 == 0) && operation != 1) {
	    /*
	     * Set the 16 bit Z flag for sub and cmpi cases only.
	     */
	    flag |= CCF_16_Z;
	} else if (res_16 & 0x8000) {
	    /*
	     * Set the 16 bit N flag in csr.
	     */
	    flag |= CCF_16_N;
	}
        if ((operation == 1) && res_32)
            flag &= ~(CCF_32_Z | CCF_16_Z);
#endif

    } else if (opsize == 1) {
	uint8_t s1 = env->src1_data;
	uint8_t s2 = env->src2_data;
	uint8_t res_8 = s1 - s2 - b_8;

	if (!((s1 < s2) || (b_8 && (s1 == s2)))) {
	    /*
	     * Set the 16 bit C Flag in csr.
	     */
	    flag |= CCF_16_C;
	}

	if (0x80 & (s1 ^ s2) & (res_8 ^ s1)) {
	    /*
	     * Set the 16 bit overflow V flag in csr.
	     */
	    flag |= CCF_16_V;
	}

#if 1
	if (res_8 & 0x80)
		flag |= CCF_16_N;

	if (operation == 1) {

		/*
		** From the IP7K PRM:
		**
		** "The Z flag is treated differently for subc than for other instructions.
		**  If the result is nonzero, the Z flag is cleared.
		**  If the result is zero, the Z flag is not changed."
		*/
		              
		if (res_8)
			flag &= ~CCF_16_Z;

	} else {

		if (!res_8)
			flag |= CCF_16_Z;
	}
#else
	if ((res_8 == 0) && operation != 1) {
	    /*
	     * Set the 16 bit Z flag
	     */
	    flag |= CCF_16_Z;
	} else if (res_8 & 0x80) {
	    /*
	     * Set the 16 bit N flag in csr.
	     */
	    flag |= CCF_16_N;
	}
#endif
    }

    /*
     * Repeat the process for the 32 bit result.
     */

    if (!((env->src1_data < env->src2_data) || (b_16 && (env->src1_data == env->src2_data)))) {
	/*
	 * Set the 32 bit C Flag in csr.
	 */
	flag |= CCF_32_C;
    }

    if (0x80000000 & (env->src1_data ^ env->src2_data) & (res_32 ^ env->src1_data)) {
	/*
	 * Set the 32 bit overflow V flag in csr.
	 */
	flag |= CCF_32_V;
    }
#if 1
	if (res_32 & 0x80000000)
		flag |= CCF_32_N;

	if (operation == 1) {

		/*
		** From the IP7K PRM:
		**
		** "The Z flag is treated differently for subc than for other instructions.
		**  If the result is nonzero, the Z flag is cleared.
		**  If the result is zero, the Z flag is not changed."
		*/
		              
		if (res_32)
			flag &= ~CCF_32_Z;

	} else {

		if (!res_32)
			flag |= CCF_32_Z;
	}
#else
    if ((res_32 == 0) && operation != 1) {
	/*
	 * Set the 32 bit Z flag
	 */
	flag |= CCF_32_Z;
    } else if (res_32 & 0x80000000) {
	/*
	 * Set the 32 bit N flag in csr.
	 */
	flag |= CCF_32_N;
    }
#endif

    env->thread_regs[env->cc_sr_index] &= ~0xff;
    env->thread_regs[env->cc_sr_index] |= flag;

    env->dest_data = res_32;
}

void HELPER(add)(CPUUBICOM32State *env, uint32_t opsize, uint32_t operation)
{
    /*
     * The sources are sitting in src1_data and src2_data in the env structure.
     * operation  0 add
     *            1 addc
     */
    uint32_t res_32 = env->src1_data + env->src2_data;
    uint32_t flag = 0;
    uint32_t c_32 = 0;

    if (operation == 1) {
	/*
	 * ADDC case. c_32 = C32 flag.
	 * Grab the current state of the Z flag and update it info flag.
	 */
	flag |= env->thread_regs[env->cc_sr_index] & (CCF_32_Z | CCF_16_Z);
	c_32 =  (env->thread_regs[env->cc_sr_index] & CCF_32_C);
    }

    if (opsize == 2 || opsize == 4) {
	uint16_t s1 = env->src1_data;
	uint16_t s2 = env->src2_data;
	uint16_t res_16 = s1 + s2;

	if (c_32) {
	    res_16++;
	}

	if ((res_16 < s1) || (c_32 && (res_16 == s1)))  {
	    /*
	     * Set the 16 bit C Flag in csr.
	     */
	    flag |= CCF_16_C;
	}

	if (0x8000 & ~(s1 ^ s2) & (res_16 ^ s1)) {
	    /*
	     * Set the 16 bit overflow V flag in csr.
	     */
	    flag |= CCF_16_V;
	}
#if 1	
	if (res_16 & 0x8000)
		flag |= CCF_16_N;

	if (operation) {

		/*
		** From the IP7K PRM:
		**
		** "The Z flag is treated differently for addc than for other instructions.
		**  If the result is nonzero, the Z flag is cleared.
		**  If the result is zero, the Z flag is not changed."
		*/
		              
		if (res_16)
			flag &= ~CCF_16_Z;

	} else {

		if (!res_16)
			flag |= CCF_16_Z;
	}
#else
	if ((res_16 == 0) && (operation !=1)) {
	    /*
	     * Set the 16 bit Z flag is only set by add operation.
	     */
	    flag |= CCF_16_Z;
	} else if (res_16 & 0x8000) {
	    /*
	     * Set the 16 bit N flag in csr.
	     */
	    flag |= CCF_16_N;
	}

        if ((operation == 1) && res_32)
            flag &= ~(CCF_32_Z | CCF_16_Z);
#endif

    } else if (opsize == 1) {
	uint8_t s1 = env->src1_data;
	uint8_t s2 = env->src2_data;
	uint8_t res_8 = s1 + s2;

	if (c_32) {
	    res_8++;
	}

	if ((res_8 < s1) || (c_32 && (res_8 == s1)))  {
	    /*
	     * Set the 16 bit C Flag in csr.
	     */
	    flag |= CCF_16_C;
	}

	if (0x80 & ~(s1 ^ s2) & (res_8 ^ s1)) {
	    /*
	     * Set the 16 bit overflow V flag in csr.
	     */
	    flag |= CCF_16_V;
	}

	if (res_32 & 0x80000000)
		flag |= CCF_32_N;

#if 1
	if (res_8 & 0x80)
		flag |= CCF_16_N;

	if (operation) {

		/*
		** From the IP7K PRM:
		**
		** "The Z flag is treated differently for addc than for other instructions.
		**  If the result is nonzero, the Z flag is cleared.
		**  If the result is zero, the Z flag is not changed."
		*/
		              
		if (res_8)
			flag &= ~CCF_16_Z;

	} else {

		if (!res_8)
			flag |= CCF_16_Z;
	}
#else
	if ((res_8 == 0) && (operation == 0)) {
	    /*
	     * Set the 16 bit Z flag. Only add can set zflag.
	     */
	    flag |= CCF_16_Z;
	} else if (res_8 & 0x80) {
	    /*
	     * Set the 16 bit N flag in csr.
	     */
	    flag |= CCF_16_N;
	}
#endif
    }

    /*
     * Repeat the process for the 32 bit result.
     */
    if (c_32) {
	res_32++;
    }

    if ((res_32 < env->src1_data) || (c_32 && (res_32 == env->src1_data)))  {
	/*
	 * Set the 32 bit C Flag in csr.
	 */
	flag |= CCF_32_C;
    }

    if (0x80000000 & ~(env->src1_data ^ env->src2_data) & (res_32 ^ env->src1_data)) {
	/*
	 * Set the 32 bit overflow V flag in csr.
	 */
	flag |= CCF_32_V;
    }

#if 1
	if (res_32 & 0x80000000)
		flag |= CCF_32_N;

	if (operation) {

		/*
		** From the IP7K PRM:
		**
		** "The Z flag is treated differently for addc than for other instructions.
		**  If the result is nonzero, the Z flag is cleared.
		**  If the result is zero, the Z flag is not changed."
		*/
	
		if (res_32)
			flag &= ~CCF_32_Z;

	} else {

		if (!res_32)
			flag |= CCF_32_Z;
	}
#else
    if ((res_32 == 0) && (operation == 0)) {
	/*
	 * Set the 32 bit Z flag. Only Add can set the z flag.
	 */
	flag |= CCF_32_Z;
    } else if (res_32 & 0x80000000) {
	/*
	 * Set the 32 bit N flag in csr.
	 */
	flag |= CCF_32_N;
    }
#endif

    env->thread_regs[env->cc_sr_index] &= ~0xff;
    env->thread_regs[env->cc_sr_index] |= flag;

    env->dest_data = res_32;
}

void HELPER(suspend)(CPUUBICOM32State *env)
{
    uint32_t mask = 1 << env->cpu_index;
    uint32_t int_state = 0;
    uint32_t num_ints = env->def->num_interrupts/32;
    uint32_t i;
    /*
     * int_state = (int_stat0 & int_mask0) | (int_stat1 & int_mask1) | ...
     */
    for (i=0; i< num_ints; i++) {
	int_state |= (ubicom32_global_registers[GLOBAL_REG(0x41) + i] & env->thread_regs[0x30 + i]);
    }

    if (int_state) {
	/*
	 * This thread should be active.
	 */
	//printf("Thread kept alive. Int_state = 0x%08x\n", int_state);
	ubicom32_global_registers[GLOBAL_REG(0x4e)] |= mask;
    } else {
	/*
	 * Clear the thread bit from mt_active register.
	 */
	ubicom32_global_registers[GLOBAL_REG(0x4e)] &= ~mask;
	//printf("I am falling asleep %d mask 0x%08x gr = 0x%08x\n", env->cpu_index, mask, env->global_regs[0x4e - 0x40]);
    }
}

void HELPER(bkpt)(CPUUBICOM32State *env)
{
    uint32_t mask = 1 << env->cpu_index;
    uint32_t dbg_active_cutoff = (1 << env->def->num_threads) - 1;
    uint32_t dbg_active_mask = env->src1_data & dbg_active_cutoff;

    /*
     * The current thread has to be stopped. So stuff his bit into the dbg_active_mask.
     */
    dbg_active_mask |= mask;

    /*
     * Drive the bits in mt_dbg_active to zero.
     */
    env->global_regs[GLOBAL_REG(0x51)] &= ~dbg_active_mask;

    /*
     * Set the bit for this thrad in mt_break;
     */
    env->global_regs[GLOBAL_REG(0x56)] |= mask;

    /*
     * Set Bit 31 in int_stat1 high.
     */
    env->global_regs[GLOBAL_REG(0x42)] |= 0x80000000;
}

void HELPER(nzflag)(CPUUBICOM32State *env, uint32_t opsize)
{
    /*
     * The result is sitting in dest_data.
     */
    uint32_t flags = 0;
    uint32_t clr_flag = CCF_32_N|CCF_32_Z|CCF_16_N|CCF_16_Z;
    if (opsize == 1) {
	uint8_t d_8 = env->dest_data;
	if(d_8 ==0) {
	    flags |= CCF_16_Z;
	} else if (d_8 & 0x80) {
	    flags |= CCF_16_N;
	}
    } else {
	uint16_t d_16 = env->dest_data;
	if(d_16 ==0) {
	    flags |= CCF_16_Z;
	} else if (d_16 & 0x8000) {
	    flags |= CCF_16_N;
	}
    }

    if(env->dest_data ==0) {
	flags |= CCF_32_Z;
    } else if (env->dest_data & 0x80000000) {
	flags |= CCF_32_N;
    }

    env->thread_regs[env->cc_sr_index] &= ~clr_flag;
    env->thread_regs[env->cc_sr_index] |= flags;
}

void HELPER(crcgen)(CPUUBICOM32State *env)
{
    /*
     * src1_data is the input byte.
     * src2_data is the Polynomial.
     */
    uint32_t MYC;
    uint32_t S, F, X;
    uint32_t machi = env->thread_regs[0x28];
    uint32_t i;
    uint32_t B = env->src1_data;
    uint32_t P = env->src2_data;

    MYC = env->thread_regs[0x29];
    X = (MYC ^ B) & 0xff;
    F = X;
 
    for (i = 0; i < 8; ++i) {
	F = (F >> 1)^(F & 1 ? P : 0);
    }

    S = MYC & 0xff;
    env->thread_regs[0x29] = (F ^ (MYC >> 8));

    machi = (machi >> 8) | (S << 24);
    env->thread_regs[0x28] = machi;

    helper_finish_multiply(env);
}

void HELPER(finish_multiply)(CPUUBICOM32State *env)
{
    /*
     * Routine that generates contents of the rc16 register.
     */

    int16_t rounded_hi = env->thread_regs[0x28];
    uint32_t mac_lo = env->thread_regs[0x29];

    int32_t positive = 0;
    uint32_t rounded_lo = mac_lo + 0x8000;

    if((rounded_hi &0x8000) == 0)
	positive = 1;
    
    if (rounded_lo < mac_lo)
	rounded_hi += 1;

    /* check for saturation */
    if(positive) {
	if(rounded_hi & 0x8000) {
	    /* The number rolled from +ve to -ve. Clamp it to 0.999999 */
	    env->thread_regs[0x2a] = 0x00007fff; /* value is >= 1.0 */
	    return;
	}
    }
    
    if (rounded_hi < 0) {
	/* check for negative saturation */
	if (!(rounded_hi == -1 && (rounded_lo & 0x80000000))) {
	    env->thread_regs[0x2a] = 0xffff8000; /* value is < -1.0 */
	    return;
	}
    } else {
	/* check for positive saturation */
	if (rounded_hi != 0 || (rounded_lo & 0x80000000)) {
	    env->thread_regs[0x2a] = 0x00007fff; /* value is >= 1.0 */
	    return;
	}
    }

    /* want the top 16-bits of mac_lo, sign-extended */
    rounded_lo >>= 16;
    rounded_lo &= 0xffff;
    if (rounded_lo & 0x8000) {
	rounded_lo |= 0xffff0000;
    }

    env->thread_regs[0x2a] = rounded_lo;
}

void HELPER(bfrvrs)(CPUUBICOM32State *env, uint32_t s1, uint32_t shift)
{
    uint32_t result = 0;
    int i;

    /*
     * Trim shift to 5 bits.
     */
    shift &= 0x1f;

    /*
     * Reverse the bits in s1
     */
    for (i = 0; i < 32; ++i) {
	result <<= 1;
	result += s1 & 1;
	s1 >>= 1;
    }

    result >>= shift;

    /*
     * Store the result in env->dest_data.
     */
    env->dest_data = result;

    /*
     * Get N and Z flag fixed.
     */
    helper_nzflag(env, 4);
}

void HELPER(jmpcc)(CPUUBICOM32State *env, uint32_t cc, uint32_t use_32)
{
    int cbit, vbit, nbit, zbit;
    uint32_t csr = env->thread_regs[env->cc_sr_index];
    uint32_t ret = 0;
#if 0
    // condition codes for jmpcc instruction
    uint32_t CC_F  = 0;
    uint32_t CC_NC = 1;
    uint32_t CC_C  = 2;
    uint32_t CC_Z  = 3;
    uint32_t CC_GE = 4;
    uint32_t CC_GT = 5;
    uint32_t CC_HI = 6;
    uint32_t CC_LE = 7;
    uint32_t CC_LS = 8;
    uint32_t CC_LT = 9;
    uint32_t CC_MI = 10;
    uint32_t CC_NZ = 11;
    uint32_t CC_PL = 12;
    uint32_t CC_T  = 13;
    uint32_t CC_NV = 14;
    uint32_t CC_V  = 15;
#endif
    if (use_32)
	{
	    cbit = (csr & CCF_32_C) != 0;
	    vbit = (csr & CCF_32_V) != 0;
	    nbit = (csr & CCF_32_N) != 0;
	    zbit = (csr & CCF_32_Z) != 0;
	}
    else
	{
	    cbit = (csr & CCF_16_C) != 0;
	    vbit = (csr & CCF_16_V) != 0;
	    nbit = (csr & CCF_16_N) != 0;
	    zbit = (csr & CCF_16_Z) != 0;
	}
    
    switch (cc)
	{
	case CC_F:
	    ret = 0;
	    break;
	case CC_T:
	    ret = 1;
	    break;
	case CC_C:
	    ret = (cbit);
	    break;
	case CC_NC:
	    ret = (cbit == 0);
	    break;
	case CC_NZ:
	    ret = (zbit == 0);
	    break;
	case CC_Z:
	    ret = (zbit);
	    break;
	case CC_GE:
	    ret = ((nbit ^ vbit) == 0);
	    break;
	case CC_LT:
	    ret = (nbit ^ vbit);
	    break;
	case CC_GT:
	    ret = ((zbit | (nbit ^ vbit)) == 0);
	    break;
	case CC_LE:
	    ret = (zbit | (nbit ^ vbit));
	    break;
	case CC_LS:
	    ret = ((!cbit) | zbit);
	    break;
	case CC_HI:
	    ret = (((!cbit) | zbit) == 0);
	    break;
	case CC_MI:
	    ret = (nbit);
	    break;
	case CC_PL:
	    ret = (nbit == 0);
	    break;
	case CC_V:
	    ret = (vbit);
	    break;
	case CC_NV:
	    ret = (vbit == 0);
	    break;
	}
    
    env->dest_data = ret;
    return;
}

void HELPER(btst_clr_set)(CPUUBICOM32State *env, uint32 operation, uint32 opcode)
{
	uint32 *dest;

	/*
	 * BTST/BCLR/BSET helper
	 * operation 0 = BTST
	 *           1 = BCLR
	 *           2 = BSET
	 *           3 = TBCLR
	 *           4 = TBSET
	 *
	 * env->bset_clr_tmp holds the mask.
	 *
	 * BTST only changes the state of the flags.
	 *
	 * Find the current state of the bit.
	 */

	/* If the operation is TBCLR or TBSET then set the mask using the cpu_index.  */

	if ((operation == 3) || (operation == 4))
		env->bset_clr_tmp = 1 << env->cpu_index;

	/* The sequence of operations is very important.
	   In the case the destination register is UCSR, e.g. BCLR/BSET UCSR,UCSR,#foo,
	   then bit operation must be performed FIRST, then the Z bits need to be set
	   LAST based on the original value of the UCSR. */

	/* Do the operation. */

	if ((operation == 2) || (operation == 4))			/* BSET or TBSET */
		env->dest_data = env->src1_data | env->bset_clr_tmp;
	else if(operation == 1 || operation == 3)			/* BCLR or TBLCR */
		env->dest_data = env->src1_data & ~env->bset_clr_tmp;	

	/* If the dest is the UCSR, then set the bits in env->dest_data instead because
	   the caller will overwrite env->thread_regs[UCSR] with env->dest_data after returning. */

	dest = (((opcode >> 16) & ((1 << 11) - 1)) == 0x139) ? &env->dest_data : &env->thread_regs[env->cc_sr_index];

	/* Modify the Z bits. */

	if (env->src1_data & env->bset_clr_tmp)
		*dest &= ~(CCF_32_Z | CCF_16_Z);	/* The bit was set, so clear the Z flags */
	else 
		*dest |= (CCF_32_Z | CCF_16_Z);		/* The bit was clear, so set the Z flags */
}

/**************************************
* Start of packed pixel instruction helpers
**************************************/

#define RGB565_R(x)	(((x) >> 11) & 0x1f)
#define RGB565_G(x)	(((x) >> 5) & 0x3f)
#define RGB565_B(x)	((x) & 0x1f)

#define ALPHA_R(x)	(((x) >> 2) & 0x3f)
#define ALPHA_G(x)	(((x) >> 1) & 0x7f)
#define ALPHA_B(x)	(((x) >> 2) & 0x3f)

void HELPER(pxadds)(CPUUBICOM32State *env, uint32_t bit_15)
{
	int32_t i, temp, min_value, max_value;
	uint32_t source1, source2, result;

//	printf("pxadds source1: %08x\n", env->src1_data);
//	printf("pxadds source2: %08x\n", env->src2_data);
//	printf("pxadds source3: %08x\n", env->thread_regs[0x2b]);

	source1 = env->src1_data;
	source2 = env->src2_data >> (bit_15 << 4);

	max_value = (env->thread_regs[REGISTER_SOURCE3] >> 8) & 0xff;
	min_value =  env->thread_regs[REGISTER_SOURCE3] & 0xff;

	for (result=i=0; i<2; i++) {

		temp = source1 & 0x1ff;

		if (temp & 0x100)
			temp |= 0xfffffe00;

		temp += source2 & 0xff;

//		printf("     pre-clip result: %08x\n", temp);

		if (temp > max_value)
			temp = max_value;
		else if (temp < min_value)
			temp = min_value;

//		printf("    post-clip result: %08x\n", temp);

		result |= temp << 16;
		result >>= 8;

		source1 >>= 16;
		source2 >>= 8;
	}

	env->dest_data = result;
}

/* Extract the RGB and alpha components as 8 bits to simplify calculations */

void HELPER(pxblend)(CPUUBICOM32State *env, uint32_t bit_15)
{
	uint32_t rgb565_0, rgb565_1, rgb565_2, rgb565_3;
	uint32_t output_a_r, output_a_g, output_a_b;
	uint32_t output_b_r, output_b_g, output_b_b;
	uint32_t alpha_a, alpha_b;

	rgb565_0 = (env->src1_data >> 16) & 0xffff;
	rgb565_1 = env->src1_data & 0xffff;

	rgb565_2 = (env->src2_data >> 16) & 0xffff;
	rgb565_3 = env->src2_data & 0xffff;

	alpha_a = (env->thread_regs[REGISTER_SOURCE3] >> 8) & 0xff;
	alpha_b = env->thread_regs[REGISTER_SOURCE3] & 0xff;

//	printf("input2: %x/%x/%x\n", RGB565_R(rgb565_2), RGB565_G(rgb565_2), RGB565_B(rgb565_2));
//	printf("input0: %x/%x/%x\n", RGB565_R(rgb565_0), RGB565_G(rgb565_0), RGB565_B(rgb565_0));
//	printf("alpha: %x\n", alpha_a);

	output_a_r = ALPHA_R(alpha_a) * RGB565_R(rgb565_2) + ( 64 - ALPHA_R(alpha_a)) * RGB565_R(rgb565_0);
	output_a_g = ALPHA_G(alpha_a) * RGB565_G(rgb565_2) + (128 - ALPHA_G(alpha_a)) * RGB565_G(rgb565_0);
	output_a_b = ALPHA_B(alpha_a) * RGB565_B(rgb565_2) + ( 64 - ALPHA_B(alpha_a)) * RGB565_B(rgb565_0);

	output_b_r = ALPHA_R(alpha_b) * RGB565_R(rgb565_3) + ( 64 - ALPHA_R(alpha_b)) * RGB565_R(rgb565_1);
	output_b_g = ALPHA_G(alpha_b) * RGB565_G(rgb565_3) + (128 - ALPHA_G(alpha_b)) * RGB565_G(rgb565_1);
	output_b_b = ALPHA_B(alpha_b) * RGB565_B(rgb565_3) + ( 64 - ALPHA_B(alpha_b)) * RGB565_B(rgb565_1);

//	printf("output: %x/%x/%x\n", output_a_r, output_a_g, output_a_b);

	if (!bit_15) {

		/* Round color components. This does not saturate which seems to match hardware behavior.  */

		output_a_r += 1 << 5;
		output_a_g += 1 << 6;
		output_a_b += 1 << 5;

		output_b_r += 1 << 5;
		output_b_g += 1 << 6;
		output_b_b += 1 << 5;
	}

//	printf("output: %x/%x/%x\n", output_a_r, output_a_g, output_a_b);

	output_a_r >>= 6;
	output_a_g >>= 7;
	output_a_b >>= 6;

	output_b_r >>= 6;
	output_b_g >>= 7;
	output_b_b >>= 6;

//	printf("output: %x/%x/%x\n", output_a_r, output_a_g, output_a_b);

	env->dest_data = (output_a_r << 27) | (output_a_g << 21) | (output_a_b << 16) | (output_b_r << 11) | (output_b_g << 5) | output_b_b;
}

void HELPER(pxcnv)(CPUUBICOM32State *env, uint32_t truncate_flag)
{
	uint32_t red, green, blue;

	red   = (env->src1_data >> 16) & 0xff;
	green = (env->src1_data >> 8) & 0xff;
	blue  = (env->src1_data >> 0) & 0xff;

	if (!truncate_flag) {
		red   += ((red   & 0xf8) != 0xf8) << 2;
		green += ((green & 0xfc) != 0xfc) << 1;
		blue  += ((blue  & 0xf8) != 0xf8) << 2;
	}

	env->dest_data = ((red & 0xf8) << 8) | ((green & 0xfc) << 3) | ((blue & 0xf8) >> 3);
}

#define INTERPOLATE(a, b, value, rnd_ctl, max_value, shifts) (((a) * ((max_value) - (value)) + (b) * (value) + rnd_ctl) >> (shifts))

void HELPER(pxhi)(CPUUBICOM32State *env, uint32_t swap_flag)
{
	int i;
	uint32_t byte_offset, rnd_ctl, pos, out;
	uint8_t p_data[8], p[5];

	byte_offset = (env->thread_regs[REGISTER_SOURCE3] >> 8) & 3;
	rnd_ctl = (env->thread_regs[REGISTER_SOURCE3] & (1 << 24)) >> 22;
	pos = env->thread_regs[REGISTER_SOURCE3] & 7;

//	printf("src1_data: %08x\n", env->src1_data);
//	printf("src2_data: %08x\n", env->src2_data);
//	printf("byte_offset: %d, pos: %d, rnd_ctl: %d\n", byte_offset, pos, rnd_ctl);

	if (swap_flag) {

		for (i=0; i<4; i++)
			p_data[i] = (env->src2_data >> (3 - i) * 8) & 0xff;

		for (i=4; i<8; i++)
			p_data[i] = (env->src1_data >> (7 - i) * 8) & 0xff;
	} else {

		for (i=0; i<4;i++)
			p_data[i] = (env->src1_data >> (3 - i) * 8) & 0xff;

		for (i=4; i<8; i++)
			p_data[i] = (env->src2_data >> (7 - i) * 8) & 0xff;
	}

	for (i=0; i<5; i++)
		p[i] = p_data[i + byte_offset];

	for (i=0; i<4; i++) {
//		printf("current_byte: %x, next_byte: %x\n", p[i], p[i+1]);
		out = (p[i] * (8 - pos) + p[i+1] * pos + rnd_ctl) / 8;
		out = (out > 255) ? 255 : out;				/* SID does this */
		env->dest_data = (env->dest_data & ~(0xff << (3 - i) * 8)) | ((out & 0xff) << (3 - i) * 8);
	}
}

void HELPER(pxvi)(CPUUBICOM32State *env, uint32_t swap_flag)
{
	int i;
	uint32 pos, rnd_ctl, hi, lo, p_0, p_1, tmp;

	pos = (env->thread_regs[REGISTER_SOURCE3] >> 16) & 7;
	rnd_ctl = (env->thread_regs[REGISTER_SOURCE3] & (1 << 28)) >> 26;

	if (swap_flag) {
		hi = env->src2_data;
		lo = env->src1_data;
	} else {
		hi = env->src1_data;
		lo = env->src2_data;
	}

	for (i=0; i<4; i++) {

		p_0 = (hi >> i * 8) & 0xff;
		p_1 = (lo >> i * 8) & 0xff;

		tmp = (p_0 * (8 - pos) + p_1 * pos + rnd_ctl) / 8;

//		printf("output byte %d: %08x\n", i, tmp);

		env->dest_data = (env->dest_data & ~(0xff << i * 8)) | (tmp << i * 8);
	}
}

/**************************************
* End of packed pixel instruction helpers
**************************************/
/**************************************
* Start of floating-point instruction helpers
**************************************/

typedef union {

	float f32;
	uint32_t i32;

	double f64;
	uint64_t i64;

} CONVERSION;

#define POSITIVE_INFINITY	0x7f800000
#define NEGATIVE_INFINITY	0xff800000

#define	IS_SINGLE_NAN(x) (((x & 0x7f800000) == 0x7f800000) && (x & 0x007fffff))
#define UBICOM32_NAN		0x7fffffff

/* x87 status word bits */

#define X87_INVALID_BIT			(1 << 0)
#define X87_DENORMALIZED_BIT		(1 << 1)
#define X87_DIVIDE_BY_ZERO_BIT		(1 << 2)
#define X87_OVERFLOW_BIT		(1 << 3)
#define X87_UNDERFLOW_BIT		(1 << 4)
#define X87_PRECISION_BIT		(1 << 5)

/* x87 control word bits */

#define X87_ROUND_TO_NEAREST		(0 << 10)
#define X87_ROUND_DOWN			(1 << 10)
#define X87_ROUND_UP			(2 << 10)
#define X87_ROUND_TO_ZERO		(3 << 10)

/* x87 miscellaneous defines */

#define X87_SINGLE_PRECISION		(0 << 8)
#define X87_DOUBLE_PRECISION		(2 << 8)
#define X87_EXTENDED_PRECISION		(3 << 8)
#define X87_EXCEPTION_MASK		((1 << 6) - 1)		/* Mask all exceptions */


#ifdef FIX_NAN
static float fix_single_NaN(float value)
{
	CONVERSION convert;

	convert.f32 = value;

	if (IS_SINGLE_NAN(convert.i32))
		convert.i32 = UBICOM32_NAN;

	return convert.f32;
}
#endif

/*
** Set the 80x87 rounding mode (based on the Ubicom32 UCSR) and precision, and clear the 80x87 status register.
*/

void set_x87_mode(int precision);
void set_x87_mode(int precision)
{
	uint16_t x87_control_word;
	uint32_t ucsr;

	ucsr = env->thread_regs[REGISTER_UCSR];

	switch ((ucsr >> 23) & 3) {

		case 0:		/* Round to nearest */

			x87_control_word = X87_ROUND_TO_NEAREST;
//			printf("x87 FPU rounding mode: round to nearest\n");
			break;

		case 1:		/* Round to zero */

			x87_control_word = X87_ROUND_TO_ZERO;
//			printf("x87 FPU rounding mode: round to zero\n");
			break;

		case 2:		/* Round to positive infinity */

			x87_control_word = X87_ROUND_UP;
//			printf("x87 FPU rounding mode: round to +infinity\n");
			break;

		case 3:		/* Round to negative infinity */

			x87_control_word = X87_ROUND_DOWN;
//			printf("x87 FPU rounding mode: round to -infinity\n");
			break;
	}

	switch (precision) {

		case 1:

			x87_control_word |= X87_SINGLE_PRECISION;
			break;

		case 2:

			x87_control_word |= X87_DOUBLE_PRECISION;
			break;

		default:

			printf("bad x87 precision mode: %d\n", precision);
			abort();
	}

	x87_control_word |= X87_EXCEPTION_MASK;

	/* Clear the FPU status bits before loading the control word */

	__asm__ volatile("fclex");
	__asm__ volatile("fldcw %0" :: "m" (x87_control_word));
}

/*
** Convert the x87 status bits into Ubicom32 status bits.
*/

void convert_x87_status_bits(void);
void convert_x87_status_bits(void)
{
	uint16_t x87_control_word, x87_status_bits;
	uint32_t ucsr_fpu_status_bits;

	 __asm__ volatile ("fstsw %0" : "=a" (x87_status_bits));

	ucsr_fpu_status_bits = 0;

	if (x87_status_bits & X87_INVALID_BIT)
		ucsr_fpu_status_bits |= UCSR_FPU_INVALID_BIT;

	if (x87_status_bits & X87_DIVIDE_BY_ZERO_BIT)
		ucsr_fpu_status_bits |= UCSR_FPU_DIVIDE_BY_ZERO_BIT;

	if (x87_status_bits & X87_OVERFLOW_BIT)
		ucsr_fpu_status_bits |= UCSR_FPU_OVERFLOW_BIT;

	if (x87_status_bits & X87_UNDERFLOW_BIT)
		ucsr_fpu_status_bits |= UCSR_FPU_UNDERFLOW_BIT;

	if (x87_status_bits & X87_PRECISION_BIT)
		ucsr_fpu_status_bits |= UCSR_FPU_INEXACT_BIT;

//	printf("x87 FPU status bits: %08x, UCSR FPU status bits: %08x\n", x87_status_bits, ucsr_fpu_status_bits);

	env->thread_regs[REGISTER_UCSR] &= ~UCSR_FPU_ALL_BITS;
	env->thread_regs[REGISTER_UCSR] |= ucsr_fpu_status_bits;

	/* Set the FPU precision back to normal */

	x87_control_word = X87_EXTENDED_PRECISION | X87_EXCEPTION_MASK;

	__asm__ volatile("fclex");
	__asm__ volatile("fldcw %0" :: "m" (x87_control_word));
}

void HELPER(fadds)(CPUUBICOM32State *env)
{
	float s1, s2;
	CONVERSION convert;

	convert.i32 = env->src1_data;
	s1 = convert.f32;

	convert.i32 = env->src2_data;
	s2 = convert.f32;

	set_x87_mode(1);

#ifdef FIX_NAN
	convert.f32 = fix_single_NaN(s1 + s2);
#else
	convert.f32 = s1 + s2;
#endif

	convert_x87_status_bits();

//	printf("FADDS: %f (0x%08x) + %f (0x%08x) = %f (0x%08x)\n", s1, env->src1_data, s2, env->src2_data, dest.f32, dest.i32);

	env->dest_data = convert.i32;
}

void HELPER(fsubs)(CPUUBICOM32State *env)
{
	float s1, s2;
	CONVERSION convert;

	convert.i32 = env->src1_data;
	s1 = convert.f32;

	convert.i32 = env->src2_data;
	s2 = convert.f32;

	set_x87_mode(1);

#ifdef FIX_NAN
	convert.f32 = fix_single_NaN(s1 - s2);
#else
	convert.f32 = s1 - s2;
#endif

	convert_x87_status_bits();

	env->dest_data = convert.i32;
}

void HELPER(fmuls)(CPUUBICOM32State *env)
{
	float s1, s2;
	CONVERSION convert;

	convert.i32 = env->src1_data;
	s1 = convert.f32;

	convert.i32 = env->src2_data;
	s2 = convert.f32;

	set_x87_mode(1);

#ifdef FIX_NAN
	convert.f32 = fix_single_NaN(s1 * s2);
#else
	convert.f32 = s1 * s2;
#endif

	convert_x87_status_bits();

//	printf("FMULS: %f (0x%08x) * %f (0x%08x) = %f (0x%08x)\n", s1, env->src1_data, s2, env->src2_data, convert.f32, convert.i32);

	env->dest_data = convert.i32;
}

void HELPER(fdivs)(CPUUBICOM32State *env)
{
	float s1, s2;
	CONVERSION convert;

	convert.i32 = env->src1_data;
	s1 = convert.f32;

	convert.i32 = env->src2_data;
	s2 = convert.f32;

	set_x87_mode(1);

#ifdef FIX_NAN
	convert.f32 = fix_single_NaN(s1 / s2);
#else
	convert.f32 = s1 / s2;
#endif

	convert_x87_status_bits();

//	printf("FDIVS: %f (0x%08x) / %f (0x%08x) = %f (0x%08x)\n", s1, env->src1_data, s2, env->src2_data, convert.f32, convert.i32);

	env->dest_data = convert.i32;
}

void HELPER(fi2d)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	set_x87_mode(1);

	convert.f64 = (double)env->src1_data;
	env->dest_64 = convert.i64;

//	printf("FI2D: 0x%08x -> 0x%016llx\n", env->src1_data, convert.i64);

	convert_x87_status_bits();
}

void HELPER(fs2d)(CPUUBICOM32State *env)
{
	CONVERSION convert1, convert2;

	set_x87_mode(1);

	convert1.i32 = env->src1_data;
	convert2.f64 = convert1.f32;
	env->dest_64 = convert2.i64;

//	printf("FS2D: 0x%08x -> 0x%016llx\n", env->src1_data, convert2.i64);

	convert_x87_status_bits();
}

void HELPER(fs2l)(CPUUBICOM32State *env)
{
	set_x87_mode(1);

/* FIXME - promotion. */
	env->dest_64 = (uint64_t)(*(float *)env->src1_data);

	convert_x87_status_bits();
}

void HELPER(fsqrts)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	set_x87_mode(1);

	convert.i32 = env->src1_data;

#ifdef FIX_NAN
	convert.f32 = fix_single_NaN(sqrtf(convert.f32));
#else
	convert.f32 = sqrtf(convert.f32);
#endif

	convert_x87_status_bits();

	env->dest_data = convert.i32;
}

void HELPER(fnegs)(CPUUBICOM32State *env)
{
	env->dest_data = env->src1_data ^ 0x80000000;
}

void HELPER(fabss)(CPUUBICOM32State *env)
{
	env->dest_data = env->src1_data & 0x7fffffff;
}

void HELPER(fi2s)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	set_x87_mode(1);

	convert.f32 = (float)(int32_t)env->src1_data;

	convert_x87_status_bits();

	env->dest_data = convert.i32;
}

void HELPER(fs2i)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	convert.i32 = env->src1_data;

	set_x87_mode(1);

	env->dest_data = (int)convert.f32;

	convert_x87_status_bits();
}

void HELPER(fcmps)(CPUUBICOM32State *env)
{
	float s1, s2, result;
	CONVERSION convert;

	convert.i32 = env->src1_data;
	s1 = convert.f32;

	convert.i32 = env->src2_data;
	s2 = convert.f32;

//	printf("comparing %f (0x%08x) with %f (0x%08x)\n", s1, env->src1_data, s2, env->src2_data);

	set_x87_mode(1);

/* FIXME - not used. */
	result = s1 - s2;

	convert_x87_status_bits();

	env->thread_regs[REGISTER_UCSR] &= ~0xee;

	if (s1 == s2) {
		/* Set Z clean N and V */
		env->thread_regs[0x39] |= (CCF_32_Z | CCF_16_Z);
	} else if (s1 < s2) {
		/* Set N clear V and Z */
		env->thread_regs[0x39] |= (CCF_32_N | CCF_16_N);
	} else if (s1 > s2) {
		/* Clear N Z and V */
		;
	} else {
		/* Unordered, Set V clear N and Z */
		env->thread_regs[0x39] |= (CCF_32_V | CCF_16_V);
	}
}

void HELPER(faddd)(CPUUBICOM32State *env)
{
	double s1, s2, result;
	CONVERSION convert;

	convert.i64 = env->src1_64;
	s1 = convert.f64;

	convert.i64 = env->src2_64;
	s2 = convert.f64;

	set_x87_mode(2);

	result = s1 + s2;

	convert_x87_status_bits();

	convert.f64 = result;

//	printf("faddd: %lf (%llx) / %lf (%llx) = %f (%llx)\n", s1, env->src1_64, s2, env->src2_64, data.f64, data.i64);

	env->dest_64 = convert.i64;
}

void HELPER(fsubd)(CPUUBICOM32State *env)
{
	double s1, s2, result;
	CONVERSION convert;

	convert.i64 = env->src1_64;
	s1 = convert.f64;

	convert.i64 = env->src2_64;
	s2 = convert.f64;

	set_x87_mode(2);

	result = s1 - s2;

	convert_x87_status_bits();

	convert.f64 = result;

	env->dest_64 = convert.i64;
}

void HELPER(fmuld)(CPUUBICOM32State *env)
{
	double s1, s2, result;
	CONVERSION convert;

	convert.i64 = env->src1_64;
	s1 = convert.f64;

	convert.i64 = env->src2_64;
	s2 = convert.f64;

	set_x87_mode(2);

	result = s1 * s2;

	convert_x87_status_bits();

	convert.f64 = result;

//	printf("FMULD: %lf (0x%016llx) * %lf (0x%016llx) = %lf (0x%016llx)\n", s1, env->src1_64, s2, env->src2_64, result, data.i64);

	env->dest_64 = convert.i64;
}

void HELPER(fdivd)(CPUUBICOM32State *env)
{
	double s1, s2, result;
	CONVERSION convert;

	convert.i64 = env->src1_64;
	s1 = convert.f64;

	convert.i64 = env->src2_64;
	s2 = convert.f64;

	set_x87_mode(2);

	result = s1 / s2;

	convert_x87_status_bits();

	convert.f64 = result;

//	printf("FDIVD: %lf (0x%llx) / %lf (0x%llx) = %lf (0x%llx)\n", s1, env->src1_64, s2, env->src2_64, data.f64, data.i64);

	env->dest_64 = convert.i64;
}

void HELPER(fl2s)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	convert.f32 = (float)(int64_t)env->src1_64;

//	printf("FL2S: input: 0x%016llx, output: 0x%08x\n", env->src1_64, convert.i32);

	env->dest_data = convert.i32;
}

void HELPER(fd2s)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	convert.f32 = (float)(int64_t)env->src1_64;

	env->dest_data = convert.i32;
}

void HELPER(fd2i)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	convert.i64 = env->src1_64;

	env->dest_data = (int)convert.f64;
}

void HELPER(fsqrtd)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	convert.i64 = env->src1_64;

	set_x87_mode(2);

	convert.f64 = sqrt(convert.f64);

	convert_x87_status_bits();

	env->dest_64 = convert.i64;
}

void HELPER(fnegd)(CPUUBICOM32State *env)
{
	env->dest_64 = env->src1_64 ^ 0x8000000000000000LL;
}

void HELPER(fabsd)(CPUUBICOM32State *env)
{
	env->dest_64 = env->src1_64 & 0x7fffffffffffffffLL;
}

void HELPER(fl2d)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	convert.f64 = (double)(int64_t)env->src1_64;

	env->dest_64 = convert.i64;
}

void HELPER(fd2l)(CPUUBICOM32State *env)
{
	CONVERSION convert;

	convert.i64 = env->src1_64;

	env->dest_64 = (int64_t)convert.f64;
}

void HELPER(fcmpd)(CPUUBICOM32State *env)
{
	double s1, s2, result;
	CONVERSION convert;

	convert.i64 = env->src1_64;
	s1 = convert.f64;

	convert.i64 = env->src2_64;
	s2 = convert.f64;

//	printf("FCMPD: %lf (0x%llx) vs %lf (0x%llx)\n", s1, env->src1_64, s2, env->src2_64);

	set_x87_mode(2);

	result = s1 - s2;

	convert_x87_status_bits();

	convert.f64 = result;

	env->thread_regs[0x39] &= ~0xee;

	if (s1 == s2) {
		/* Set Z clean N and V */
		env->thread_regs[0x39] |= (CCF_32_Z | CCF_16_Z);
	} else if (s1 < s2) {
		/* Set N clear V and Z */
		env->thread_regs[0x39] |= (CCF_32_N | CCF_16_N);
	} else if (s1 > s2) {
		/* Clear N Z and V */
		;
	} else {
		/* Unordered, Set V clear N and Z */
		env->thread_regs[0x39] |= (CCF_32_V | CCF_16_V);
	}
}

/**************************************
* End of floating-point instruction helpers
**************************************/

void HELPER(debug)(CPUUBICOM32State *env)
{
	printf("src1_data: 0x%08x, src1_64: 0x%16llx\n", env->src1_data, 
	       (unsigned long long) env->src1_64);
	printf("src2_64: 0x%08x, src2_64: 0x%16llx\n", env->src2_data, 
	       (unsigned long long) env->src2_64);
}

/**************************************
* Start shared DSP instruction helpers
**************************************/

typedef enum {

	CLIP_NONE,
	CLIP_TO_SIGNED_16,
	CLIP_TO_UNSIGNED_16,
	CLIP_TO_SIGNED_32,
	CLIP_TO_UNSIGNED_32,

} CLIP_MODE;

/*
** Common epilogue for DSP functions.
*/

static void dsp_epilogue(CPUUBICOM32State *env, uint32_t opcode, uint64_t result_64, uint32_t add_flag, uint32_t clip_mode, uint64_t store_mask)
{
	int acc_index;
	int64_t result_s64;
	uint64_t acc_64, clipped_64;

	/*
	** The order of operations in this code is critical. Operations must be performed in this order:
	**
	** 1. Accumulate
	** 2. Clip (aka saturate)
	** 4. Final truncation
	**
	** If operations are not performed in this order, the verification tests will fail.
	*/

	/* Load the current accumulator value into acc_64 and sign-extend to 64 bits */

//	printf("dsp_epilogue(): result_64: %llx\n", result_64);

	acc_index = (opcode & DSP_A_BIT) ? REGISTER_ACC1_HI : REGISTER_ACC0_HI;

	if (add_flag) {

//		printf("adding to accumulator\n");
		acc_64 = (((uint64_t)env->thread_regs[acc_index] << 32) | env->thread_regs[acc_index + 1]) & 0xffffffffffffLL;
		acc_64 = (acc_64 & 0x800000000000LL) ? (acc_64 | 0xffff000000000000LL) : (acc_64 & 0xffffffffffffLL);

//		printf("acc_64: 0x%16llx\n", acc_64);

		/* Update the acc_64 with the DSP instruction result */

		result_64 += acc_64;
	}

	clipped_64 = result_64;

	switch (clip_mode) {

		case CLIP_TO_SIGNED_16:

			result_s64 = result_64;
			clipped_64 = (result_s64 > INT16_MAX) ? INT16_MAX : ((result_s64 < INT16_MIN) ? INT16_MIN : result_s64);
			break;

		case CLIP_TO_UNSIGNED_16:

			clipped_64 = (result_64 > INT16_MAX) ? INT16_MAX : ((result_64 < INT16_MIN) ? INT16_MIN : result_64);
			break;

		case CLIP_TO_SIGNED_32:
		
			result_s64 = result_64;
			clipped_64 = (result_s64 > INT32_MAX) ? INT32_MAX : ((result_s64 < INT32_MIN) ? INT32_MIN : result_64);
			break;

		case CLIP_TO_UNSIGNED_32:

			clipped_64 = (result_64 > UINT32_MAX) ? UINT32_MAX : result_64;
			break;
	}

	/*
	** IP8K ISA specification, section DSP Operations (Version 3+ only):
	**
	** "Whether the C bit is set or not, any operation that would have caused clipping
	** to change the result, will set the O bit in the CSR register."
	*/

	if (result_64 != clipped_64)
		env->thread_regs[0x2d] |= CSR_O_BIT;

//	printf("pre-clip result: 0x%llx\n", result_64);

	/* If the clip flag is set, then update acc_64 with the clipped value */

//	printf("opcode & DSP_C_BIT: %x\n", opcode & DSP_C_BIT);

	result_64 = (opcode & DSP_C_BIT) ? clipped_64 : result_64;

//	printf("post-clip result: 0x%llx\n", result_64);

	result_64 &= store_mask;

	env->thread_regs[acc_index]     = result_64 >> 32;
	env->thread_regs[acc_index + 1] = result_64;
}

/**************************************
* End shared DSP instruction helpers
**************************************/
/**************************************
* Start of 16-bit DSP instruction helpers
**************************************/

void HELPER(mulf)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	s1 = (int16_t)env->src1_data;
	s2 = (int16_t)env->src2_64;

	result_64 = (s1 * s2) << 1;

	/* "The result of -1.0 * -1.0 is clipped to the largest possible number 0.9999...."
	  Yeah, thanks for the precise explanation. */

//	printf("MULF: (%llx * %llx) = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 0, CLIP_TO_SIGNED_32, UINT48_MAX);
}

void HELPER(muls)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	s1 = (int16_t)env->src1_data;
	s2 = (int16_t)env->src2_64;

	result_64 = s1 * s2;

//	printf("MULS: (%llx * %llx) = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 0, CLIP_TO_SIGNED_32, UINT48_MAX);
}

void HELPER(mulu)(CPUUBICOM32State *env, uint32_t opcode)
{
	uint64_t s1, s2, result_64;

	s1 = (uint16_t)env->src1_data;
	s2 = (uint16_t)env->src2_64;

	result_64 = s1 * s2;

//	printf("MULU4: %llx * %llx = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 0, CLIP_NONE, UINT48_MAX);
}

void HELPER(macs)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	s1 = (int16_t)env->src1_data;
	s2 = (int16_t)env->src2_64;

	result_64 = s1 * s2;

	dsp_epilogue(env, opcode, result_64, 1, CLIP_TO_SIGNED_32, UINT48_MAX);
}

void HELPER(macf)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	s1 = (int16_t)env->src1_data;
	s2 = (int16_t)env->src2_64;

	result_64 = (s1 * s2) << 1;

//	printf("MACF: (%llx * %llx) << 1 = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 1, CLIP_TO_SIGNED_32, UINT48_MAX);
}

void HELPER(msuf)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	s1 = (int16_t)env->src1_data;
	s2 = (int16_t)env->src2_64;

	result_64 = -((s1 * s2) << 1);

//	printf("MSUF: (%llx * %llx) << 1 = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 1, CLIP_TO_SIGNED_32, UINT48_MAX);
}

void HELPER(macu)(CPUUBICOM32State *env, uint32_t opcode)
{
	uint64_t s1, s2, result_64;

	s1 = (uint16_t)env->src1_data;
	s2 = (uint16_t)env->src2_64;

	result_64 = s1 * s2;

//	printf("MACU: (%llx * %llx) = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 1, CLIP_TO_UNSIGNED_32, UINT48_MAX);
}

void HELPER(macus)(CPUUBICOM32State *env, uint32_t opcode)
{
	uint64_t s1, s2, result_64;

	s1 = (uint16_t)env->src1_data;
	s2 = (uint16_t)env->src2_64;

	result_64 = (s1 * s2) << 16;

//	printf("MACUS: (%llx * %llx) = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 1, CLIP_TO_UNSIGNED_32, UINT48_MAX);
}

void HELPER(madd2)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	/* This is intentional. RTL always uses 32-bit source1.  */

	s1 = (int32_t)env->src1_data;
	s2 = (opcode & DSP_S_BIT) ? env->src2_64 : (int16_t)env->src2_64;

	result_64 = s1 + s2;

//	printf("MADD2: %llx + %llx = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 0, CLIP_TO_SIGNED_16, UINT48_MAX);
}

void HELPER(msub2)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	/* This is intentional. RTL always uses 32-bit source1.  */

	s1 = (int32_t)env->src1_data;
	s2 = (opcode & DSP_S_BIT) ? env->src2_64 : (int16_t)env->src2_64;

	result_64 = s1 - s2;

//	printf("MSUB2: %llx - %llx = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 0, CLIP_TO_SIGNED_16, UINT48_MAX);
}

/**************************************
* End of 16-bit DSP instruction helpers
**************************************/
/**************************************
* Start of 32-bit DSP instruction helpers
**************************************/

void HELPER(muls4)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	s1 = (int32_t)env->src1_data;
	s2 = (int32_t)env->src2_64;

	result_64 = s1 * s2;

//	printf("MULS4: (%llx * %llx) = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 0, CLIP_NONE, UINT64_MAX);
}

void HELPER(mulu4)(CPUUBICOM32State *env, uint32_t opcode)
{
	uint64_t s1, s2, result_64;

	s1 = (uint32_t)env->src1_data;
	s2 = (uint32_t)env->src2_64;

	result_64 = s1 * s2;

//	printf("MULU4: (%llx * %llx) = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 0, CLIP_NONE, UINT64_MAX);
}

void HELPER(macu4)(CPUUBICOM32State *env, uint32_t opcode)
{
	uint64_t s1, s2, result_64;

	s1 = (uint32_t)env->src1_data;
	s2 = (uint32_t)env->src2_64;

	result_64 = s1 * s2;

	dsp_epilogue(env, opcode, result_64, 1, CLIP_TO_UNSIGNED_32, UINT64_MAX);
}

void HELPER(macs4)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	s1 = (uint32_t)env->src1_data;
	s2 = (uint32_t)env->src2_64;

	result_64 = s1 * s2;

	dsp_epilogue(env, opcode, result_64, 1, CLIP_TO_UNSIGNED_32, UINT64_MAX);
}

void HELPER(madd4)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	s1 = (int32_t)env->src1_data;
	s2 = env->src2_64;

	result_64 = s1 + s2;

//	printf("MADD.4: %llx + %llx = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 0, CLIP_TO_SIGNED_32, UINT48_MAX);
}

void HELPER(msub4)(CPUUBICOM32State *env, uint32_t opcode)
{
	int64_t s1, s2, result_64;

	s1 = (int32_t)env->src1_data;
	s2 = env->src2_64;

	result_64 = s1 - s2;

//	printf("MSUB: %llx - %llx = %llx\n", s1, s2, result_64);

	dsp_epilogue(env, opcode, result_64, 0, CLIP_TO_SIGNED_32, UINT48_MAX);
}

/**************************************
* End of 32-bit DSP instruction helpers
**************************************/

void HELPER(iprivilege_check)(CPUUBICOM32State *env)
{
    /*
     * If the Supervisor bit is set in CSR then this is a privilege violation.
     */
    if (env->thread_regs[0x2d] & (1 << CSR_PRIV_BIT)) {
	env->thread_regs[0x34] = env->current_pc;
	generate_trap(env, env->cpu_index, TRAP_CAUSE_PRIVILEGE);
	cpu_loop_exit(env);
    }
    return;
}

void HELPER(syscall)(CPUUBICOM32State *env)
{
    /*
     * Take current privilege bit and move it into the previous privilege bit position.
     */
    uint32_t prev_priv_bit = (env->thread_regs[0x2d] & (1 << CSR_PRIV_BIT)) << 1;

    /*
     * Drive the Prev and current privilege bits to zero. This will switch thread state to supervisor.
     */
    env->thread_regs[0x2d] &= ~CSR_PRIV_MASK;

    /*
     * Put the saved privilege bit into the csr.
     */
    env->thread_regs[0x2d] |= prev_priv_bit;

    /*
     * Load the Current PC from SEP register.
     */
    env->thread_regs[0x34] = env->global_regs[GLOBAL_REG(0x72)];
    return;
}

void HELPER(direct_register_privilege_check)(CPUUBICOM32State *env, uint32_t reg_index)
{
    /*
     * A thread in user mode has r/w access to a limited subset of registers.
     */
    if ((env->thread_regs[0x2d] & (1 << CSR_PRIV_BIT)) == 0) {
	/*
	 * Supervisor mode just return.
	 */
	return;
    }

	/* Write to a global register is allowed (even in user mode) if traps are disabled. */

    	if (!(env->global_regs[REGISTER_MT_TRAP_EN] & (1 << env->thread_number)))
		return;

    if (reg_index < 0x10) {
	/*
	 * D reg.
	 */
	return;
    }

    if (reg_index >= 0x20 && reg_index <= 0x2c) {
	/*
	 * A0 - inst_cnt.
	 */
	return;
    }

    if (reg_index == 0x36 || reg_index == 0x37 || reg_index == 0x39) {
	/*
	 * acc1_lo, acc1_hi, ucsr.
	 */
	return;
    }

    /*
     * Anyting else is a privilege violation.
     */
    generate_trap(env, env->cpu_index, TRAP_CAUSE_PRIVILEGE);
    cpu_loop_exit(env);
}

void HELPER(debug2)(CPUUBICOM32State *env, uint32_t opsize)
{
    printf("At debug %d\n", opsize);
}

void HELPER(debug3)(CPUUBICOM32State *env, uint32_t opsize)
{
    printf("At debug3\n");
}

/*
 * This is called for i_fetch cases only
 */
void inst_mapping_check(CPUUBICOM32State *env, uint32_t i_vaddr)
{
    uint32_t asid;
    uint32_t vpn;
    uint32_t tlb_miss_type;
    
    /*
     * Call tlb_check with is_read = 1 and is_ifetch = 1.
     */
    tlb_miss_type = tlb_check(env, &vpn, &asid, i_vaddr, NULL, 1, 1);
    
    if (tlb_miss_type == MMU_MISSQW0_TYPE_SERVICED) {
	return;
    } else if (tlb_miss_type == MMU_MISSQW0_TYPE_SERROR) {
	/*
	 * Raise exception and get out.
	 */
	env->thread_regs[0x34] = env->current_pc;
	raise_exception_with_env(env, EXCP_INTERRUPT);
    }

    /*
     * Create the fault. Set the dtlb field to 0. Create only slot 0 entry.
     * bale out of the whole dea.
     */
    create_fault(env->cpu_index, vpn , asid, tlb_miss_type, 0, 0);
    env->thread_regs[0x34] = env->current_pc;
    //raise_exception_with_env(env, EXCP_INTERRUPT);
}

    
    
/*
 * mapping_check
 *     This routine will check if a virtual address has entries for it in the ptec.
 *     If there are no entries then generate the fault and put entries in the missq.
 *     If there is permission missmatch then that is also a fault.
 */
void mapping_check(CPUUBICOM32State *env, uint32_t s1_vaddr, uint32_t d_vaddr, uint32_t check)
{
    uint32_t asid;
    uint32_t vpn;
    uint32_t tlb_miss_type;
    uint32_t slot = 0;

    if (check & S1_CHECK) {
	/*
	 * Call tlb_check with is_read = 1 and is_ifetch = 0.
	 */
	tlb_miss_type = tlb_check(env, &vpn, &asid, s1_vaddr, NULL, 1, 0);

	if (tlb_miss_type == MMU_MISSQW0_TYPE_SERROR) {
	    /*
	     * Raise exception and get out.
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    raise_exception(EXCP_INTERRUPT);
	    return;
	} else if (tlb_miss_type != MMU_MISSQW0_TYPE_SERVICED) {
	    /*
	     * Create the fault. Set the dtlb field to 1. Create only slot 0 entry.
	     */
	    create_fault(env->cpu_index, vpn , asid, tlb_miss_type, slot++, 1);
	}
    }

    if (check & DEST_CHECK) {
	/*
	 * Call tlb_check with is_read = 0 and is_ifetch = 0.
	 */
	tlb_miss_type = tlb_check(env, &vpn, &asid, d_vaddr, NULL, 0, 0);

	if (tlb_miss_type == MMU_MISSQW0_TYPE_SERROR) {
	    /*
	     * Raise exception and get out.
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    raise_exception(EXCP_INTERRUPT);
	    return;
	} else if (tlb_miss_type != MMU_MISSQW0_TYPE_SERVICED) {
	    /*
	     * Create the fault. Set the dtlb field to 1..
	     */
	    create_fault(env->cpu_index, vpn , asid, tlb_miss_type, slot++, 1);
	}
    }
    
    /*
     * If any fault got created then call the loop_exit
     */
    if (slot) {
 	env->thread_regs[0x34] = env->current_pc;
	raise_exception(EXCP_INTERRUPT);
    }
}

