/*
 *  UBICOM32 semihosting routines
 *
 *  Copyright (c) 2013 Qualcomm Atheros, Inc.
 *  Written by Michael Eager <eager@eagercon.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "cpu.h"
#include "helper.h"
#include "softmmu-semi.h"
#include "qemu-debug.h"
#include "gdbstub.h"

/* From libc/include/sys/_default_fcntl.h.  */
enum {
  FCNTL_FREAD	= 0x0001,	/* read enabled */
  FCNTL_FWRITE	= 0x0002,	/* write enabled */
  FCNTL_FAPPEND	= 0x0008,	/* append (writes guaranteed at the end) */
  FCNTL_FMARK	= 0x0010,	/* internal; mark during gc() */
  FCNTL_FDEFER	= 0x0020,	/* internal; defer for next gc pass */
  FCNTL_FASYNC	= 0x0040,	/* signal pgrp when data ready */
  FCNTL_FSHLOCK	= 0x0080,	/* BSD flock() shared lock present */
  FCNTL_FEXLOCK	= 0x0100,	/* BSD flock() exclusive lock present */
  FCNTL_FCREAT	= 0x0200,	/* open with file create */
  FCNTL_FTRUNC	= 0x0400,	/* open with truncation */
  FCNTL_FEXCL	= 0x0800,	/* error on open if file exists */
  FCNTL_FNBIO	= 0x1000,	/* non blocking I/O (sys5 style) */
  FCNTL_FSYNC	= 0x2000,	/* do all writes synchronously */
  FCNTL_FNONBLOCK = 0x4000,	/* non blocking I/O (POSIX style) */
  FCNTL_FNDELAY	= FCNTL_FNONBLOCK,	/* non blocking I/O (4.2 style) */
  FCNTL_FNOCTTY	= 0x8000	/* don't assign a ctty on this open */
};

/* System call numbers, matching libgloss/syscall.h. */
enum {
	TARGET_SYS_exit=1,
	TARGET_SYS_open=2,
	TARGET_SYS_close=3,
	TARGET_SYS_read=4,
	TARGET_SYS_write=5,
	TARGET_SYS_lseek=6,
	TARGET_SYS_unlink=7,
	TARGET_SYS_getpid=8,
	TARGET_SYS_kill=9,
	TARGET_SYS_fstat=10,
	TARGET_SYS_argvlen=12,
	TARGET_SYS_argv=13,
	TARGET_SYS_chdir=14,
	TARGET_SYS_stat=15,
	TARGET_SYS_chmod=16,
	TARGET_SYS_utime=17,
	TARGET_SYS_time=18,
	TARGET_SYS_gettimeofday=19,
	TARGET_SYS_times=20,
	TARGET_SYS_link=21,
	TARGET_SYS_argc=22,
	TARGET_SYS_argnlen=23,
	TARGET_SYS_argn=24,
	TARGET_SYS_reconfig=25
};

static unsigned int
translate_open_flags (unsigned int flags)
{
  unsigned int linux_flags = 0;
  unsigned int newlib_flags = flags + 1;

  if (newlib_flags & FCNTL_FREAD) linux_flags |= O_RDONLY;
  if (newlib_flags & FCNTL_FWRITE) linux_flags |= O_WRONLY;
  if (newlib_flags & FCNTL_FAPPEND) linux_flags |= O_APPEND;
  if (newlib_flags & FCNTL_FASYNC) linux_flags |= O_ASYNC;
  if (newlib_flags & FCNTL_FCREAT) linux_flags |= O_CREAT;
  if (newlib_flags & FCNTL_FTRUNC) linux_flags |= O_TRUNC;
  if (newlib_flags & FCNTL_FEXCL) linux_flags |= O_EXCL;
  if (newlib_flags & FCNTL_FSYNC) linux_flags |= O_SYNC;
  if (newlib_flags & FCNTL_FNDELAY) linux_flags |= O_NDELAY;
  if (newlib_flags & FCNTL_FNONBLOCK) linux_flags |= O_NONBLOCK;

  newlib_flags &= ~FCNTL_FREAD;
  newlib_flags &= ~FCNTL_FWRITE;
  newlib_flags &= ~FCNTL_FAPPEND;
  newlib_flags &= ~FCNTL_FCREAT;
  newlib_flags &= ~FCNTL_FTRUNC;
  newlib_flags &= ~FCNTL_FEXCL;
  newlib_flags &= ~FCNTL_FSYNC;
  newlib_flags &= ~FCNTL_FNDELAY;
  newlib_flags &= ~FCNTL_FNONBLOCK;
  assert (newlib_flags == 0);

  return linux_flags;
}

void HELPER(simcall)(CPUUBICOM32State *env)
{
    unsigned int syscall_num = env->thread_regs[REGISTER_D1];
    unsigned int arg1 =  env->thread_regs[REGISTER_D2];
    unsigned int arg2 =  env->thread_regs[REGISTER_D3];
    unsigned int arg3 =  env->thread_regs[REGISTER_D4];
    unsigned int rc = 0;
    char *s;

    if (qemu_debug & DEBUG_SIMCALL)
	printf ("simcall(num=%d, arg1=0x%8.8x, arg2=0x%8.8x, arg3=0x%8.8x)\n",
	        syscall_num, arg1, arg2, arg3);

    switch (syscall_num) {

    case TARGET_SYS_kill:
    case TARGET_SYS_exit:
	if (syscall_num == TARGET_SYS_kill) {
	    qemu_log("simcall kill\n");
	    qemu_log("simcall kill(%d)\n", arg1);
	    if (qemu_debug & DEBUG_SIMCALL)
		printf ("simcall kill (arg1=%d)\n", arg1);
	} else {
	    qemu_log("simcall exit\n");
	    qemu_log("simcall exit(%d)\n", arg1);
	    if (qemu_debug & DEBUG_SIMCALL)
		printf ("simcall exit (arg1=%d)\n", arg1);
	}

	if (use_gdb_syscalls()) {
	  gdb_exit(env, 0);
          exit(0);
	} else {
	  /* FIXME -- shutdown QEMU instead of exit.  */
	  exit (arg1);
	}
	break;

    case TARGET_SYS_open:
	qemu_log("simcall open\n");
	if (!(s = lock_user_string(arg1))) {
            /* FIXME - should this error code be -TARGET_EFAULT ? */
	    rc = -1;
	} else {
	    if (qemu_debug & DEBUG_SIMCALL)
		printf ("simcall open(%s, 0x%8.8x, 0x%8.8x)\n", s, translate_open_flags(arg2), arg3);
	    rc = open (s, translate_open_flags(arg2), arg3);
	    unlock_user(s, arg1, 0);
	}
	break;

    case TARGET_SYS_read:
	if (qemu_debug & DEBUG_SIMCALL)
	    printf ("simcall read(%d, 0x%8.8x, %d)\n", arg1, arg2, arg3);
	if (!(s = lock_user(VERIFY_READ, arg2, arg3, 1))) {
            /* FIXME - check error code? */
              rc = -1;
        } else {
            rc = read(arg1, s, arg3);
            unlock_user(s, arg2, arg3);
        }
	break;

    case TARGET_SYS_write:
	if (qemu_debug & DEBUG_SIMCALL)
	    printf ("simcall write(%d, 0x%8.8x, %d)\n", arg1, arg2, arg3);

	if (!(s = lock_user(VERIFY_WRITE, arg2, arg3, 1))) {
            /* FIXME - check error code? */
              rc = -1;
        } else {
            rc = write(arg1, s, arg3);
            unlock_user(s, arg2, 0);
        }
	break;

    case TARGET_SYS_close:
	if (qemu_debug & DEBUG_SIMCALL)
	    printf ("simcall close(%d, 0x%8.8x, %d)\n", arg1, arg2, arg3);
	qemu_log("simcall close\n");
	if (qemu_debug & DEBUG_SIMCALL)
	    printf ("simcall close(%d)\n", arg1);
	rc = close (arg1);
	break;

    default:
	qemu_log("%s(%d): syscall not implemented\n", __func__, syscall_num);
	printf ("%s(%d): syscall not implemented\n", __func__, syscall_num);
	exit(1);
   }

   if (qemu_debug & DEBUG_SIMCALL) printf ("simcall return rc = %d\n", rc);
   env->thread_regs[REGISTER_D0] = rc;
   env->thread_regs[REGISTER_D5] = errno;
}

