/*
 * QEMU/UBICOM32 OCP block.
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Nat Gurumoorthy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * This block contains
 *
 *    pll
 *    mailbox interface
 *    timer block
 *    MMU block.
 *
 * We will make this 4k block look like a piece of memory on the IO bus. We will set up read and write interfaces
 * to this block.
 *
 * We will have 2 Qemu timers to represent the MPT and Sysval block. We need to extract the frequency from the PLL
 * Registers settings. This will be different for 8k.
 */

#include <arpa/inet.h>
#include <stdint.h>

#include "sysemu.h"
#include "hw/boards.h"
#include "monitor.h"
#include "qemu-timer.h"
#include "qemu-char.h"
#include "qemu-log.h"
#include "cpu.h"
#include "memory.h"
#include "ubicom32_ocp.h"

#define USE_NONCACHEABLE_BIT

void irq_info(Monitor *mon);
void pic_info(Monitor *mon);


/*
 * ubicom32_io_timer
 */
struct ubicom32_io_timer {
    volatile uint32_t mptval;
    volatile uint32_t rtcom;
    volatile uint32_t tkey;
    volatile uint32_t wdcom;
    volatile uint32_t wdcfg;
    volatile uint32_t sysval;
    volatile uint32_t syscom[64 - 6];
    volatile uint32_t rsgcfg;
    volatile uint32_t trn;
} ubicom32_timer;

/*
 * ubicom32_mailbox
 */
struct ubicom32_mailbox {
    volatile uint32_t inmail;
    volatile uint32_t outmail;
    volatile uint32_t mail_stat;
};

#define ISD_MAILBOX_STATUS_IN_FULL (1 << 31)
#define ISD_MAILBOX_STATUS_IN_EMPTY (1 << 30)
#define ISD_MAILBOX_STATUS_OUT_FULL (1 << 29)
#define ISD_MAILBOX_STATUS_OUT_EMPTY (1 << 28)

/*
 * The serial data arriving from Qemu is stored in a 32 byte ring buffer. Write at head and read from tail.
 * The data will be taken from Qemu and placed into the ring buffer. Every time Ubicom reads the inmail location
 * we will give it data from the ring buffer tail. A read to the mail stat will have the IN_EMPTY bit clear if
 * there is data in the ring buffer. Write of data by UBICOM to the outmail register will be take and supplied
 * directly to Qemu. The out queue will be empty all the time.
 */ 
struct serial_in_data {
    uint32_t head;
    uint32_t tail;
    char data[32];
} qemu_in_data;

uint32_t ocp_general[0x40];

/*
 * OCP MMU block.
 */
uint32_t ocp_mmu[0x80];

/*
** Note: The values in the Jupiter Chip Specification section 9.1: MMU registers are BYTE indexes.
**       These are INT indexes (byte index / 4) 
*/

#define CONFIG		0
#define PTEC_CFG0	1
#define PTEC_CFG1	2
#define PTEC_CFG2	3
#define MISSQW0		4
#define MISSQW1		5
#define RESTART		6
#define INSERTW0	7
#define INSERTW1	8
#define I_PURGE		9
#define D_PURGE		10
#define PTEC_ERR	11
#define TLB_IDX		12
#define ENTRYW0		13
#define ENTRYW21	14
#define STATSU		15
#define BUS_ST0		16	/* A 1 indicates a thread in blocked state */
#define BUS_ST1		17	/* 1 = data bus blocked, 0 = instruction bus blocked */
#define BUS_ST2		18	/* 1 = MMU blocked 0 = Blocked by some other reason */
#define BUS_ST3		19	/* 1 = Cache blocked 0 = Blocked by some other reason */

/*************************************
* Start ASID access macros 
*************************************/

#define PGD(thread_num) 	(ocp_mmu[(0x100 + (thread_num << 4)) >> 2] >>  0)
#define PGD_INDEX(thread_num) 	((0x100 + (thread_num << 4)) >> 2)

#define ASID0(thread_num) 	((ocp_mmu[(0x104 + (thread_num << 4)) >> 2] >>  0) & 0x3ff)
#define ASID1(thread_num) 	((ocp_mmu[(0x104 + (thread_num << 4)) >> 2] >> 10) & 0x3ff)
#define ASID2(thread_num) 	((ocp_mmu[(0x104 + (thread_num << 4)) >> 2] >> 20) & 0x3ff)

#define CMP_0_EN(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >> 24) & 1)
#define CMP_1_EN(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >> 25) & 1)
#define CMP_2_EN(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >> 26) & 1)

#define ASID_CMP0(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >>  0) & 0xf)
#define ASID_CMP1(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >>  8) & 0xf)
#define ASID_CMP2(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >> 16) & 0xf)

#define ASID_MASK0(thread_num)	((ocp_mmu[(0x10c + (thread_num << 4)) >> 2] >>   0) & 0xf)
#define ASID_MASK1(thread_num)	((ocp_mmu[(0x10c + (thread_num << 4)) >> 2] >>   8) & 0xf)
#define ASID_MASK2(thread_num)	((ocp_mmu[(0x10c + (thread_num << 4)) >> 2] >>  16) & 0xf)

/*************************************
* End ASID access macros 
*************************************/
/*************************************
* Start PTEC access macros 
*************************************/

#define PTEC_ENABLED		(ocp_mmu[0] & (1 << 4))

#define ATAG_BASE		(ocp_mmu[0x8 >> 2] & (((1 << 25) - 1) << 7))
#define ENTRY_BASE		(ocp_mmu[0xc >> 2] & (((1 << 25) - 1) << 7))

/* This is the equivalent of "ENTRYO_BITS" in the manual, but renamed to be less confusing */

#define PTEC_ENTRIES_LOG2	((ocp_mmu[0x4 >> 2] >> 16) & ((1 << 5) - 1))

#define PTEC_ENTRIES_NUM 	(1 << PTEC_ENTRIES_LOG2)

#define EXTRACT_BITS_FROM_INT32(value, right_shift, bits_num)		(((unsigned int)value >> right_shift) & ((1 << bits_num) - 1))

#define PTEC_TAG0(atag_value)	EXTRACT_BITS_FROM_INT32(atag_value, 26,  6)
#define PTEC_TAG1(atag_value)	EXTRACT_BITS_FROM_INT32(atag_value, 10,  6)
#define PTEC_ASID0(atag_value)	EXTRACT_BITS_FROM_INT32(atag_value, 16, 10)
#define PTEC_ASID1(atag_value)	EXTRACT_BITS_FROM_INT32(atag_value,  0, 10)

/*************************************
* End PTEC access macros 
*************************************/
/*************************************
* Start TLB access macros 
*************************************/

#ifdef USE_NONCACHEABLE_BIT

/*
** The Jupiter hardware (and software) TLB value is in this format:
**
** |vpn|asid|ppn|r|w|x|s|v|
**
** where:
**
** field    description          # of  bits   bit positions
** --------------------------------------------------------
** vpn     virtual page number    19 bits       bits 53:35
** asid    address space ID       10 bits       bits 34:25
** ppn     physical page number   19 bits       bits 24:6
** nocache noncacheable bit        1 bit        bit  5  (unused by sidrefcompare)
** r       read permission         1 bit        bit  4
** w       write permission        1 bit        bit  3
** x       execute permission      1 bit        bit  2
** s       supervisor permission   1 bit        bit  1
** v       valid qualifier         1 bit        bit  0
**
** IMPORTANT NOTE IMPORTANT NOTE IMPORTANT NOTE IMPORTANT NOTE
**
** The "supervisor permission" bit is inverted!
**
** 0 = supervisor mode
** 1 = user mode
**
** IMPORTANT NOTE IMPORTANT NOTE IMPORTANT NOTE IMPORTANT NOTE
*/

/*
** Note: "tlb" is a long long * (pointer to a 64-bit value) macros should return int
** in case the code prints the value with %d or %x.
*/

/* The upper 19 bits of the virtual page of the TLB entry (19 bits) */
#define TLB_ENTRY_VPN(tlb_value)                ((int)(tlb_value >> 35) & 0x7ffff)

/* The ASID of the TLB entry (10 bits) */
#define TLB_ENTRY_ASID(tlb_value)               ((int)(tlb_value >> 25) & 0x3ff)

/* The physical page page number of the TLB entry (19 bits) */
#define TLB_ENTRY_PPN(tlb_value)                ((int)(tlb_value >> 6) & 0x7ffff)

/* The read permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_READ_BIT(tlb_value)           ((int)(tlb_value >> 4) & 1)

/* The write permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_WRITE_BIT(tlb_value)          ((int)(tlb_value >> 3) & 1)

/* The execute permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_EXECUTE_BIT(tlb_value)        ((int)(tlb_value >> 2) & 1)

/* The supervisor permission bit for the TLB entry (1 bit) */
/* 0 = supervisor mode, 1 = user mode - this is the way the hardware works! */
#define TLB_ENTRY_SUPERVISOR_BIT(tlb_value)     ((int)(tlb_value >> 1) & 1)

/* The valid bit for the TLB entry (1 bit) */
#define TLB_ENTRY_VALID_BIT(tlb_value)          ((int)(tlb_value >> 0) & 1)

#else

/* The upper 19 bits of the virtual page of the TLB entry (19 bits) */
#define TLB_ENTRY_VPN(tlb_value)                ((int)(tlb_value >> 34) & 0x7ffff)

/* The ASID of the TLB entry (10 bits) */
#define TLB_ENTRY_ASID(tlb_value)               ((int)(tlb_value >> 24) & 0x3ff)

/* The physical page page number of the TLB entry (20 bits) */
#define TLB_ENTRY_PPN(tlb_value)                ((int)(tlb_value >> 5) & 0x7ffff)

/* The read permission bit for the TLB entry (1 bit)*/
#define TLB_ENTRY_READ_BIT(tlb_value)           ((int)(tlb_value >> 4) & 1)

/* The write permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_WRITE_BIT(tlb_value)          ((int)(tlb_value >> 3) & 1)

/* The execute permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_EXECUTE_BIT(tlb_value)        ((int)(tlb_value >> 2) & 1)

/* The supervisor permission bit for the TLB entry (1 bit) */
/* 0 = supervisor mode, 1 = user mode - this is the way the hardware works! */
#define TLB_ENTRY_SUPERVISOR_BIT(tlb_value)     ((int)(tlb_value >> 1) & 1)

/* The valid bit for the TLB entry (1 bit) */
#define TLB_ENTRY_VALID_BIT(tlb_value)          ((int)(tlb_value >> 0) & 1)

#endif

/*************************************
* Start TLB access macros 
*************************************/
/*************************************
* Start other MMU macros 
*************************************/

/* Number of hardware TLB entries */
#define TLB_ENTRIES_NUM 64

/* MMU page size - 1 = 8kbytes, 3 = 16kbytes */
#define PAGE_MASK			(ocp_mmu[0] & 3)
#ifdef USE_NONCACHEABLE_BIT
#define RTAG_MASK			((ocp_mmu[4 >> 2] & ((1 << 7) - 1)) >> 1)
#else
#define RTAG_MASK			(ocp_mmu[4 >> 2] & ((1 << 7) - 1))
#endif

/*************************************
* End other MMU macros 
*************************************/

#define GEN_GET_CLK_PLL_NR(v) ((((v) >> 23) & 0x003f) + 1)
#define GEN_GET_CLK_PLL_NF(v) ((((v) >> 11) & 0x0fff) + 1)
#define GEN_GET_CLK_PLL_OD(v) ((((v) >> 8) & 0x7) + 1)

struct miss_queue_entry {
    uint32_t w0;		/* This word holds PGD, TYPE, SRC, TNUM */
    uint32_t w1;		/* This word holds VPN, MULTIPLE, SRC and ASID */
};

/*
 * For each thread there are 2 slots. 
 */
struct per_thread_miss_queue_entries {
    struct miss_queue_entry slot[2];
};

/*
 * m_queue is the array that has the per thread miss queue slots.
 */
static struct miss_queue {
    struct per_thread_miss_queue_entries m_q_entries[12];
} m_queue;

/*
 * The miss queue is implemented as a 13 entry ring buffer. Write at head and read from tail.
 * When we have to generate a tlb miss we first go scribble over the entry/entries for a specific thread
 * in the m_queue data structure.
 * We then have to enter the thread index to the end of the m_t_ix_queue if the thread index is not
 * already in the queue. If thread 1 is already in the queue then we will just write over the information
 * in the m_queue but we will leave the entry in m_t_ix_queue alone. 
 * For the ring buffer if head == tail then queue is empty. We will stop adding entries to the queue
 * if ((head +1 ) %13) == tail.
 */ 
static struct miss_thread_index_queue {
    uint32_t head;
    uint32_t tail;
    uint32_t miss_thread_index[13];
} m_t_ix_queue;

/* 
 * Entries in this data structure get updated when writes happen to CONFIG and
 * PTEX_CFG0 - 2 registers.
 */
static struct mmu_config_state {
    uint32_t ptec_enabled;
    uint32_t vpn_bits;
    uint32_t extra_asid_bits;
    uint32_t ptec_entries_log2;
    uint32_t ptec_index_mask;
    uint32_t atag_base;
    uint32_t entry_base;
    uint32_t rtag_mask;
} ubicom32_mmu;

#define VPN_BITS(x) ((x) >> (32 - ubicom32_mmu.vpn_bits))
#define RESTORE_VPN_BITS(x) ((x) << (32 - ubicom32_mmu.vpn_bits))

static int select_ASID(uint32_t current_thread_num, uint32_t vaddr, uint32_t *asid)

{
    uint32_t upper_vaddr_bits, matches;

    matches = 0;

    upper_vaddr_bits = (vaddr >> 28) & 0x0f;

    if (CMP_0_EN(current_thread_num)) {
	if ((upper_vaddr_bits & ASID_MASK0(current_thread_num)) == ASID_CMP0(current_thread_num)) {
	    *asid = ASID0(current_thread_num);
	    matches++;
	}
    }

    if (CMP_1_EN(current_thread_num)) {
	if ((upper_vaddr_bits & ASID_MASK1(current_thread_num)) == ASID_CMP1(current_thread_num)) {
	    *asid = ASID1(current_thread_num);
	    matches++;
	}
    }

    if (CMP_2_EN(current_thread_num)) {
	if ((upper_vaddr_bits & ASID_MASK2(current_thread_num)) == ASID_CMP2(current_thread_num)) {
	    *asid = ASID2(current_thread_num);
	    matches++;
	}
    }

    return matches;
}

/*************************************
* Start check PTEC check code
*************************************/

/*
** The PTEC checking code herein performs two primary functions:
**
** o Performs a sanity check on the order of MMU read accesses.
**   It ensures the MMU reads the atag, way0, and way1 arrays in the correct order.
**
** o It keeps track of the current PTEC index accesse by the MMU for later use by other routines.
**/

/* This is the equivalent of "ENTRY_BITS" in the manual, but renamed to be less confusing */

#define PTEC_ENTRIES_MASK       ((1 << PTEC_ENTRIES_LOG2) - 1)

typedef enum {

	EXPECTING_PTEC_ATAG_READ,
	EXPECTING_PTEC_ATAG_OR_WAY0_OR_WAY1_READ,
	EXPECTING_PTEC_WAY1_READ,
	EXPECTING_PTEC_WAY1_OR_ATAG_READ

} MMU_PTEC_STATE;

/* The current state of the PTEC-tracking state machine */
MMU_PTEC_STATE mmu_ptec_state;

/* The last MMU PTEC index accessed (deduced from MMU Read lines in mp.trace) */
int last_ptec_index;

/*************************************
* End check PTEC check code
*************************************/
#define PTEC_USER	(1 << 1)
#define PTEC_EXECUTE	(1 << 2)
#define PTEC_WRITE	(1 << 3)
#define PTEC_READ	(1 << 4)

/*
 * This routine will take a vaddr and see if it is present in the ptec.
 * If it is not it will return READ_MISS/ WRITE_MISS
 * If there is a hit it will test and see the privilege bits and return the 
 * PRIV_ACC_READ/Write. The caller will then have to generate the
 * proper entries in the miss queue.
 * If the asid select bombs it will generate the proper synchronous error trap
 * and fource it to bale out to QEMU outer loop
 */
uint32_t tlb_check(CPUUBICOM32State *env, uint32_t *vpn, uint32_t *asid, uint32_t vaddr, uint32_t *return_ptec_entry, uint32_t is_read, uint32_t is_ifetch)
{
    uint32_t ptec_index = 0;
    uint32_t thread_mask = 1 << env->cpu_index;
    uint32_t match;
    uint32_t asid_mask;
    uint32_t atag, rtag;
    uint32_t ptec_entry;
    uint32_t atag_entry;
    uint32_t user_mode = env->thread_regs[0x2d] & (1 << CSR_PRIV_BIT);
    
    match = select_ASID(env->cpu_index, vaddr, asid);

    /*
     * If match is any value other than 1 then trigger a ASYNC_TRAP and get out.
     */
    if (match != 1) {
	/*
	 * Generate a synchronous error trap and get out.
	 */
	if (is_ifetch) {
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_I_SERROR);
	} else if (is_read) {
	    /*
	     * SRC1_SERROR
	     */
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_SRC1_SERROR);
	} else {
	    /*
	     * DST_SERROR.
	     */
	    generate_trap(env, env->cpu_index, TRAP_CAUSE_DST_SERROR);
	}
	
	/*
	 * Raise the synchronous error interrupt in int_stat0
	 */
	ubicom32_raise_interrupt(31);

	return MMU_MISSQW0_TYPE_SERROR;
    }

    /*
     * Got a valid asid. Now compute the ptec_index.
     */
    *vpn = VPN_BITS(vaddr);
    asid_mask = *asid << ubicom32_mmu.extra_asid_bits;
    ptec_index = (*vpn ^ asid_mask) & ubicom32_mmu.ptec_index_mask;
    atag = (((*vpn >> ubicom32_mmu.ptec_entries_log2) & 0x3f) << 10) | *asid;
    rtag = (*vpn >> (ubicom32_mmu.ptec_entries_log2 + 6)) & ubicom32_mmu.rtag_mask;

    /*
     * Go fetch the atag entry from PTEC.
     */
    atag_entry = ldl_phys(ubicom32_mmu.atag_base + ptec_index * 4);

    if ((atag_entry & 0xffff) == atag) {
        /*
         * hit on way 1. Read the ptec_entry from the correct array.
         */
        ptec_entry = ldl_phys(ubicom32_mmu.entry_base + (ptec_index  + ( 1 << ubicom32_mmu.ptec_entries_log2)) * 4);
    } else if (((atag_entry >> 16) & 0xffff) == atag) {
        /*
         * hit on way 0. Read the ptec_entry from the correct array.
         */
        ptec_entry = ldl_phys(ubicom32_mmu.entry_base + ptec_index * 4);
    } else {
        /*
         * Generate a TLB miss and return.
         */
        if (is_read) {
            return MMU_MISSQW0_TYPE_MISS_READ;
        }

            return MMU_MISSQW0_TYPE_MISS_WRITE;
    }

    if (return_ptec_entry) {
        *return_ptec_entry = ptec_entry;
    }
    /*
     * Test for a rtag hit.
     */
    if (((ptec_entry >> 5) & ubicom32_mmu.rtag_mask) != rtag) {
        /*
         * Rtag mismatch. Generate a TLB miss and return.
         */
        if (is_read) {
            return MMU_MISSQW0_TYPE_MISS_READ;
        }

        return MMU_MISSQW0_TYPE_MISS_WRITE;
    }

    /*
     * Atag and rtag match. Now to check if we have proper privileges.
     */
    if (user_mode && ((ptec_entry & PTEC_USER) == 0)) {
        /*
         * Privelage violation
         */
        if (is_read) {
            return MMU_MISSQW0_TYPE_PRIVACC_READ;
        }

        return MMU_MISSQW0_TYPE_PRIVACC_WRITE;
    }

    /*
     * If i_fetch and no execute then it is a READ violation
     */
    if (is_ifetch) {
        if ((ptec_entry & PTEC_EXECUTE) == 0) {
            /*
             * Privelage violation
             */
            return MMU_MISSQW0_TYPE_PRIVACC_READ;
        }

        return MMU_MISSQW0_TYPE_SERVICED;
    }


    /*
     * If read and no read permission then it is a READ violation
     */
    if (is_read) {
        if ((ptec_entry & PTEC_READ) == 0) {
            /*
             * Privelage violation
             */
            return MMU_MISSQW0_TYPE_PRIVACC_READ;
        }

        return MMU_MISSQW0_TYPE_SERVICED;
    }

    /*
     * If no write permission then this is write violation
     */
    if ((ptec_entry & PTEC_WRITE) == 0) {
        /*
         * Privelage violation
         */
        return MMU_MISSQW0_TYPE_PRIVACC_WRITE;
    }

    /*
     * This has been successfuly serviced.
     */
    return MMU_MISSQW0_TYPE_SERVICED;
}

/*
 * This routine will take a vaddr and see if it is present in the ptec.
 * If it is not it will return READ_MISS/ WRITE_MISS
 * If there is a hit it will test and see the privilege bits and return the 
 * PRIV_ACC_READ/Write. The caller will then have to generate the
 * proper entries in the miss queue.
 * If the asid select bombs it will generate the proper synchronous error trap
 * and fource it to bale out to QEMU outer loop
 */
uint32_t tlb_check_no_serror(CPUUBICOM32State *env, uint32_t *vpn, uint32_t *asid, uint32_t vaddr, uint32_t *return_ptec_entry, uint32_t is_read, uint32_t is_ifetch)
{
    uint32_t ptec_index = 0;
    uint32_t match;
    uint32_t asid_mask;
    uint32_t atag, rtag;
    uint32_t ptec_entry;
    uint32_t atag_entry;
    uint32_t user_mode = env->thread_regs[0x2d] & (1 << CSR_PRIV_BIT);
    
    match = select_ASID(env->cpu_index, vaddr, asid);

    /*
     * If match is any value other than 1 then trigger a ASYNC_TRAP and get out.
     */
    if (match != 1) {
	return MMU_MISSQW0_TYPE_SERROR;
    }

    /*
     * Got a valid asid. Now compute the ptec_index.
     */
    *vpn = VPN_BITS(vaddr);
    asid_mask = *asid << ubicom32_mmu.extra_asid_bits;
    ptec_index = (*vpn ^ asid_mask) & ubicom32_mmu.ptec_index_mask;
    atag = (((*vpn >> ubicom32_mmu.ptec_entries_log2) & 0x3f) << 10) | *asid;
    rtag = (*vpn >> (ubicom32_mmu.ptec_entries_log2 + 6)) & ubicom32_mmu.rtag_mask;

    /*
     * Go fetch the atag entry from PTEC.
     */
    atag_entry = ldl_phys(ubicom32_mmu.atag_base + ptec_index * 4);

    if ((atag_entry & 0xffff) == atag) {
	/*
	 * hit on way 1. Read the ptec_entry from the correct array.
	 */
	ptec_entry = ldl_phys(ubicom32_mmu.entry_base + (ptec_index  + ( 1 << ubicom32_mmu.ptec_entries_log2)) * 4);
    } else if (((atag_entry >> 16) & 0xffff) == atag) {
	/*
	 * hit on way 0. Read the ptec_entry from the correct array.
	 */
	ptec_entry = ldl_phys(ubicom32_mmu.entry_base + ptec_index * 4);
    } else {
	/*
	 * Generate a TLB miss and return.
	 */
	if (is_read) {
	    return MMU_MISSQW0_TYPE_MISS_READ;
	}

	    return MMU_MISSQW0_TYPE_MISS_WRITE;
    }

    if (return_ptec_entry) {
	*return_ptec_entry = ptec_entry;
    }

    /*
     * Test for a rtag hit.
     */
    if (((ptec_entry >> 5) & ubicom32_mmu.rtag_mask) != rtag) {
	/*
	 * Rtag mismatch. Generate a TLB miss and return.
	 */
	if (is_read) {
	    return MMU_MISSQW0_TYPE_MISS_READ;
	}

	return MMU_MISSQW0_TYPE_MISS_WRITE;
    }

    /*
     * Atag and rtag match. Now to check if we have proper privileges.
     */
    if (user_mode && ((ptec_entry & PTEC_USER) == 0)) {
	/*
	 * Privelage violation
	 */
	if (is_read) {
	    return MMU_MISSQW0_TYPE_PRIVACC_READ;
	}

	return MMU_MISSQW0_TYPE_PRIVACC_WRITE;
    }

    /*
     * If i_fetch and no execute then it is a READ violation
     */
    if (is_ifetch) {
	if ((ptec_entry & PTEC_EXECUTE) == 0) {
	    /*
	     * Privelage violation
	     */
	    return MMU_MISSQW0_TYPE_PRIVACC_READ;
	} 
	
	return MMU_MISSQW0_TYPE_SERVICED;
    }
	

    /*
     * If read and no read permission then it is a READ violation
     */
    if (is_read) {
	if ((ptec_entry & PTEC_READ) == 0) {
	    /*
	     * Privelage violation
	     */
	    return MMU_MISSQW0_TYPE_PRIVACC_READ;
	}

	return MMU_MISSQW0_TYPE_SERVICED;
    }
	
    /*
     * If no write permission then this is write violation
     */
    if ((ptec_entry & PTEC_WRITE) == 0) {
	/*
	 * Privelage violation
	 */
	return MMU_MISSQW0_TYPE_PRIVACC_WRITE;
    }

    /*
     * This has been successfuly serviced.
     */
    return MMU_MISSQW0_TYPE_SERVICED;
}

void create_fault(uint32_t tnum, uint32_t vpn, uint32_t asid, uint32_t type, uint32_t slot, uint32_t dtlb) 
{
    uint32_t w0, w1;
    uint32_t ix_q_tail = m_t_ix_queue.tail;

    if ((dtlb == 0) && (type == MMU_MISSQW0_TYPE_PRIVACC_WRITE)) {
	    printf("Bad combination\n");
    }

    /*
     * Create w0  PGD|TYPE|SRC|TNUM
     */
    w0 = PGD(tnum) | (type << 6) | (dtlb << 5) | tnum;
    
    /*
     * Create w1 VPN|MULTIPLE|SRC|ASID
     */
    w1 = (RESTORE_VPN_BITS(vpn)) | (dtlb << 10) | asid;

    /*
     * Load the entries.
     */
    m_queue.m_q_entries[tnum].slot[slot].w0 = w0;
    m_queue.m_q_entries[tnum].slot[slot].w1 = w1;
    
    /*
     * If slot number is 1 we have to set the multiple bit in slot 0.
     */
    if (slot) {
	m_queue.m_q_entries[tnum].slot[0].w1 |= (1 << 11);
    }

    /*
     * If the slot is zero see if the thread is already in the ring buffer. If it is nothing else to do.
     */
    if (slot) {
	return;
    }

    while (ix_q_tail != m_t_ix_queue.head) {
	if (m_t_ix_queue.miss_thread_index[ix_q_tail++] == tnum) {
	    /*
	     * Preexisting entry. Nothing to do.
	     */
	    return;
	}
	ix_q_tail %= 13;
    }

    /*
     * Add the thread to the queue via the head pointer.
     */
    m_t_ix_queue.miss_thread_index[m_t_ix_queue.head++] = tnum;
    m_t_ix_queue.head %= 13;

    /*
     * Raise the MMU interrupt.
     */
    ubicom32_raise_interrupt(29);

    /*
     * Block the thread. 
     * BUS_ST0 set the thread bit.
     * BUS_ST1 set bit for dtlb, clrer for itlb.
     * BUS_ST2 set bit for MMU fault.
     * BUS_ST3 don't care.
     */
    tnum = 1 << tnum;
    ocp_mmu[BUS_ST0] |= tnum;
    ocp_mmu[BUS_ST2] |= tnum;
    if (dtlb) {
	/*
	 * Set bit in BUS_ST1
	 */
	ocp_mmu[BUS_ST1] |= tnum;
    } else {
	/*
	 * Clear the bit.
	 */
	ocp_mmu[BUS_ST1] &= ~tnum;
    }
}

static void ubicom32_ocp_qemu_writeb (void *opaque, target_phys_addr_t addr,
			      uint32_t val)
{
    struct ocp_info *ocp = opaque;

    addr &= 0xfff;
    if (addr < 0x100) {
	uint32_t word_address = addr & ~0x3;
	uint32_t sub_byte = addr & 0x3;
	uint64_t now, next;
	uint32_t shift = 3 - sub_byte;
	uint32_t mask = 0xff << shift;
	
	val <<= shift;

	/*
	 * Convert word address into an index
	 */
	word_address /= 4;
	ocp_general[word_address] &= ~mask;
	ocp_general[word_address] |= val;

	if (addr < 4) {
	    /*
	     * Write to core PLL.
	     */
	    uint32_t pll_val = ocp_general[word_address];

	    /*
	     * If PLL output source is external clock then change the sysval frequency to 12000000 Else compute it.
	     */
	    if (pll_val & 0x10) {
		/*
		 * Compute the PLL frequency.
		 */
		uint32_t nr = GEN_GET_CLK_PLL_NR(pll_val);
		uint32_t nf = GEN_GET_CLK_PLL_NF(pll_val);
		uint32_t od = GEN_GET_CLK_PLL_OD(pll_val);
		ocp->sysval_frequency = ((ocp->mptval_frequency / nr) * nf) / od;

		if ((pll_val & 0xf) != 1) {
		    uint32_t output_div = (pll_val & 0xf) + 1;
		    ocp->sysval_frequency /= output_div;
		}

	    } else {
		ocp->sysval_frequency = 12000000;
	    }

	    /*
	     * Readjust the sysval timer to the new frequency.
	     */
	    now = qemu_get_clock_ms(vm_clock);
	    next = now + muldiv64(1, get_ticks_per_sec(), ocp->sysval_frequency);
	    qemu_mod_timer(ocp->sysval_timer, next);
	    return;
	}
    }
    //printf("Byte access at address 0x%03x val = 0x%08x\n", addr&0xfff, val);
}

static void ubicom32_ocp_qemu_writew (void *opaque, target_phys_addr_t addr,
			      uint32_t val)
{
    //printf("Word access at address 0x%03x val = 0x%08x\n", addr&0xfff, val);
    struct ocp_info *ocp = opaque;

    uint32_t word_address = addr & ~0x3;
    uint32_t sub_byte = addr & 0x3;
    addr &= 0xfff;
    if (addr < 0x100) {
	uint64_t now, next;

	/*
	 * Convert word address into an index
	 */
	word_address /= 4;

	if (sub_byte == 0) {
	    /*
	     * Write to the upper half of the location.
	     */
	    ocp_general[word_address] &=  0x0000ffff;
	    ocp_general[word_address] |= (val << 16);
	} else {
	    /*
	     * Write to lower half of pll reg.
	     */
	    ocp_general[word_address] &= 0xffff0000;
	    ocp_general[word_address] |= (val & 0xffff);
	}

	if (addr == 0 || addr == 2) {
	    uint32_t pll_val = ocp_general[word_address];
	    /*
	     * If PLL output source is external clock then change the sysval frequency to 12000000 Else compute it.
	     */
	    if (pll_val & 0x10) {
		/*
		 * Compute the PLL frequency.
		 */
		uint32_t nr = GEN_GET_CLK_PLL_NR(pll_val);
		uint32_t nf = GEN_GET_CLK_PLL_NF(pll_val);
		uint32_t od = GEN_GET_CLK_PLL_OD(pll_val);
		ocp->sysval_frequency = ((ocp->mptval_frequency / nr) * nf) / od;

		if ((pll_val & 0xf) != 1) {
		    uint32_t output_div = (pll_val & 0xf) + 1;
		    ocp->sysval_frequency /= output_div;
		}

	    } else {
		ocp->sysval_frequency = 12000000;
	    }

	    /*
	     * Readjust the sysval timer to the new frequency.
	     */
	    now = qemu_get_clock_ms(vm_clock);
	    next = now + muldiv64(1, get_ticks_per_sec(), ocp->sysval_frequency);
	    qemu_mod_timer(ocp->sysval_timer, next);
	    return;
	}
    } else if ((addr >= ocp->def->timer_offset) && addr < ocp->def->timer_offset + 0x100) {

	printf("Word write to timer\n");
	if (word_address == ocp->def->timer_offset + 0x4) {
	    /*
	     * Write to timer rtcom register.
	     */
	    if (sub_byte) {
		/*
		 * Write to lower half.
		 */
		ubicom32_timer.rtcom &= ~0xffff;
		ubicom32_timer.rtcom |= val & 0xffff;
	    } else {
		/*
		 * Write to the upper half.
		 */
		ubicom32_timer.rtcom &= ~0xffff0000;
		ubicom32_timer.rtcom |= (val & 0xffff) <<16;
	    }
	    return ;
	}

	if (word_address < ocp->def->timer_offset + 0x18) {
	    /*
	     * Write to watchdog. Currently not implemented.
	     */
	    return;
	}

	/*
	 * Write to a syscom register. Convert the address to an index.
	 */
	word_address -= (ocp->def->timer_offset + 0x18);
	addr >>= 2;

	if (sub_byte) {
	    /*
	     * Write to lower half.
	     */
	    ubicom32_timer.syscom[word_address] &= ~0xffff;
	    ubicom32_timer.syscom[word_address] |= val & 0xffff;
	} else {
	    /*
	     * Write to the upper half.
	     */
	    ubicom32_timer.syscom[word_address] &= ~0xffff0000;
	    ubicom32_timer.syscom[word_address] |= (val & 0xffff) <<16;
	}
	return;
    }
}

static void ubicom32_ocp_qemu_writel (void *opaque, target_phys_addr_t addr,
			      uint32_t val)
{
    struct ocp_info *ocp = opaque;

    addr &= 0xfff;

    if (addr < 0x100) {
	if (addr == 0) {
	    uint64_t now, next;
	    /*
	     * Write to PLL config register. Grab it a calculate core clock frequency.
	     */
	    ocp_general[0] = val;

	    /*
	     * If PLL output source is external clock then change the sysval frequency to 12000000 Else compute it.
	     */
	    if (val & 0x10) {
		/*
		 * Compute the PLL frequency.
		 */
		uint32_t nr = GEN_GET_CLK_PLL_NR(val);
		uint32_t nf = GEN_GET_CLK_PLL_NF(val);
		uint32_t od = GEN_GET_CLK_PLL_OD(val);
		ocp->sysval_frequency = ((ocp->mptval_frequency / nr) * nf) / od;
		
		if ((val & 0xf) != 1) {
		    uint32_t output_div = (val & 0xf) + 1;
		    ocp->sysval_frequency /= output_div;
		}
		
	    } else {
		ocp->sysval_frequency = 12000000;
	    }

	    /*
	     * Readjust the sysval timer to the new frequency.
	     */
	    now = qemu_get_clock_ms(vm_clock);
	    next = now + muldiv64(1, get_ticks_per_sec(), ocp->sysval_frequency);
	    qemu_mod_timer(ocp->sysval_timer, next);
	    return;
	} 

	/*
	 * Write to a general OCP block. Convert to an index and note the value.
	 */
	addr /= 4;
	ocp_general[addr] = val;
	return;
    } else if (addr == (ocp->def->mailbox_offset + 4)) {
	/*
	 * Write to mailbox output queue. Capture the data and send it to qemu.
	 */
	const uint8_t output = val & 0xff;

	(void) qemu_chr_fe_write(ocp->chr, &output, 1);
	return;
    } else if ((addr >= ocp->def->timer_offset) && addr < ocp->def->timer_offset + 0x100) {
	if (addr == ocp->def->timer_offset + 0x4) {
	    /*
	     * Write to timer rtcom register.
	     */
	    ubicom32_timer.rtcom = val;
	    return ;
	}

	if (addr < ocp->def->timer_offset + 0x18) {
	    /*
	     * Write to watchdog. Currently not implemented.
	     */
	    return;
	}

	/*
	 * Write to a syscom register. Convert the address to an index.
	 */
	addr -= (ocp->def->timer_offset + 0x18);
	addr >>= 2;

	ubicom32_timer.syscom[addr] = val;
	return;
    } else if ((addr >= ocp->def->mmu_offset) && addr < ocp->def->mmu_offset + 0x200) {
	uint32_t tnum;
	addr -= ocp->def->mmu_offset;

	/*
	 * Write to MMU block. Convert address into an index.
	 */
	addr >>= 2;

	switch (addr) {
	case CONFIG:
	    ocp_mmu[CONFIG] = val;
	    if (PTEC_ENABLED) {
		ubicom32_mmu.ptec_enabled = 1;
		ubicom32_mmu.extra_asid_bits = (ubicom32_mmu.ptec_entries_log2 > 10) ? (ubicom32_mmu.ptec_entries_log2 - 10) : 0;
		ubicom32_mmu.ptec_index_mask = (1 << ubicom32_mmu.ptec_entries_log2) -1;
	    } else {
		ubicom32_mmu.ptec_enabled = 0;
	    }

	    if (PAGE_MASK == 1) {
		ubicom32_mmu.vpn_bits = 19;
	    } else if (PAGE_MASK == 3) {
		ubicom32_mmu.vpn_bits = 18;
	    } else {
		ubicom32_mmu.vpn_bits = 20;
	    }
		
	    return;
	    break;
	case PTEC_CFG0:
	    ocp_mmu[addr] = val;
	    ubicom32_mmu.ptec_entries_log2 = PTEC_ENTRIES_LOG2;
	    ubicom32_mmu.rtag_mask = RTAG_MASK;

	    /*
	     * Set the extra_asid_bits since we know the ptec_entries_log2.
	     */
	    ubicom32_mmu.extra_asid_bits = (ubicom32_mmu.ptec_entries_log2 > 10) ? (ubicom32_mmu.ptec_entries_log2 - 10) : 0;
	    ubicom32_mmu.ptec_index_mask = (1 << ubicom32_mmu.ptec_entries_log2) -1;
	    return;
	    break;
	case PTEC_CFG1:
	    ocp_mmu[addr] = val;
	    ubicom32_mmu.atag_base = ATAG_BASE;
	    return;
	    break;
	case PTEC_CFG2:
	    ocp_mmu[addr] = val;
	    ubicom32_mmu.entry_base = ENTRY_BASE;
	    return;
	    break;
	case RESTART:
	    /*
	     * Grab the tnum from the sent value and convert it to a mask.
	     */
	    tnum = 1 << (val & 0x1f);

	    /*
	     * Clear the bit in BUS_ST0
	     */
	    ocp_mmu[BUS_ST0] &= ~tnum;
	    return;
	    break;
	case INSERTW0:
	    //printf("INSERTw0 0x%x\n", val);
	    return;
	    break;
	case INSERTW1:
	    //printf("INSERTw1 0x%x\n", val);
	    return;
	    break;
	case I_PURGE:
	case D_PURGE:
	    {
		/*
		 * Blow out all the tlb entries.
		 */
		uint32_t i;
		for (i = 0; i < ocp->def->num_threads; i++) {
		    tlb_flush(threads[i], 1);
		}
		break;
	    }
	case TLB_IDX:
	    /*
	     * Drop the write on the floor. XXXXXX if we write an extensive TLB model then this will have stuff in it.
	     */
	    break;
	    return;
	case PGD_INDEX(0):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[0], 1);
	    return;
	    break;
	case PGD_INDEX(1):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[1], 1);
	    return;
	    break;
	case PGD_INDEX(2):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[2], 1);
	    return;
	    break;
	case PGD_INDEX(3):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[3], 1);
	    return;
	    break;
	case PGD_INDEX(4):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[4], 1);
	    return;
	    break;
	case PGD_INDEX(5):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[5], 1);
	    return;
	    break;
	case PGD_INDEX(6):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[6], 1);
	    return;
	    break;
	case PGD_INDEX(7):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[7], 1);
	    return;
	    break;
	case PGD_INDEX(8):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[8], 1);
	    return;
	    break;
	case PGD_INDEX(9):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[9], 1);
	    return;
	    break;
	case PGD_INDEX(10):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[10], 1);
	    return;
	    break;
	case PGD_INDEX(11):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[11], 1);
	    return;
	    break;
	default:
	    //printf("Write to 0x%08x val 0x%08x\n", (ocp->def->mmu_offset + (addr << 2)), val);
	    fflush(stdout);
	    ocp_mmu[addr] = val;
	    return;
	}
    }
}

static uint32_t ubicom32_ocp_qemu_readl (void *opaque, target_phys_addr_t addr)
{
    struct ocp_info *ocp = opaque;
    uint32_t val;
    addr &= 0xfff;

    if (addr < 0x100) {
	addr /= 4;
	return ocp_general[addr];
    } else if ((addr >= ocp->def->timer_offset) && addr < ocp->def->timer_offset + 0x100) {
	volatile uint32_t *ptr = &ubicom32_timer.mptval;
	/*
	 * Read from a timer register. Convert the address to an index.
	 */
	addr -= (ocp->def->timer_offset);
	addr >>= 2;
	return ptr[addr];
    } else if ((addr >= ocp->def->mailbox_offset) && addr < ocp->def->mailbox_offset + 0x100) {
	/*
	 * Read from mailbox. 0 is data from the qemu_in_data. 8 is mail stat.
	 */
	addr -= ocp->def->mailbox_offset;

	if (addr == 0) {
	    /*
	     * There better be data to read;
	     */
	    uint32_t mb_data;
	    if(qemu_in_data.tail == qemu_in_data.head)
		return 0;

	    mb_data = qemu_in_data.data[qemu_in_data.tail++];
	    qemu_in_data.tail %= 32;
	    return mb_data;
	} else if (addr == 8) {
	    uint32_t mstat = ISD_MAILBOX_STATUS_OUT_EMPTY;

	    if (qemu_in_data.tail == qemu_in_data.head) {
		mstat |= ISD_MAILBOX_STATUS_IN_EMPTY;
	    }

	    return mstat;
	}
    } else if ((addr >= ocp->def->mmu_offset) && addr < ocp->def->mmu_offset + 0x200) {
	/*
	 * Convert to an index.
	 */
	addr -= ocp->def->mmu_offset;
	addr >>= 2;
	val = ocp_mmu[addr];

	if ((CONFIG <= addr && addr < MISSQW0) || (PTEC_ERR <= addr && addr < 0x80)) {
	    return val;
	}

	if (addr == MISSQW0) {
	    uint32_t tnum = m_t_ix_queue.miss_thread_index[m_t_ix_queue.tail];
	    if (m_t_ix_queue.tail != m_t_ix_queue.head) {
		/*
		 * There is a valid entry. Read the w0 from slot 0
		 */
		return m_queue.m_q_entries[tnum].slot[0].w0;
	    } else {
		return 0;
	    }
	}

	if (addr == MISSQW1) {
	    uint32_t tnum = m_t_ix_queue.miss_thread_index[m_t_ix_queue.tail];
	    if (m_t_ix_queue.tail != m_t_ix_queue.head) {
		/*
		 * There is a valid entry. Read the w1 from slot 0
		 */
		uint32_t w1 = m_queue.m_q_entries[tnum].slot[0].w1;

		/*
		 * Test the multiple bit in W1
		 */
		if (w1 & (1 << 11)) {
		    /*
		     * We have to copy slot 1 to slot 0
		     */
		    m_queue.m_q_entries[tnum].slot[0] = m_queue.m_q_entries[tnum].slot[1];
		} else {
		    /*
		     * Take the entry off the ring buffer.
		     */
		    m_t_ix_queue.tail++;
		    m_t_ix_queue.tail %= 13;

		    if (m_t_ix_queue.tail == m_t_ix_queue.head) {
			/*
			 * The queue is empty lower the interrupt.
			 */
			ubicom32_lower_interrupt(29);
		    }
		}
		return w1;
	    } else {
		return 0;
	    }
	}

	return 0;
    } else if (addr == 0x510 || addr == 0x610) {
	/*
	 * Read of the cache control registers. Return 1.Fake it as if the done bit is set
	 */
	return 1;
    }
    return 0;
}

static CPUWriteMemoryFunc *ubicom32_ocp_qemu_write[] = {
    &ubicom32_ocp_qemu_writeb,
    &ubicom32_ocp_qemu_writew,
    &ubicom32_ocp_qemu_writel,
};

static CPUReadMemoryFunc *ubicom32_ocp_qemu_read[] = {
    &ubicom32_ocp_qemu_readl,
    &ubicom32_ocp_qemu_readl,
    &ubicom32_ocp_qemu_readl,
};

static int ubicom32_ocp_qemu_iomemtype = 0;
struct ocp_info ubicom32_ocp_info;

static void cpu_ubicom32_irq_request(void *opaque, int irq, int level)
{
    struct ocp_info *ocp = (struct ocp_info *)opaque;
    int int_reg_num = irq / 32;
    int int_bit_num = irq % 32;
    int i;
    int int_mask;
    /*
     * Set the bit in the appropriate int_stat register.
     */
    int_mask = (1 << int_bit_num);
 
    if (level) {
	ocp->global_regs[1 + int_reg_num] |= int_mask;

	/*
	 * See if interrupts are enabled. If they are not we are done.
	 */
	if ((ocp->global_regs[0x4d - 0x40] & 1) == 0) {
	    return;
	}

	/*
	 * Interrupts are enabled. Wake up an inactive thread that is looking for this
	 * interrupt.
	 */
	for (i = 0; i < ocp->def->num_threads; i++) {
	    /*
	     * If the thread is active continue to the next thread.
	     */
	    int thread_mask = 1 << i;
	    if (ocp->global_regs[0x4e - 0x40] & thread_mask) {
		continue;
	    }
	    
	    /*
	     * If the bit is set in the int mask register wake the process by setting the thread bit in the mt_active register.
	     */
	    if (threads[i]->thread_regs[0x30 + int_reg_num] & int_mask) {
		/*
		 * Set the mt_active bit.
		 */
		ocp->global_regs[0x4e - 0x40] |=  thread_mask;
	    }
	}
    } else {
	/*
	 * Clear the bit.
	 */
	ocp->global_regs[1 + int_reg_num] &= ~int_mask;
    }
}

static void ubicom32_mptval_timer_cb (void *opaque)
{
    struct ocp_info *ocp = opaque;
    uint64_t now, next;

    ubicom32_timer.mptval++;

    /*
     * Restart the timer for another tick.
     */
    now = qemu_get_clock_ms(vm_clock);
    //next = now + muldiv64(1, get_ticks_per_sec(), ubicom32_ocp_info.mptval_frequency);
    next = now+1;
    qemu_mod_timer(ocp->mptval_timer, next);

    if ( ubicom32_timer.mptval == ubicom32_timer.rtcom ) {
	/*
	 * Fire the real time timer interrupt. (bit 32 in int_stat0)
	 */
	qemu_irq_raise(ocp->ubi_irq[30]);
    }
}

static void ubicom32_sysval_timer_cb (void *opaque)
{
    struct ocp_info *ocp = opaque;
    uint64_t now, next;
    uint32_t i;
    //int32_t old_time = (int32_t) ubicom32_timer.sysval;
    //uint32_t new_time = ubicom32_timer.sysval + 1000;

    /*
     * Restart the timer for another tick.
     */
    now = qemu_get_clock_ms(vm_clock);
    //next = now + muldiv64(1, get_ticks_per_sec(), ubicom32_ocp_info.sysval_frequency);
    next = now+1;
    qemu_mod_timer(ocp->sysval_timer, next);

    /*
     * Run through all the syscom registers and see if any of them is going to fire.
     * If it fires then we have to raise the appropriate interrupt.
     */
    for (i = 0; i < ocp->def->num_timers; i++) {
	//if (((int32_t) ubicom32_timer.syscom[i] - old_time ) < 1000) {
	if ( ubicom32_timer.sysval == ubicom32_timer.syscom[i]) {
	    /*
	     * Fire the interrupt.
	     */
	    qemu_irq_raise(ocp->ubi_irq[32+i]);
	}
    }    
    ubicom32_timer.sysval++;
}

static int serial_can_receive(void *opaque)
{
    int ring_data = (qemu_in_data.head < qemu_in_data.tail) ? (qemu_in_data.head + 32 - qemu_in_data.tail) : (qemu_in_data.tail - qemu_in_data.head);
    
    return 31 - ring_data;
}

static void serial_receive(void *opaque, const uint8_t *buf, int size)
{
    struct ocp_info *ocp = opaque;
    int i;

    for (i = 0; i < size; i++) {
	qemu_in_data.data[qemu_in_data.head ++] = buf[i];
	qemu_in_data.head %= 32;
    }

    /*
     * Tell cpu there is data in mailbox by raising the mailbox interrupt (bit 30 in int_stat1).
     */
    qemu_irq_raise(ocp->ubi_irq[32+30]);
}

static void serial_event(void *opaque, int event)
{
  //struct ocm_info *ocm = opaque;
#ifdef DEBUG_SERIAL
    printf("serial: event %x\n", event);
#endif
}

static const MemoryRegionOps ubicom32_opc_ops = {
    .read = ubicom32_ocp_qemu_read,
    .write = ubicom32_ocp_qemu_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

void ubicom32_ocp_init (CPUUBICOM32State *env, const struct ubicom32_board_t *def)
{
    uint64_t now, next;
/* FIXME */
/*    printf ("Ubicom32 OCP not initialized\n");	*/
    return;

#if 0 
/* FIXME -- use memory_region_init_io() -- see mips_malta.c */
    if (!ubicom32_ocp_qemu_iomemtype) {
        ubicom32_ocp_qemu_iomemtype = cpu_register_io_memory(ubicom32_ocp_qemu_read,
							     ubicom32_ocp_qemu_write, &ubicom32_ocp_info);
    }
    cpu_register_physical_memory(def->ocp_base, def->ocp_size, ubicom32_ocp_qemu_iomemtype);
#endif

    ubicom32_ocp_info.def = def;
    ubicom32_ocp_info.mptval_frequency = 12000000;
    ubicom32_ocp_info.sysval_frequency = 12000000;
    ubicom32_ocp_info.global_regs = ubicom32_global_registers;
    ubicom32_ocp_info.ubi_irq = qemu_allocate_irqs(cpu_ubicom32_irq_request, &ubicom32_ocp_info, def->num_interrupts);;

    /*
     * Initialize the ocp_general block.
     */
    ocp_general[0] = 0x80000080;
    ocp_general[1] = 0x0103e080;
    ocp_general[2] = 0x00000080;
    ocp_general[3] = 0x00000080;
    ocp_general[4] = 0x00000000;
    ocp_general[6] = 0x0003000e;
    ocp_general[7] = 0x00000000;
    ocp_general[0x21] = 0x00000001; /* External Reset */

    /*
     * Zero the timer block.
     */
    memset(&ubicom32_timer, 0, sizeof(ubicom32_timer));
    ubicom32_ocp_info.mptval_timer = qemu_new_timer_ms(vm_clock, &ubicom32_mptval_timer_cb, &ubicom32_ocp_info);
    ubicom32_ocp_info.sysval_timer = qemu_new_timer_ms(vm_clock, &ubicom32_sysval_timer_cb, &ubicom32_ocp_info);

    /*
     * Get both the timers running.
     */
    now = qemu_get_clock_ms(vm_clock);
    next = now + muldiv64(1, get_ticks_per_sec(), ubicom32_ocp_info.mptval_frequency);
    qemu_mod_timer(ubicom32_ocp_info.mptval_timer, next);
    qemu_mod_timer(ubicom32_ocp_info.sysval_timer, next);

    /*
     * Hook up QEMU serial_hds[0] to the qemu_in_data.
     */
    memset(&qemu_in_data, 0, sizeof(qemu_in_data));

    ubicom32_ocp_info.chr = serial_hds[0];
    qemu_in_data.head = qemu_in_data.tail = 0;

    qemu_chr_add_handlers(serial_hds[0], serial_can_receive, serial_receive,
                          serial_event, &ubicom32_ocp_info);

    /*
     * Initialize the mmu block.
     */
    memset(&ocp_mmu[0], 0, 0x200);
    memset(&ubicom32_mmu, 0, sizeof(ubicom32_mmu));
    ocp_mmu[0] = 0x0000f003;
    ocp_mmu[1] = 0x00050000;

    ubicom32_mmu.ptec_entries_log2 = PTEC_ENTRIES_LOG2;
    if (PAGE_MASK == 1) {
	ubicom32_mmu.vpn_bits = 19;
    } else {
	ubicom32_mmu.vpn_bits = 18;
    }

    m_t_ix_queue.head = m_t_ix_queue.tail = 0;
}

void irq_info(Monitor *mon)
{
}

void pic_info(Monitor *mon)
{
}

void ubicom32_raise_interrupt(uint32_t int_num)
{
#if 0
/* FIXME */
    qemu_irq_raise(ubicom32_ocp_info.ubi_irq[int_num]);
#endif
}

void ubicom32_lower_interrupt(uint32_t int_num)
{
    qemu_irq_lower(ubicom32_ocp_info.ubi_irq[int_num]);
}

uint32_t mmu_runnable(CPUUBICOM32State *env)
{
    return ((ocp_mmu[BUS_ST0] & (1 << env->cpu_index)) == 0);
}
