/*
 *  ubicom32 opc block structure defines
 *
 *  Copyright (c) 2009 Ubicom Inc.
 *  Written by Nat Gurumoorthy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "qemu-common.h"

struct ocp_info {
    const struct ubicom32_board_t *def;
    struct QEMUTimer *mptval_timer;
    struct QEMUTimer *sysval_timer;
    uint32_t sysval_frequency;
    uint32_t mptval_frequency;
    uint32_t *global_regs;
    qemu_irq *ubi_irq;
    uint32_t pll_reg;		/* This represents location 0000 of the ocp block which for the 7k contains the PLL config. */
    CharDriverState *chr;
};

extern struct ocp_info ubicom32_ocp_info;

extern uint32_t mmu_runnable(CPUUBICOM32State *env);
extern void ubicom32_lower_interrupt(uint32_t int_num);
extern uint32_t tlb_check_no_serror(CPUUBICOM32State *env, uint32_t *vpn, uint32_t *asid, 
				    uint32_t vaddr, uint32_t *return_ptec_entry, uint32_t is_read, 
				    uint32_t is_ifetch);
